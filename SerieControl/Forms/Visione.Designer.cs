﻿namespace SerieControl
{
    partial class Visione
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Visione));
            this.axVLCPlugin21 = new AxAXVLC.AxVLCPlugin2();
            this.buttonStart = new System.Windows.Forms.Button();
            this.trackBar1 = new System.Windows.Forms.TrackBar();
            this.buttonRestart = new System.Windows.Forms.Button();
            this.buttonLess = new System.Windows.Forms.Button();
            this.buttonPuls = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.buttonFullScreen = new System.Windows.Forms.Button();
            this.trackBarVolume = new System.Windows.Forms.TrackBar();
            this.labelVolume = new System.Windows.Forms.Label();
            this.buttonScreenshot = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.checkBoxNextEpSave = new System.Windows.Forms.CheckBox();
            this.checkBoxVR = new System.Windows.Forms.CheckBox();
            this.buttonPlus5 = new System.Windows.Forms.Button();
            this.buttonLess5 = new System.Windows.Forms.Button();
            this.buttonPrev = new System.Windows.Forms.Button();
            this.buttonNext = new System.Windows.Forms.Button();
            this.checkBoxPrecisino = new System.Windows.Forms.CheckBox();
            this.buttonEndEp = new System.Windows.Forms.Button();
            this.buttonEndSigla = new System.Windows.Forms.Button();
            this.buttonStartSigla = new System.Windows.Forms.Button();
            this.listBoxSpeech = new System.Windows.Forms.ListBox();
            ((System.ComponentModel.ISupportInitialize)(this.axVLCPlugin21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarVolume)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // axVLCPlugin21
            // 
            this.axVLCPlugin21.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.axVLCPlugin21.Enabled = true;
            this.axVLCPlugin21.Location = new System.Drawing.Point(12, 12);
            this.axVLCPlugin21.Name = "axVLCPlugin21";
            this.axVLCPlugin21.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axVLCPlugin21.OcxState")));
            this.axVLCPlugin21.Size = new System.Drawing.Size(840, 327);
            this.axVLCPlugin21.TabIndex = 0;
            this.axVLCPlugin21.MediaPlayerPlaying += new System.EventHandler(this.AxVLCPlugin21_MediaPlayerPlaying);
            this.axVLCPlugin21.MediaPlayerEndReached += new System.EventHandler(this.axVLCPlugin21_MediaPlayerEndReached);
            this.axVLCPlugin21.MediaPlayerTimeChanged += new AxAXVLC.DVLCEvents_MediaPlayerTimeChangedEventHandler(this.axVLCPlugin21_MediaPlayerTimeChanged);
            // 
            // buttonStart
            // 
            this.buttonStart.Location = new System.Drawing.Point(3, 34);
            this.buttonStart.Name = "buttonStart";
            this.buttonStart.Size = new System.Drawing.Size(75, 23);
            this.buttonStart.TabIndex = 1;
            this.buttonStart.Text = "Play";
            this.buttonStart.UseVisualStyleBackColor = true;
            this.buttonStart.Click += new System.EventHandler(this.buttonStart_Click);
            // 
            // trackBar1
            // 
            this.trackBar1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.trackBar1.Location = new System.Drawing.Point(3, -2);
            this.trackBar1.Maximum = 2000;
            this.trackBar1.Name = "trackBar1";
            this.trackBar1.Size = new System.Drawing.Size(842, 45);
            this.trackBar1.TabIndex = 2;
            this.trackBar1.TickFrequency = 100;
            this.trackBar1.Scroll += new System.EventHandler(this.trackBar1_Scroll);
            // 
            // buttonRestart
            // 
            this.buttonRestart.Location = new System.Drawing.Point(84, 34);
            this.buttonRestart.Name = "buttonRestart";
            this.buttonRestart.Size = new System.Drawing.Size(75, 23);
            this.buttonRestart.TabIndex = 4;
            this.buttonRestart.Text = "Restart";
            this.buttonRestart.UseVisualStyleBackColor = true;
            this.buttonRestart.Click += new System.EventHandler(this.buttonRestart_Click);
            // 
            // buttonLess
            // 
            this.buttonLess.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonLess.Location = new System.Drawing.Point(636, 33);
            this.buttonLess.Name = "buttonLess";
            this.buttonLess.Size = new System.Drawing.Size(28, 23);
            this.buttonLess.TabIndex = 6;
            this.buttonLess.Text = "-";
            this.buttonLess.UseVisualStyleBackColor = true;
            this.buttonLess.Click += new System.EventHandler(this.buttonLess_Click);
            // 
            // buttonPuls
            // 
            this.buttonPuls.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonPuls.Location = new System.Drawing.Point(604, 33);
            this.buttonPuls.Name = "buttonPuls";
            this.buttonPuls.Size = new System.Drawing.Size(28, 23);
            this.buttonPuls.TabIndex = 5;
            this.buttonPuls.Text = "+";
            this.buttonPuls.UseVisualStyleBackColor = true;
            this.buttonPuls.Click += new System.EventHandler(this.buttonPuls_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(246, 38);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(36, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "coso: ";
            // 
            // buttonFullScreen
            // 
            this.buttonFullScreen.Location = new System.Drawing.Point(165, 34);
            this.buttonFullScreen.Name = "buttonFullScreen";
            this.buttonFullScreen.Size = new System.Drawing.Size(75, 23);
            this.buttonFullScreen.TabIndex = 8;
            this.buttonFullScreen.Text = "FullScreen";
            this.buttonFullScreen.UseVisualStyleBackColor = true;
            this.buttonFullScreen.Click += new System.EventHandler(this.buttonFullScreen_Click);
            // 
            // trackBarVolume
            // 
            this.trackBarVolume.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.trackBarVolume.Location = new System.Drawing.Point(670, 32);
            this.trackBarVolume.Maximum = 150;
            this.trackBarVolume.Name = "trackBarVolume";
            this.trackBarVolume.Size = new System.Drawing.Size(142, 45);
            this.trackBarVolume.TabIndex = 9;
            this.trackBarVolume.TickFrequency = 25;
            this.trackBarVolume.Value = 100;
            this.trackBarVolume.Scroll += new System.EventHandler(this.trackBarVolume_Scroll);
            // 
            // labelVolume
            // 
            this.labelVolume.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelVolume.AutoSize = true;
            this.labelVolume.Location = new System.Drawing.Point(809, 39);
            this.labelVolume.Name = "labelVolume";
            this.labelVolume.Size = new System.Drawing.Size(36, 13);
            this.labelVolume.TabIndex = 10;
            this.labelVolume.Text = "100 %";
            // 
            // buttonScreenshot
            // 
            this.buttonScreenshot.Location = new System.Drawing.Point(3, 63);
            this.buttonScreenshot.Name = "buttonScreenshot";
            this.buttonScreenshot.Size = new System.Drawing.Size(102, 23);
            this.buttonScreenshot.TabIndex = 11;
            this.buttonScreenshot.Text = "TakeScreenshot";
            this.buttonScreenshot.UseVisualStyleBackColor = true;
            this.buttonScreenshot.Click += new System.EventHandler(this.buttonScreenshot_Click);
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Controls.Add(this.checkBoxNextEpSave);
            this.panel1.Controls.Add(this.checkBoxVR);
            this.panel1.Controls.Add(this.buttonPlus5);
            this.panel1.Controls.Add(this.buttonLess5);
            this.panel1.Controls.Add(this.buttonPrev);
            this.panel1.Controls.Add(this.buttonNext);
            this.panel1.Controls.Add(this.checkBoxPrecisino);
            this.panel1.Controls.Add(this.buttonEndEp);
            this.panel1.Controls.Add(this.buttonEndSigla);
            this.panel1.Controls.Add(this.buttonStartSigla);
            this.panel1.Controls.Add(this.buttonScreenshot);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.labelVolume);
            this.panel1.Controls.Add(this.buttonStart);
            this.panel1.Controls.Add(this.trackBarVolume);
            this.panel1.Controls.Add(this.buttonRestart);
            this.panel1.Controls.Add(this.buttonFullScreen);
            this.panel1.Controls.Add(this.buttonPuls);
            this.panel1.Controls.Add(this.buttonLess);
            this.panel1.Controls.Add(this.trackBar1);
            this.panel1.Location = new System.Drawing.Point(12, 345);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(842, 91);
            this.panel1.TabIndex = 12;
            // 
            // checkBoxNextEpSave
            // 
            this.checkBoxNextEpSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBoxNextEpSave.AutoSize = true;
            this.checkBoxNextEpSave.Checked = true;
            this.checkBoxNextEpSave.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxNextEpSave.Location = new System.Drawing.Point(406, 40);
            this.checkBoxNextEpSave.Name = "checkBoxNextEpSave";
            this.checkBoxNextEpSave.Size = new System.Drawing.Size(192, 17);
            this.checkBoxNextEpSave.TabIndex = 21;
            this.checkBoxNextEpSave.Text = "save end ep. when clicked next ep";
            this.checkBoxNextEpSave.UseVisualStyleBackColor = true;
            this.checkBoxNextEpSave.CheckedChanged += new System.EventHandler(this.checkBoxNextEpSave_CheckedChanged);
            // 
            // checkBoxVR
            // 
            this.checkBoxVR.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBoxVR.AutoSize = true;
            this.checkBoxVR.Checked = true;
            this.checkBoxVR.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxVR.Location = new System.Drawing.Point(481, 66);
            this.checkBoxVR.Name = "checkBoxVR";
            this.checkBoxVR.Size = new System.Drawing.Size(108, 17);
            this.checkBoxVR.TabIndex = 20;
            this.checkBoxVR.Text = "Voice recognition";
            this.checkBoxVR.UseVisualStyleBackColor = true;
            this.checkBoxVR.CheckedChanged += new System.EventHandler(this.checkBoxVR_CheckedChanged);
            // 
            // buttonPlus5
            // 
            this.buttonPlus5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonPlus5.Location = new System.Drawing.Point(604, 62);
            this.buttonPlus5.Name = "buttonPlus5";
            this.buttonPlus5.Size = new System.Drawing.Size(28, 23);
            this.buttonPlus5.TabIndex = 18;
            this.buttonPlus5.Text = "+5";
            this.buttonPlus5.UseVisualStyleBackColor = true;
            this.buttonPlus5.Click += new System.EventHandler(this.buttonPlus5_Click);
            // 
            // buttonLess5
            // 
            this.buttonLess5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonLess5.Location = new System.Drawing.Point(636, 62);
            this.buttonLess5.Name = "buttonLess5";
            this.buttonLess5.Size = new System.Drawing.Size(28, 23);
            this.buttonLess5.TabIndex = 19;
            this.buttonLess5.Text = "-5";
            this.buttonLess5.UseVisualStyleBackColor = true;
            this.buttonLess5.Click += new System.EventHandler(this.buttonLess5_Click);
            // 
            // buttonPrev
            // 
            this.buttonPrev.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonPrev.Location = new System.Drawing.Point(684, 63);
            this.buttonPrev.Name = "buttonPrev";
            this.buttonPrev.Size = new System.Drawing.Size(75, 23);
            this.buttonPrev.TabIndex = 17;
            this.buttonPrev.Text = "<";
            this.buttonPrev.UseVisualStyleBackColor = true;
            this.buttonPrev.Click += new System.EventHandler(this.buttonPrev_Click);
            // 
            // buttonNext
            // 
            this.buttonNext.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonNext.Location = new System.Drawing.Point(765, 63);
            this.buttonNext.Name = "buttonNext";
            this.buttonNext.Size = new System.Drawing.Size(75, 23);
            this.buttonNext.TabIndex = 16;
            this.buttonNext.Text = ">";
            this.buttonNext.UseVisualStyleBackColor = true;
            this.buttonNext.Click += new System.EventHandler(this.buttonNext_Click);
            // 
            // checkBoxPrecisino
            // 
            this.checkBoxPrecisino.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBoxPrecisino.AutoSize = true;
            this.checkBoxPrecisino.Location = new System.Drawing.Point(406, 67);
            this.checkBoxPrecisino.Name = "checkBoxPrecisino";
            this.checkBoxPrecisino.Size = new System.Drawing.Size(69, 17);
            this.checkBoxPrecisino.TabIndex = 15;
            this.checkBoxPrecisino.Text = "Precisino";
            this.checkBoxPrecisino.UseVisualStyleBackColor = true;
            this.checkBoxPrecisino.CheckedChanged += new System.EventHandler(this.checkBoxPrecisino_CheckedChanged);
            // 
            // buttonEndEp
            // 
            this.buttonEndEp.Location = new System.Drawing.Point(269, 63);
            this.buttonEndEp.Name = "buttonEndEp";
            this.buttonEndEp.Size = new System.Drawing.Size(81, 23);
            this.buttonEndEp.TabIndex = 14;
            this.buttonEndEp.Text = "Fine Episodio";
            this.buttonEndEp.UseVisualStyleBackColor = true;
            this.buttonEndEp.Click += new System.EventHandler(this.buttonEndEp_Click);
            // 
            // buttonEndSigla
            // 
            this.buttonEndSigla.Location = new System.Drawing.Point(190, 63);
            this.buttonEndSigla.Name = "buttonEndSigla";
            this.buttonEndSigla.Size = new System.Drawing.Size(73, 23);
            this.buttonEndSigla.TabIndex = 13;
            this.buttonEndSigla.Text = "Fine Sigla";
            this.buttonEndSigla.UseVisualStyleBackColor = true;
            this.buttonEndSigla.Click += new System.EventHandler(this.buttonEndSigla_Click);
            // 
            // buttonStartSigla
            // 
            this.buttonStartSigla.Location = new System.Drawing.Point(111, 63);
            this.buttonStartSigla.Name = "buttonStartSigla";
            this.buttonStartSigla.Size = new System.Drawing.Size(73, 23);
            this.buttonStartSigla.TabIndex = 12;
            this.buttonStartSigla.Text = "Start Sigla";
            this.buttonStartSigla.UseVisualStyleBackColor = true;
            this.buttonStartSigla.Click += new System.EventHandler(this.buttonStartSigla_Click);
            // 
            // listBoxSpeech
            // 
            this.listBoxSpeech.FormattingEnabled = true;
            this.listBoxSpeech.Location = new System.Drawing.Point(12, 12);
            this.listBoxSpeech.Name = "listBoxSpeech";
            this.listBoxSpeech.Size = new System.Drawing.Size(159, 69);
            this.listBoxSpeech.TabIndex = 13;
            this.listBoxSpeech.Visible = false;
            // 
            // Visione
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(866, 448);
            this.Controls.Add(this.listBoxSpeech);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.axVLCPlugin21);
            this.Name = "Visione";
            this.Text = "Visione";
            ((System.ComponentModel.ISupportInitialize)(this.axVLCPlugin21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarVolume)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private AxAXVLC.AxVLCPlugin2 axVLCPlugin21;
        private System.Windows.Forms.Button buttonStart;
        private System.Windows.Forms.TrackBar trackBar1;
        private System.Windows.Forms.Button buttonRestart;
        private System.Windows.Forms.Button buttonLess;
        private System.Windows.Forms.Button buttonPuls;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button buttonFullScreen;
        private System.Windows.Forms.TrackBar trackBarVolume;
        private System.Windows.Forms.Label labelVolume;
        private System.Windows.Forms.Button buttonScreenshot;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button buttonEndEp;
        private System.Windows.Forms.Button buttonEndSigla;
        private System.Windows.Forms.Button buttonStartSigla;
        private System.Windows.Forms.CheckBox checkBoxPrecisino;
        private System.Windows.Forms.Button buttonNext;
        private System.Windows.Forms.Button buttonPrev;
        private System.Windows.Forms.Button buttonPlus5;
        private System.Windows.Forms.Button buttonLess5;
        private System.Windows.Forms.ListBox listBoxSpeech;
        private System.Windows.Forms.CheckBox checkBoxVR;
        private System.Windows.Forms.CheckBox checkBoxNextEpSave;
    }
}