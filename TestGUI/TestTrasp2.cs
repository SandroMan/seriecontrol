﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TestGUI
{
    public partial class TestTrasp2 : Form
    {
        public TestTrasp2()
        {
            InitializeComponent();
        }


        private Bitmap DrawControlToBitmap(Control control)
        {
            Bitmap bitmap = new Bitmap(control.Width, control.Height);
            Graphics graphics = Graphics.FromImage(bitmap);
            Rectangle rect = control.RectangleToScreen(control.ClientRectangle);
            graphics.CopyFromScreen(rect.Location, Point.Empty, control.Size);
            return bitmap;
        }

        private Image DrawZigZag(Size size)
        {
            Bitmap image = new Bitmap(size.Width, size.Height);

            using (var g = Graphics.FromImage(image))
            {
                Form form = this.FindForm();
                if (form == null)
                    return null;
                
                int graySpace = 5;

                g.SmoothingMode = SmoothingMode.AntiAlias;
                g.InterpolationMode = InterpolationMode.HighQualityBicubic;
                g.PixelOffsetMode = PixelOffsetMode.HighQuality;
                Brush brush = Brushes.White;
                #region Header
                int space = (int)(image.Width * 0.111);
                g.FillRectangle(brush, space + graySpace, 0, image.Width, image.Height);



                int x = space + graySpace, y = 0;
                //spicchi delle punte (metà lunghezza e l'altezza)
                int trW = 48, trH = 38;

                while (y < image.Height)
                {
                    g.FillPolygon(Brushes.Gray, new Point[] { new Point(x, y - graySpace), new Point(x - trW - graySpace, y + trH), new Point(x, y + (trH * 2) + graySpace) });
                    y += trH * 2;
                }

                y = 0;
                while (y < image.Height)
                {
                    g.FillPolygon(brush, new Point[] { new Point(x, y), new Point(x - trW, y + trH), new Point(x, y + (trH * 2)) });
                    y += trH * 2;
                }
                #endregion

                //image.Save(@"C:\test\1.png");

            }

            return image;
        }

        private void button1_Click(object sender, EventArgs e)
        {
        }

        private void button2_Click(object sender, EventArgs e)
        {
            pictureBoxInfo.Visible = false;
            box1.Refresh();
            button1.Refresh();
            Bitmap bitmap = DrawControlToBitmap(panel1);
            bitmap.Save(@"C:\test\ggg.png");
            pictureBoxInfo.Visible = true;
        }
    }
}
