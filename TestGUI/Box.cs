﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing.Drawing2D;
using System.Threading;
using SerieControl;

namespace TestGUI
{
    public partial class Box : UserControl
    {
        public Box()
        {
            InitializeComponent();
        }

        private Serie _serie;

        public Serie serie
        {
            get { return _serie; }
            set { _serie = value;
                
                pictureBoxCover.Image = DrawZigZag(cover);
            }
        }

        public int ID { get; set; }
        private Image cover;

        public Image Cover
        {
            get { return cover; }
            set { cover = value;
                pictureBoxCover.Image = DrawZigZag(cover);
            }
        }

        //This function checks the room size and your text and appropriate font for your text to fit in room
        //PreferedFont is the Font that you wish to apply
        //Room is your space in which your text should be in.
        //LongString is the string which it's bounds is more than room bounds.
        private Font FindFont(Graphics g, string longString, Size Room, Font PreferedFont)
        {
            SizeF FontSize = g.MeasureString(longString, PreferedFont);

            int w = (int)((PreferedFont.Size * Room.Width) / FontSize.Width);
            int h = (int)((PreferedFont.Size * Room.Height) / FontSize.Height);

            return new Font(PreferedFont.FontFamily, Math.Min(w, h));
            //if (FontSize.Width > Room.Width || FontSize.Height > Room.Height)
            //{
            //    //scale down
            //}




            //you should perform some scale functions!!!
            //float HeightScaleRatio = Room.Height / RealSize.Height;
            //float WidthScaleRatio = Room.Width / RealSize.Width;
            //float ScaleRatio = (HeightScaleRatio < WidthScaleRatio) ? ScaleRatio = HeightScaleRatio : ScaleRatio = WidthScaleRatio;
            //float ScaleFontSize = PreferedFont.Size * ScaleRatio;
            //return new Font(PreferedFont.FontFamily, ScaleFontSize);
        }

        private Image DrawZigZag(Image input)
        {
            Bitmap image = new Bitmap(204, 300);
            if (input != null)
                image = (Bitmap)((Bitmap)input).Clone();

            
            using (var g = Graphics.FromImage(image))
            {
                g.SmoothingMode = SmoothingMode.AntiAlias;
                g.InterpolationMode = InterpolationMode.HighQualityBicubic;
                g.PixelOffsetMode = PixelOffsetMode.HighQuality;
                Brush brush = Brushes.Black;

                //image.Save(@"C:\test\0.png");
                #region Header
                int header = (int)(image.Height * 0.111);
                g.FillRectangle(brush, 0, 0, image.Width, header);



                int x = 0, y = header;
                int trW = (int)(image.Width * 0.074), trH = (int)(image.Height * 0.052);
                int whiteSpace = 5;

                while (x < image.Width)
                {
                    g.FillPolygon(Brushes.White, new Point[] { new Point(x - whiteSpace, y), new Point(x + trW, y + trH + whiteSpace), new Point(x + trW * 2 + whiteSpace, y) });
                    x += trW * 2;
                }

                x = 0;
                while (x < image.Width)
                {
                    g.FillPolygon(brush, new Point[] { new Point(x, y), new Point(x + trW, y + trH), new Point(x + trW * 2, y) });
                    x += trW * 2;
                }
                #endregion
                //image.Save(@"C:\test\1.png");

                if (serie != null && serie.name != "")
                {
                    int DiffW = (int)(image.Width * 0.007);
                    int DiffH = (int)(image.Height * 0.005);

                    Rectangle rect1 = new Rectangle(DiffW, DiffH, image.Width - (DiffW * 2), header - (DiffH * 2));
                    //g.DrawRectangle(new Pen(Color.Red, 5), rect1);


                    StringFormat stringFormat = new StringFormat();
                    stringFormat.Alignment = StringAlignment.Center;
                    stringFormat.LineAlignment = StringAlignment.Center;
                    g.TextRenderingHint = System.Drawing.Text.TextRenderingHint.ClearTypeGridFit;

                    Font goodFont = FindFont(g, serie.name, rect1.Size, new Font("Calibri", 8));
                    g.DrawString(serie.name, goodFont, Brushes.White, rect1, stringFormat);


                    //g.DrawString(_name, new Font("Calibri", 8, FontStyle.Regular, GraphicsUnit.Pixel), Brushes.Black, 0, 0);
                }
                //image.Save(@"C:\test\2.png");
            }

            return image;
        }

        private void panel_Load(object sender, EventArgs e)
        {
            pictureBoxPlay.BackColor = Color.Transparent;
            pictureBoxPlay.Parent = pictureBoxCover;

            pictureBoxInfo.BackColor = Color.Transparent;
            pictureBoxInfo.Parent = pictureBoxCover;
        }



        public event EventHandler<EventArgs> ButtonInfoClicked;
        private void pictureBoxInfo_Click(object sender, EventArgs e)
        {
            if (ButtonInfoClicked != null)
                ButtonInfoClicked(this, null);
        }

        public event EventHandler<EventArgs> ButtonPlayClicked;
        private void pictureBoxPlay_Click(object sender, EventArgs e)
        {
            if(ButtonPlayClicked != null)
                ButtonPlayClicked(this, null);
        }

    }
}
