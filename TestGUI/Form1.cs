﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TestGUI
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            //panel1.name = "Serie 1";
            panel1.ButtonPlayClicked += Panel1_ButtonPlayClicked;
            panel1.ButtonInfoClicked += Panel1_ButtonInfoClicked;
        }

        private void Panel1_ButtonInfoClicked(object sender, EventArgs e)
        {
            MessageBox.Show(((Box)sender).Name + "  button info clicked");
        }

        private void Panel1_ButtonPlayClicked(object sender, EventArgs e)
        {
            MessageBox.Show(((Box)sender).Name + "  button play clicked");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            pictureBox1.Image = panel1.Cover;
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            panel1.serie.name = textBox1.Text;
        }
    }
}
