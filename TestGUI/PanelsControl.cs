﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing.Drawing2D;
using System.Threading;

namespace TestGUI
{
    public partial class PanelsControl : UserControl
    {
        public PanelsControl()
        {
            InitializeComponent();

            this.HorizontalScroll.Maximum = 0;
            this.AutoScroll = false;
            this.VerticalScroll.Visible = false;
            this.AutoScroll = true;

            infoControl1.Location = new Point(this.Width / 2, 0);
            infoControl1.BackColor = panel2.BackColor;
            infoControl1.BehindColor = Color.White;
        }

        private List<Box> _boxes;

        public List<Box> boxes
        {
            get { return _boxes; }
            set
            {
                _boxes = value;
                SetPannels();

            }
        }

        private void SetPannels(bool move = false)
        {
            if (_boxes == null)
                return;
            
            int panelW = 204;
            int panelH = 300;

            int distW = 8;
            int distH = 8;


            int ncol = this.Width / (panelW + distW); // non sto contando il distW inizale

            int col = 0;
            int row = 0;
            for (int i = 0; i < _boxes.Count; i++)
            {
                if (i % ncol == 0 && i != 0)
                {
                    row++;
                    col = 0;
                }

                _boxes[i].Location = new Point(distW + (panelW + distW) * col, distH + (panelH + distH) * row);
                if (!move)
                {
                    _boxes[i].Name = "box" + i;
                    _boxes[i].ID = i;
                    _boxes[i].ButtonInfoClicked += PanelsControl_ButtonInfoClicked;
                    _boxes[i].BackColor = panel2.BackColor;
                    panel2.Controls.Add(_boxes[i]);
                }
                col++;
            }
        }

        #region Info
        #region BoxInfo Selected
        void InfoBoxPosition(Box box)
        {
            int x = this.Width / 4 - box.Width / 2;
            int y = this.Height / 2 - box.Height / 2;
            box.Location = new Point(x, y);
        }

        void ScaleInfoBox(Box box)
        {
            double newW = this.Width * 0.212;
            double newH = this.Height * 0.558;

            double scaleW = newW / originalW;
            double scaleH = newH / originalH;
            double scale = Math.Min(scaleW, scaleH);

            PreviusScale = scale;
            box.Width = (int)(originalW * scale);
            box.Height = (int)(originalH * scale);
        }
        #endregion

        #region InfoBox
        public void ShowInfo()
        {
            infoControl1.Location = new Point(this.Width / 2, 0);
            infoControl1.Size = new Size(this.Width / 2, this.Height);
            ScaleInfoBox(_boxes[inExameID]);
            InfoBoxPosition(_boxes[inExameID]);
            infoControl1.UpdateAll();
        }
        #endregion

        int originalW, originalH;
        double PreviusScale = 1;
        int inExameID = -1;
        private void PanelsControl_ButtonInfoClicked(object sender, EventArgs e)
        {
            Box send = (Box)sender;
            if (!infoControl1.Visible)
            {
                inExameID = send.ID;
                originalW = send.Width;
                originalH = send.Height;

                infoControl1.serie = send.serie;
                ScaleInfoBox(send);
                InfoBoxPosition(send);
                infoControl1.Visible = true;

                foreach (var box in panel2.Controls)
                {
                    if (box.GetType() == typeof(Box) && ((Box)box).ID != send.ID)
                    {
                        ((Box)box).Hide();
                    }
                }
                ShowInfo();
            }
            else
            {
                send.Width = (int)(send.Width / PreviusScale);
                send.Height = (int)(send.Height / PreviusScale);
                PreviusScale = 1;
                infoControl1.Visible = false;
                SetPannels(true);
                foreach (var box in panel2.Controls)
                {
                    if (box.GetType() == typeof(Box))
                    {
                        ((Box)box).Show();
                    }
                }
            }
        }
        #endregion

        private void PanelsControl_Resize(object sender, EventArgs e)
        {
            try
            {
                if (infoControl1.Visible)
                {
                    ShowInfo();
                }
                else
                {
                    SetPannels(true);
                }
            }
            catch { }
        }
    }
}
