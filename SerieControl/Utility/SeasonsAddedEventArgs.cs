﻿using System;

namespace SerieControl
{
    public class SeasonsAddedEventArgs : EventArgs
    {
        public SeasonsAddedEventArgs(string seasonName, Serie serie)
        {
            SeasonName = seasonName;
            this.serie = serie;
        }

        public string SeasonName { get; set; }
        public Serie serie { get; set; }
    }
}
