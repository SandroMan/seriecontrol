﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TestGUI
{
    public partial class TestTrasp : Form
    {
        public TestTrasp()
        {
            InitializeComponent();

            pictureBoxInfo.Image = DrawZigZag(pictureBoxInfo.Size);
        }

        private Image DrawZigZag(Size size)
        {
            Bitmap image = new Bitmap(size.Width, size.Height);

            using (var g = Graphics.FromImage(image))
            {
                Form form = this.FindForm();
                if (form == null)
                    return null;

                g.FillRectangle(Brushes.DarkRed, new RectangleF(0, 0, size.Width, size.Height));

                int graySpace = 5;

                g.SmoothingMode = SmoothingMode.AntiAlias;
                g.InterpolationMode = InterpolationMode.HighQualityBicubic;
                g.PixelOffsetMode = PixelOffsetMode.HighQuality;
                Brush brush = Brushes.White;
                #region Header
                int space = (int)(image.Width * 0.111);
                g.FillRectangle(brush, space + graySpace, 0, image.Width, image.Height);



                int x = space + graySpace, y = 0;
                //spicchi delle punte (metà lunghezza e l'altezza)
                int trW = 48, trH = 38;

                while (y < image.Height)
                {
                    g.FillPolygon(Brushes.Gray, new Point[] { new Point(x, y - graySpace), new Point(x - trW - graySpace, y + trH), new Point(x, y + (trH * 2) + graySpace) });
                    y += trH * 2;
                }

                y = 0;
                while (y < image.Height)
                {
                    g.FillPolygon(brush, new Point[] { new Point(x, y), new Point(x - trW, y + trH), new Point(x, y + (trH * 2)) });
                    y += trH * 2;
                }
                #endregion

                //image.Save(@"C:\test\1.png");

            }

            return image;
        }


    }
}
