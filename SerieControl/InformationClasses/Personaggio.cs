﻿namespace SerieControl
{
    public class Personaggio
    {
        public ulong ID { get; set; } //salvare l'ID e non tutta la classe

        public string language { get; set; }

        public Person attore { get; set; }
        public Person doppiatore { get; set; }

        public string story { get; set; }
    }
}
