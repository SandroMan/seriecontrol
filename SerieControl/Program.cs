﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SerieControl
{
    static class Program
    {
        /// <summary>
        /// Punto di ingresso principale dell'applicazione.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
            //Application.Run(new AddInfo(new string[] { "serie1", "serie2" }, new ListBox()));
            //Application.Run(new Visione(@"E:\seriecontrol\Test\Serie 1\seson 1\Avatar La leggenda di Aang 1x01.mp4"));
        }
    }
}
