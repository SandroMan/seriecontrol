﻿namespace SerieControl
{
    public class Person
    {
        public ulong ID { get; set; } //salvare l'ID e non tutta la classe

        public string language { get; set; }
        public string name { get; set; }
        public string surname { get; set; }

        public string fullname
        {
            get { return surname + " " + name; }
        }


        public string WikipediaPage { get; set; } = "???";
    }
}