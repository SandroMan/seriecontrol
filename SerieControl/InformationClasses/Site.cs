﻿namespace SerieControl
{
    public class Site
    {
        public ulong ID { get; set; } //salvare l'ID e non tutta la classe

        public string language { get; set; }
        public string name { get; set; }
        public string url { get; set; }
    }
}