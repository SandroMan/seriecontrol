﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.WindowsAPICodePack.Taskbar;
using Microsoft.Win32;

namespace SerieControl
{
    public partial class AddInfo : Form
    {
        #region SetUp
        int allFiles = 0;
        int allComplietedFiles = 0;
        int allSerie = 0;
        int allComplietedSerie = 0;

        string[] pathsSeries;
        ListBox listBoxSerie;

        Serie serie;
        public event EventHandler<SeasonsAddedEventArgs> SeasonsAdded;

        TaskbarManager taskbarInstance;

        public AddInfo(string[] pathsSeries, ListBox listBoxSerie)
        {
            InitializeComponent();
            this.pathsSeries = pathsSeries;
            this.listBoxSerie = listBoxSerie;
            this.MaximumSize = new Size(int.MaxValue, 489);


            //if (TaskbarManager.IsPlatformSupported)
            //{
            taskbarInstance = TaskbarManager.Instance;
            taskbarInstance.SetProgressState(TaskbarProgressBarState.Normal);
            //}
        }


        public AddInfo(string[] pathsSeasons, Serie serie, EventHandler<SeasonsAddedEventArgs> callbackAdded)
        {
            InitializeComponent();
            this.pathsSeries = pathsSeasons;
            this.serie = serie;
            this.SeasonsAdded = callbackAdded;
            this.MaximumSize = new Size(int.MaxValue, 489);


            taskbarInstance = TaskbarManager.Instance;
            taskbarInstance.SetProgressState(TaskbarProgressBarState.Normal);
        }

        Thread check;
        void SetUpFinalThread()
        {
            if (check == null || check.ThreadState == ThreadState.Stopped)
            {
                check = new Thread(() =>
                {
                    for (int i = 0; i < allT.Count; i++)
                    {
                        allT[i].Join();
                    }


                    finito = true;
                    try
                    {
                        this.Invoke((Action)delegate
                        {
                            //x sicurezza
                            this.Text = $"Added Files: {allFiles}/{allFiles} [{allSerie} / {allSerie} series] - 100 %";
                            allFiles = allComplietedFiles = allSerie = allComplietedSerie = 0;
                            taskbarInstance.SetProgressValue(0, 0);
                            FlashWindow.Flash(this);
                        });
                    }
                    catch { }
                });
            }

            //non ho voglia di devere tutti i stati
            try
            {
                check.Start();
            }
            catch { }

            //if (check.ThreadState != ThreadState.Running)//|| check.ThreadState == ThreadState.Unstarted || check.ThreadState == ThreadState.WaitSleepJoin)
            //{
            //    check.Start();
            //}
        }

        bool finito = false;
        List<Thread> allT;
        private void AddInfo_Load(object sender, EventArgs e)
        {
            allT = new List<Thread>();

            if (serie == null)
            {
                for (int i = 0; i < pathsSeries.Count(); i++)
                {
                    string folder = pathsSeries.ToArray()[i];
                    int llock = allT.Count;
                    allT.Add(new Thread(() => CreaSerie(llock, folder)));
                    allT.Last().Start();
                    allSerie++;
                }
            }
            else
            {
                for (int i = 0; i < pathsSeries.Count(); i++)
                {
                    string folder = pathsSeries.ToArray()[i];
                    int llock = allT.Count;
                    SetUpControls($"Stagione: {Path.GetFileName(folder)} di {serie.name}", llock);
                    var myprogressBar = ((ProgressBar)panel1.Controls["progressbar" + llock]);
                    myprogressBar.Maximum = Directory.GetFiles(folder, "*.*", SearchOption.AllDirectories).Where(s => s.EndsWith(".mp4") || s.EndsWith(".avi") || s.EndsWith(".mkv")).Count();
                    allFiles += myprogressBar.Maximum;

                    allT.Add(new Thread(() => {
                        var season = CreaStagione(llock, folder, llock);
                        if(season != null)
                        {
                            serie.Seasons.Add(season);
                            SeasonsAdded(null, new SeasonsAddedEventArgs(Path.GetFileName(folder), serie));
                        }
                        allComplietedSerie++;
                    })); // varie stagioni da varie serie;
                    allT.Last().Start();
                    allSerie++;
                }
            }

            SetUpFinalThread();
        }
        #endregion

        private void AddInfo_FormClosing(object sender, FormClosingEventArgs e)
        {
            foreach (var t in allT)
            {
                t.Abort();
            }

            taskbarInstance.SetProgressValue(0, 0);
        }

        #region Utility
        void SetUpControls(string serieName, int i)
        {
            Label label = new Label();
            label.Name = "labelserie" + i;
            label.AutoSize = true;
            label.Text = serieName;
            label.Location = new Point(12 + 156 * i, 9);
            panel1.Controls.Add(label);

            ListBox listBox = new ListBox();
            listBox.Name = "listbox" + i;
            listBox.HorizontalScrollbar = true;
            listBox.Location = new Point(12 + 156 * i, 25);
            listBox.Size = new Size(150, 381);
            panel1.Controls.Add(listBox);

            ProgressBar progressBar = new ProgressBar();
            progressBar.Name = "progressbar" + i;
            progressBar.Location = new Point(12 + 156 * i, 412);
            progressBar.Size = new Size(110, 27);
            panel1.Controls.Add(progressBar);

            Label labelPerc = new Label();
            labelPerc.Name = "labelprogress" + i;
            labelPerc.AutoSize = true;
            labelPerc.Text = "0 %";
            labelPerc.Location = new Point(126 + 156 * i, 417);
            panel1.Controls.Add(labelPerc);

        }
        void AddListBoxItem(ListBox lstBox, string value)
        {
            lstBox.Items.Add(value);
            lstBox.SelectedIndex = lstBox.Items.Count - 1;
        }
        #endregion

        #region Crea Serie Season
        void CreaSerie(int ID, string folder)
        {
            string NomeSerie = Path.GetFileName(folder);

            if (Form1.series.Where(ser => ser.name == NomeSerie).Count() != 0)
            {
                if (MessageBox.Show("Serie con lo stesso nome già presente, Continuare comunque rinominandola(Y) o skippare la serie(N) ?", "Duplicato: " + NomeSerie, MessageBoxButtons.YesNoCancel) != DialogResult.Yes)
                {
                    this.BeginInvoke((Action)delegate {
                        //AddListBoxItem(myInfo, "Cancellata");
                        ((ProgressBar)panel1.Controls["progressbar" + ID]).Value = 100;
                        ((Label)panel1.Controls["labelprogress" + ID]).Text = "100 %";
                    });
                    return;
                }

                NomeSerie += "_";
            }
            ListBox myInfo = null;
            this.BeginInvoke((Action)delegate { SetUpControls(NomeSerie, ID); myInfo = ((ListBox)panel1.Controls["listbox" + ID]); });

            string[] stagioni = Directory.GetDirectories(folder);
            ProgressBar myprogressBar = null;
            this.BeginInvoke((Action)delegate {
                myprogressBar = ((ProgressBar)panel1.Controls["progressbar" + ID]);
                myprogressBar.Maximum = Directory.GetFiles(folder, "*.*", SearchOption.AllDirectories).Where(s => s.EndsWith(".mp4") || s.EndsWith(".avi") || s.EndsWith(".mkv")).Count();
                allFiles += myprogressBar.Maximum;
            });


            serie = new Serie(NomeSerie);
            //per ogni cartella seson
            foreach (var stagione in stagioni) // va in ordine di creazione
            {
                var season = CreaStagione(serie.Seasons.Count, stagione, ID);
                if (season != null)
                {
                    serie.Seasons.Add(season);
                }
            }

            if (Directory.GetFiles(folder, "*.*", SearchOption.TopDirectoryOnly).Where(s => s.EndsWith(".mp4") || s.EndsWith(".avi") || s.EndsWith(".mkv")).Count() != 0)
            {
                this.Invoke((Action)delegate { AddListBoxItem(myInfo, "Trovati files nella cartella delle serie"); });
                var season = CreaStagione(serie.Seasons.Count, folder, ID, SearchOption.TopDirectoryOnly);
                if (season != null)
                {
                    serie.Seasons.Add(season);
                }
            }

            if(serie.Seasons.Count != 0)
                Form1.series.Add(serie);

            this.Invoke((Action)delegate
            {
                if (serie.Seasons.Count != 0)
                    listBoxSerie.Items.Add(NomeSerie);

                if (myprogressBar.Maximum == 0)
                {
                    myprogressBar.Maximum = 100;
                    myprogressBar.Value = 100;
                }
                allComplietedSerie++;

                AddListBoxItem(myInfo, "Finito");
            });
        }

        Season CreaStagione(int Nseason, string pathCartellaStagione, int IDSerie, SearchOption option = SearchOption.AllDirectories)
        {
            string name = Path.GetFileName(pathCartellaStagione);
            ListBox listBox = null;
            this.BeginInvoke((Action)delegate {
                listBox = ((ListBox)panel1.Controls["listbox" + IDSerie]);
                AddListBoxItem(listBox, "Creazione stagione: " + name);
            });

            List<Episode> episodes = new List<Episode>();
            int ID = 1;

            ProgressBar progressBar = null;
            Label progresslabel = null;
            this.BeginInvoke((Action)delegate
            {
                progressBar = ((ProgressBar)panel1.Controls["progressbar" + IDSerie]);
                progresslabel = ((Label)panel1.Controls["labelprogress" + IDSerie]);
            });
            foreach (string ep in Directory.GetFiles(pathCartellaStagione, "*.*", option).Where(s => s.EndsWith(".mp4") || s.EndsWith(".avi") || s.EndsWith(".mkv")))
            {
                //se il thread viene interrotto
                try
                {
                    Episode episode = new Episode(ep, ID++ + "");
                    episodes.Add(episode);
                    this.Invoke((Action)delegate
                    {
                        progressBar.Value++;
                        allComplietedFiles++;

                        this.Text = $"Added Files: {allComplietedFiles}/{allFiles} [{allComplietedSerie} / {allSerie} series] - {100 * allComplietedFiles / allFiles} %";
                        progresslabel.Text = (100 * progressBar.Value / progressBar.Maximum) + " %";

                        AddListBoxItem(listBox, $"Creato episodio: [{Nseason + 1}-{episode.ID}] {episode.titolo}");
                        taskbarInstance.SetProgressValue(allComplietedFiles, allFiles);
                    });
                }
                catch { }
                //File.AppendAllLines("")
            }

            return episodes.Count == 0 && Settings.noEpisodebutAdd ? null : new Season(name, episodes);
        }
        #endregion

        #region AddPaths
        public void AddSeriePaths(string[] pathsSeries)
        {
            for (int i = 0; i < pathsSeries.Count(); i++)
            {
                string folder = pathsSeries.ToArray()[i];
                int llock = allT.Count;
                allT.Add(new Thread(() => CreaSerie(llock, folder)));
                allT.Last().Start();
                allSerie++;
            }

            SetUpFinalThread();
        }

        public void AddSeasonPaths(string[] pathsSeasons, Serie serie, EventHandler<SeasonsAddedEventArgs> callbackAdded)
        {
            for (int i = 0; i < pathsSeasons.Count(); i++)
            {
                string folder = pathsSeasons.ToArray()[i];
                int llock = allT.Count;
                SetUpControls($"Stagione: {Path.GetFileName(folder)} di {serie.name}", llock);
                var myprogressBar = ((ProgressBar)panel1.Controls["progressbar" + llock]);
                myprogressBar.Maximum = Directory.GetFiles(folder, "*.*", SearchOption.AllDirectories).Where(s => s.EndsWith(".mp4") || s.EndsWith(".avi") || s.EndsWith(".mkv")).Count();
                allFiles += myprogressBar.Maximum;
                allSerie++;

                allT.Add(new Thread(() => {
                    var season = CreaStagione(llock, folder, llock);
                    if (season != null)
                    {
                        serie.Seasons.Add(season);
                        callbackAdded(null, new SeasonsAddedEventArgs(Path.GetFileName(folder), serie));
                        allComplietedSerie++;
                    }
                })); // varie stagioni da varie serie;
                allT.Last().Start();
                allSerie++;
            }

            SetUpFinalThread();
        }
        #endregion
    }
}
