﻿using System.Collections.Generic;

namespace SerieControl
{
    public class SerieMyInfo
    {
        public string language { get; set; }

        public string name { get; set; } = "???";
        public string description { get; set; } = "";

        
        public List<Person> characters { get; set; } //da sistemare: una personaggio ha si una persona realte ma cìè differenza tra un attore, il doppiatore, poi c'è la storia della persona reale e quella del personaggio
        public List<Person> directors { get; set; }
        public List<Person> writers { get; set; }
        public List<Person> dubbers { get; set; }

        public List<Person> executiveProducers { get; set; }
        public List<Location> locations { get; set; }
        
    }
}