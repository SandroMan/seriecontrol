﻿using Microsoft.WindowsAPICodePack.Dialogs;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net.Http;
using Newtonsoft.Json;
using System.Net.Http.Headers;
using System.Web;
using System.Diagnostics;
using System.Speech.Recognition;

namespace SerieControl
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            /*
            Visione visione = new Visione(@"C:\Users\SM\Desktop\Serie Animate\OnePunchMan\ita\OnePunchMan_Ep01_it_QqofCjGCG12i8V6km.mp4");
            visione.Show();
            */

            listBoxSerie.DrawMode = DrawMode.OwnerDrawVariable;
            listBoxSerie.DrawItem += listBoxSerie_DrawItem;

            listBoxSeason.DrawMode = DrawMode.OwnerDrawVariable;
            listBoxSeason.DrawItem += listBoxSeason_DrawItem;

            listBoxEp.DrawMode = DrawMode.OwnerDrawVariable;
            listBoxEp.DrawItem += listBoxEp_DrawItem;

            foreach (var control in this.Controls)
            {
                ((Control)control).GotFocus += Form1_GotFocus;
            }

            Settings.saveStuffTimer = timerSaveStuff;
        }

        private void Form1_GotFocus(object sender, EventArgs e)
        {
            if(Settings.refreshFocus)
                RefreshAll();
        }

        #region Current
        public Serie CurrentSerie
        {
            get { return listBoxSerie.SelectedItem == null ? null : series.Where(serie => serie.name == listBoxSerie.SelectedItem.ToString()).ToArray()[0]; }
        }

        public Season CurrentSeason
        {
            get { return listBoxSeason.SelectedItem == null ? null : CurrentSerie.Seasons.Where(seas => seas.name == listBoxSeason.SelectedItem.ToString()).ToArray()[0]; }
        }

        public Episode CurrentEpisode
        {
            get { return listBoxEp.SelectedItem == null ? null : CurrentSeason.Episodes.Where(ep => ep.titolo == listBoxEp.SelectedItem.ToString()).ToArray()[0]; }
        }
        #endregion

        enum Types
        {
            Null,
            Serie,
            Season,
            Episode
        }
        public static List<Serie> series { get; set; } = new List<Serie>();

        #region IO
        private void Form1_Load(object sender, EventArgs e)
        {
            GetIMPORTANTInfo();
            GetInfo();
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            SaveImportantInfo();

            if (Settings.saveExit)
                SaveInfo();
        }

        #region IMPORTANT
        static byte[] vectorIMP = Encoding.UTF8.GetBytes("GumNJCnCnzmrZ§2");
        static byte[] keyIMP = new UnicodeEncoding().GetBytes("rGm0eAz");
        static string pathIMPORTANT = AppDomain.CurrentDomain.BaseDirectory + "info.info";
        void GetIMPORTANTInfo(string path = null)
        {
            if (path == null)
                path = pathIMPORTANT;

            try
            {
                using (var stream = new FileStream(path, FileMode.Open))
                {
                    using (CryptoStream cs = new CryptoStream(stream, new RijndaelManaged().CreateDecryptor(key, vector), CryptoStreamMode.Read))
                    {
                        using (var reader = new BinaryReader(cs))
                        {
                            string date = reader.ReadString();
                            timeOfToken = DateTime.ParseExact(date, "dd/MM/yyyy HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
                            token = reader.ReadString();
                            if (token == "null")
                                token = null;

                            path1 = reader.ReadString();
                            path2 = reader.ReadString();
                            path3 = reader.ReadString();
                            path4 = reader.ReadString();
                            path5 = reader.ReadString();
                            path6 = reader.ReadString();

                            string type = reader.ReadString();
                            if(type != "null")
                                Settings.cultureVR = new System.Globalization.CultureInfo(type);
                            Settings.saveExit = reader.ReadBoolean();
                            Settings.autoSaveTimer = reader.ReadBoolean();
                            if (Settings.autoSaveTimer)
                                Settings.saveStuffTimer.Start();
                            Settings.refreshFocus = reader.ReadBoolean();
                            Settings.noEpisodebutAdd = reader.ReadBoolean();



                            Visione.volume = reader.ReadByte();
                            Visione.Precisino = reader.ReadBoolean();
                            Visione.VoiceRecognition = reader.ReadBoolean();
                            Visione.saveEpEndNext = reader.ReadBoolean();

                            InfoName.type = reader.ReadString();
                            InfoName.checkcorrupted = reader.ReadBoolean();
                            int count = reader.ReadInt32();
                            InfoName.SE.Clear();
                            for (int i = 0; i < count; i++)
                            {
                                InfoName.SE.Add(reader.ReadString());
                            }
                        }
                    }
                }
            }
            catch
            {
                if (!path.EndsWith(".bak") && File.Exists(path + ".bak")) // provo con il backup
                {
                    GetIMPORTANTInfo(path + ".bak");
                }
            }
        }

        void SaveImportantInfo(int tried = 0)
        {
            if (tried == 20)
                return;
            
            try
            {
                using (FileStream stream = new FileStream(pathIMPORTANT, FileMode.Create))
                {
                    using (CryptoStream cs = new CryptoStream(stream, new RijndaelManaged().CreateEncryptor(key, vector), CryptoStreamMode.Write))
                    {
                        using (BinaryWriter writer = new BinaryWriter(cs))
                        {
                            writer.Write(timeOfToken.ToString());
                            writer.Write(token == null ? "null" : token);

                            writer.Write(path1);
                            writer.Write(path2);
                            writer.Write(path3);
                            writer.Write(path4);
                            writer.Write(path5);
                            writer.Write(path6);

                            writer.Write(Settings.cultureVR == null ? "null" : Settings.cultureVR.Name);
                            writer.Write(Settings.saveExit);
                            writer.Write(Settings.autoSaveTimer);
                            writer.Write(Settings.refreshFocus);
                            writer.Write(Settings.noEpisodebutAdd);

                            writer.Write(Visione.volume);
                            writer.Write(Visione.Precisino);
                            writer.Write(Visione.VoiceRecognition);
                            writer.Write(Visione.saveEpEndNext);

                            writer.Write(InfoName.type);
                            writer.Write(InfoName.checkcorrupted);
                            writer.Write(InfoName.SE.Count);
                            foreach (var se in InfoName.SE)
                            {
                                writer.Write(se);
                            }
                        }
                    }
                }
                File.Copy(pathIMPORTANT, pathIMPORTANT + ".bak", true);

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                Thread.Sleep(500);
                SaveInfo(tried + 1);
            }
        }
        #endregion

        #region Get e Save
        bool BytesToImage(BinaryReader reader, out Image image)
        {

            //array = (byte[])converter.ConvertTo(image.Clone(), typeof(byte[]));


            int nBytes = reader.ReadInt32();
            if (nBytes != -1)
            {
                byte[] bytes = reader.ReadBytes(nBytes);


                ImageConverter converter = new ImageConverter();
                image = (Image)converter.ConvertFrom(bytes);
                return true;
            }
            else
            {
                reader.ReadBytes(1);
                image = null;
                return false;
            }
        }

        bool ImageToBytes(Image image, out byte[] array)
        {
            if(image == null)
            {
                array = null;
                return false;
            }


            ImageConverter converter = new ImageConverter();
            array = (byte[])converter.ConvertTo(image.Clone(), typeof(byte[]));
            return true;
        }


        string pathInfo = AppDomain.CurrentDomain.BaseDirectory + "serie.info";
        void GetInfo(string path = null)
        {
            if (path == null)
                path = pathInfo;
            
            try
            {
                using (var stream = new FileStream(path, FileMode.Open))
                {
                    using (CryptoStream cs = new CryptoStream(stream, new RijndaelManaged().CreateDecryptor(key, vector), CryptoStreamMode.Read))
                    {
                        using (var reader = new BinaryReader(cs))
                        {
                            int version = reader.ReadInt32();
                            if (version < 0)
                            {
                                MessageBox.Show("Tipo di file sbagliato: non voglio un file con un'unica serie");
                                return;
                            }

                            int nSerie = reader.ReadInt32();
                            for (int i = 0; i < nSerie; i++)
                            {
                                Serie serie = GetSerie(reader, version);
                                listBoxSerie.Items.Add(serie.name);
                                series.Add(serie);
                            }
                        }
                    }
                }
            }
            catch
            {
                if (!path.EndsWith(".bak") && File.Exists(path + ".bak")) // provo con il backup
                {
                    GetInfo(path + ".bak");
                }
            }
        }

        Serie GetSerie(BinaryReader reader, int version, Serie serie = null)
        {
            bool New = serie == null;
            if (serie == null)
                serie = new Serie(reader.ReadString());
            else
                serie.name = reader.ReadString();

            this.Invoke((Action)delegate { this.Text = "Serie Control - Lettura serie: " + serie.name; });
            if (version == 1)
                reader.ReadInt32();

            if(BytesToImage(reader, out Image image))
            {
                serie.preview = image;
            }
            serie.info.description = reader.ReadString();

            int nSeason = reader.ReadInt32();
            for (int j = 0; j < nSeason; j++)
            {
                Season season;
                if(New)
                    season = new Season(reader.ReadString());
                else
                {
                    season = serie.Seasons[j];
                    season.name = reader.ReadString();
                }

                this.Invoke((Action) delegate { this.Text = $"Serie Control - Lettura stagione: {season.name} di {serie.name}"; });
                if (version == 1)
                    reader.ReadInt32();

                if (BytesToImage(reader, out image))
                {
                    season.preview = image;
                }
                season.info.description = reader.ReadString();

                int nEpisode = reader.ReadInt32();
                for (int z = 0; z < nEpisode; z++)
                {
                    Episode episode;
                    if (New)
                        episode = new Episode();
                    else
                        episode = season.Episodes[z];

                    episode.titolo = reader.ReadString();
                    this.Invoke((Action) delegate { this.Text = $"Serie Control - Lettura episodio: {episode.titolo} di stagione {season.name} di {serie.name}"; });
                    if (New)
                        episode.path = reader.ReadString();
                    episode.ID = reader.ReadString();
                    episode.Width = reader.ReadInt32();
                    episode.Height = reader.ReadInt32();
                    episode.fps = reader.ReadDouble();
                    episode.Lenght = reader.ReadDouble();
                    if (New)
                        episode.tempoVisto = reader.ReadDouble();
                    episode.info.description = reader.ReadString();
                    if (version >= 3)
                    {
                        episode.InizioSigla = reader.ReadDouble();
                        episode.FineSigla = reader.ReadDouble();
                        episode.FineVideo = reader.ReadDouble();
                    }

                    if (BytesToImage(reader, out image))
                    {
                        episode.preview = image;
                    }


                    if (New)
                    {
                        int nVis = reader.ReadInt32();
                        for (int v = 0; v < nVis; v++)
                        {
                            episode.visulizzazioni.Add(DateTime.ParseExact(reader.ReadString(), "dd/MM/yyyy HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture));
                        }
                    }
                    
                    if(New)
                        season.Episodes.Add(episode);
                }

                if (New)
                    serie.Seasons.Add(season);
            }
            return serie;
        }

        
        static byte[] vector = Encoding.UTF8.GetBytes("§§CiaoneXD§§"); //16
        static byte[] key = new UnicodeEncoding().GetBytes("affdser"); //14
        void SaveInfo(int tried = 0)
        {
            if (tried == 3)
                return;
            
            try
            {
                using (FileStream stream = new FileStream(pathInfo, FileMode.Create))
                {
                    using (CryptoStream cs = new CryptoStream(stream, new RijndaelManaged().CreateEncryptor(key, vector), CryptoStreamMode.Write))
                    {
                        using (BinaryWriter writer = new BinaryWriter(cs))
                        {
                            writer.Write(3); //versione del file
                            writer.Write(series.Count);
                            foreach (var serie in series)
                            {
                                SaveSerie(writer, serie, tried);
                            }
                            this.Invoke((Action) delegate { this.Text = "Serie Control - Fine scrittura delle serie"; });
                        }
                    }
                }
                File.Copy(pathInfo, pathInfo + ".bak", true);
                
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                Thread.Sleep(500);
                SaveInfo(tried + 1);
            }
        }

        void SaveSerie(BinaryWriter writer, Serie serie, int tried)
        {
            this.Invoke((Action) delegate { this.Text = $"Serie Control - [{tried}]Scrittura serie: " + serie.name; });
            writer.Write(serie.name);
            if (ImageToBytes(serie.preview, out byte[] bytes))
            {
                writer.Write(bytes.Length);
                writer.Write(bytes);
            }
            else
            {
                writer.Write(-1);
                writer.Write(new byte[] { 1 });
            }
            writer.Write(serie.info.description);

            writer.Write(serie.Seasons.Count);
            foreach (var season in serie.Seasons)
            {
                this.Invoke((Action) delegate { this.Text = $"Serie Control - [{tried}]Scrittura stagione: {season.name} di {serie.name}"; });
                writer.Write(season.name);
                if (ImageToBytes(season.preview, out bytes))
                {
                    writer.Write(bytes.Length);
                    writer.Write(bytes);
                }
                else
                {
                    writer.Write(-1);
                    writer.Write(new byte[] { 1 });
                }
                writer.Write(season.info.description);

                writer.Write(season.Episodes.Count);
                foreach (var ep in season.Episodes)
                {
                    this.Invoke((Action) delegate { this.Text = $"Serie Control - [{tried}]Scrittura episodio: {ep.titolo} di stagione {season.name} di {serie.name}"; });

                    writer.Write(ep.titolo);
                    if(tried != -1)
                        writer.Write(ep.path);
                    writer.Write(ep.ID);
                    writer.Write(ep.Width);
                    writer.Write(ep.Height);
                    writer.Write(ep.fps);
                    writer.Write(ep.Lenght);
                    if (tried != -1)
                        writer.Write(ep.tempoVisto);
                    writer.Write(ep.info.description);
                    writer.Write(ep.InizioSigla);
                    writer.Write(ep.FineSigla);
                    writer.Write(ep.FineVideo);
                    
                    if (ImageToBytes(ep.preview, out bytes))
                    {
                        writer.Write(bytes.Length);
                        writer.Write(bytes);
                    }
                    else
                    {
                        writer.Write(-1);
                        writer.Write(new byte[] { 1 });
                    }

                    if(tried != -1)
                    {
                        writer.Write(ep.visulizzazioni.Count);
                        foreach (var vis in ep.visulizzazioni)
                        {
                            writer.Write(vis.ToString());
                        }
                    }

                    //this.Invoke((Action) delegate { this.Text = $"Serie Control - [{tried}]Fine scrittura episodio: {ep.titolo} di stagione {season.name} di {serie.name}"; });
                }
            }
        }
        #endregion

        private void buttonSave_Click(object sender, EventArgs e)
        {
            new Thread(() =>
            {
                SaveImportantInfo();
                SaveInfo();
            }).Start();
        }



        private void esportaFilesSerieToolStripMenuItem_Click(object sender, EventArgs e)
        {
            esportaFilesSerieToolStripMenuItem.Enabled = false;
            new Thread(() =>
            {
                Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory + "series");
                foreach (var serie in series)
                {
                    string path = AppDomain.CurrentDomain.BaseDirectory + "series\\" + serie.name + ".serie";

                    #region scrittura
                    try
                    {
                        using (FileStream stream = new FileStream(path, FileMode.Create))
                        {
                            using (CryptoStream cs = new CryptoStream(stream, new RijndaelManaged().CreateEncryptor(key, vector), CryptoStreamMode.Write))
                            {
                                using (BinaryWriter writer = new BinaryWriter(cs))
                                {
                                    writer.Write(-3); //versione del file
                                    this.Invoke((Action) delegate { this.Text = "Serie Control - Scrittura serie: " + serie.name; });
                                    SaveSerie(writer, serie, -1);
                                }
                            }
                        }

                        File.Copy(path, path + ".bak", true);
                        this.Invoke((Action) delegate { this.Text = "Serie Control - Fine scrittura delle serie"; });
                    }
                    catch
                    {
                        this.Invoke((Action) delegate { this.Text = "Serie Control - Errore durante la scrittura della serie:" + serie.name; });
                    }
                    #endregion
                }
                this.Invoke((Action)delegate {
                    esportaFilesSerieToolStripMenuItem.Enabled = true;
                });
            }).Start();
        }

        string path6 = "";
        private void importaFileInfoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //selezione
            OpenFileDialog dialog = new OpenFileDialog();
            
            dialog.Filter = "Serie file (*.serie)|*.serie;*.serie.bak"; //.serie
            dialog.InitialDirectory = path6;

            if (dialog.ShowDialog() == DialogResult.OK)
            {
                path6 = dialog.FileName;
                Serie selected = CurrentSerie;

                new Thread(() =>
                {
                    Serie serie;
                    using (var stream = new FileStream(dialog.FileName, FileMode.Open))
                    {
                        using (CryptoStream cs = new CryptoStream(stream, new RijndaelManaged().CreateDecryptor(key, vector), CryptoStreamMode.Read))
                        {
                            using (var reader = new BinaryReader(cs))
                            {
                                int version = reader.ReadInt32();
                                if (version > 0)
                                {
                                    this.Invoke((Action) delegate { this.Text = "Serie Control - Tipo di file sbagliato: non voglio il file con tutte le serie"; });
                                    return;
                                }

                                selected = GetSerie(reader, Math.Abs(version), selected);
                            }
                        }
                    }

                    this.Invoke((Action) delegate {
                        this.Text = "Serie Control - Fine lettura della serie";
                        RefreshAll();
                    });
                }).Start();
            }
        }
        #endregion

        #region Control
        string path1 = "";
        AddInfo addInfo = null;
        void AggiungiSerie()
        {
            CommonOpenFileDialog dialog = new CommonOpenFileDialog();
            dialog.IsFolderPicker = true;
            dialog.Multiselect = true;
            dialog.InitialDirectory = path1;

            if (dialog.ShowDialog() == CommonFileDialogResult.Ok)
            {
                path1 = dialog.FileNames.ToArray()[0];
                //addToolStripMenuItem.Enabled = false;


                if(addInfo == null)
                {
                    addInfo = new AddInfo(dialog.FileNames.ToArray(), listBoxSerie);
                    addInfo.Show();
                    addInfo.FormClosed += AddInfo_FormClosed;
                }
                else
                {
                    addInfo.AddSeriePaths(dialog.FileNames.ToArray());
                }
            }
        }

        private void AddInfo_FormClosed(object sender, FormClosedEventArgs e)
        {
            addInfo = null;
        }

        List<string> serieLocked = new List<string>();
        string path2 = "";
        void AggiungiStagione()
        {
            CommonOpenFileDialog dialog = new CommonOpenFileDialog();
            dialog.IsFolderPicker = true;
            dialog.Multiselect = true;
            dialog.InitialDirectory = path2;

            if (dialog.ShowDialog() == CommonFileDialogResult.Ok)
            {
                path2 = dialog.FileNames.ToArray()[0];
                //addToolStripMenuItem.Enabled = false;
                lock (serieLocked)
                {
                    if(!serieLocked.Contains(CurrentSerie.name))
                        serieLocked.Add(CurrentSerie.name);
                }

                if (addInfo == null)
                {
                    addInfo = new AddInfo(dialog.FileNames.ToArray(), CurrentSerie, SeasonAddedEvent);
                    addInfo.Show();
                    addInfo.FormClosed += AddInfo_FormClosed;
                }
                else
                {
                    addInfo.AddSeasonPaths(dialog.FileNames.ToArray(), CurrentSerie, SeasonAddedEvent);
                }
            }
        }

        private void SeasonAddedEvent(object sender, SeasonsAddedEventArgs e)
        {
            lock (serieLocked)
            {
                serieLocked.Remove(e.serie.name);
            }

            this.Invoke((Action)delegate {
                if (e.serie.name == listBoxSerie.SelectedItem.ToString())
                    listBoxSeason.Items.Add(e.SeasonName);
            });
        }
        
        #endregion

        #region ListBox
        void RefreshWidth(ListBox listBox)
        {
            int width = 0;
            //Create a graphics.
            Graphics g = listBox.CreateGraphics();
            //Measure the string of each item and select the max width.
            foreach (object item in listBox.Items)
            {
                string text = item.ToString();
                SizeF s = g.MeasureString(text, listBox.Font);
                if (s.Width > width)
                    width = (int)s.Width;
            }
            //Set the horizontal extent to the width plus the offset.
            listBox.HorizontalExtent = width + 2;
        }

        private void listBoxSerie_SelectedIndexChanged(object sender, EventArgs e)
        {
            listBoxSeason.Items.Clear();
            listBoxEp.Items.Clear();

            if (listBoxSerie.SelectedIndex == -1)
                return;
            
            if (CurrentSerie.Seasons.Count == 0)
                return;

            foreach (var season in CurrentSerie.Seasons)
            {
                listBoxSeason.Items.Add(season.name);
            }

            int daV = CurrentSerie.DaVedere;
            listBoxSeason.SelectedIndex = daV == -1 ? listBoxSeason.Items.Count - 1 : daV;
            pictureBox1.Image = CurrentSerie.preview;
            textBoxTitle.Text = CurrentSerie.name;
            textBoxPath.Text = "";
            textBoxInfo.Text = "";
            listBoxVisulizzazioni.Items.Clear();
            //textBoxInfo.Text = $"Seasons: {serie.IndexSeason + 1}/{serie.Seasons.Count}";
            textBoxDescription.Text = CurrentSerie.info.description;
            currentType = Types.Serie;

            RefreshWidth(listBoxSeason);
        }

        private void listBoxSeason_SelectedIndexChanged(object sender, EventArgs e)
        {
            listBoxEp.Items.Clear();

            if (listBoxSeason.SelectedIndex == -1)
                return;
            
            /*
            Season season;
            try
            {
                season = CurrentSeason;
            }
            catch
            {
                return;
            }
            */


            if (CurrentSeason.Episodes.Count != 0)
            {
                foreach (var ep in CurrentSeason.Episodes)
                {
                    listBoxEp.Items.Add(ep.titolo);
                }
            }

            int daV = CurrentSeason.DaVedere;
            listBoxEp.SelectedIndex = daV == -1 ? listBoxEp.Items.Count - 1 : daV;
            pictureBox1.Image = CurrentSeason.preview;
            textBoxTitle.Text = CurrentSeason.name;
            textBoxPath.Text = "";
            textBoxInfo.Text = "";
            listBoxVisulizzazioni.Items.Clear();
            //textBoxInfo.Text = $"Episode: {CurrentSeason.IndexEpisode + 1}/{CurrentSeason.Episodes.Count}";
            textBoxDescription.Text = CurrentSeason.info.description;
            currentType = Types.Season;

            RefreshWidth(listBoxEp);
        }
        
        private void listBoxEp_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listBoxEp.SelectedIndex == -1)
                return;

            if (CurrentEpisode == null)
                return;

            pictureBox1.Image = CurrentEpisode.preview;
            textBoxTitle.Text = CurrentEpisode.titolo;
            textBoxPath.Text = CurrentEpisode.path;
            textBoxInfo.Text = $"Size: {CurrentEpisode.Width}x{CurrentEpisode.Height}\r\nFps: {CurrentEpisode.fps}\r\nDurata: {CurrentEpisode.BetterTempoVisto}/{CurrentEpisode.BetterFineVideo}[{CurrentEpisode.BetterLenght}]\r\nSigla: Inizio:{CurrentEpisode.BetterInizioSigla}   Fine:{CurrentEpisode.BetterFineSigla}\r\nID: {CurrentEpisode.ID}";
            listBoxVisulizzazioni.Items.Clear();
            if(CurrentEpisode.visulizzazioni.Count == 0)
            {
                listBoxVisulizzazioni.Items.Add("Mai visto");
            }
            else
            {
                foreach (var date in CurrentEpisode.visulizzazioni)
                {
                    listBoxVisulizzazioni.Items.Add(date.ToString());
                }
            }
            textBoxDescription.Text = CurrentEpisode.info.description;
            currentType = Types.Episode;
        }
        


        private void listBoxSerie_DrawItem(object sender, DrawItemEventArgs e)
        {
            if (e.Index == -1)
            {
                e.DrawBackground();
                return;
            }

            var allS = series.Where(ser => ser.name == listBoxSerie.Items[e.Index].ToString()).ToArray();
            if (allS.Count() == 0)
                return;

            var serie = allS[0];

            bool selected = (e.State & DrawItemState.Selected) == DrawItemState.Selected;
            bool red = true;
            foreach (var seas in serie.Seasons)
            {
                foreach (var ep in seas.Episodes)
                {
                    if (ep.Exist)
                    {
                        red = false;
                        break;
                    }
                }
            }

            if (red)
            {
                if (selected)
                    e.Graphics.FillRectangle(new SolidBrush(Color.DarkRed), e.Bounds);
                else
                    e.Graphics.FillRectangle(new SolidBrush(Color.Red), e.Bounds);
            }
            else if (serie.Completata)
            {
                if (selected)
                    e.Graphics.FillRectangle(new SolidBrush(Color.Green), e.Bounds);
                else
                    e.Graphics.FillRectangle(new SolidBrush(Color.LightGreen), e.Bounds);
            }
            else if (serie.Iniziata)
            {
                if (selected)
                    e.Graphics.FillRectangle(new SolidBrush(Color.FromArgb(255, 128, 0)), e.Bounds);
                else
                    e.Graphics.FillRectangle(new SolidBrush(Color.FromArgb(255, 192, 128)), e.Bounds);
            }
            else
            {
                if (selected)
                    e.Graphics.FillRectangle(new SolidBrush(Color.FromArgb(51, 153, 255)), e.Bounds);
                else
                    e.Graphics.FillRectangle(new SolidBrush(Color.White), e.Bounds);
            }
            e.Graphics.DrawString(((ListBox)sender).Items[e.Index].ToString(), e.Font, new SolidBrush(e.ForeColor), new PointF(e.Bounds.X, e.Bounds.Y));
        }

        private void listBoxSeason_DrawItem(object sender, DrawItemEventArgs e)
        {
            if (e.Index == -1)
            {
                e.DrawBackground();
                return;
            }

            Season season;
            try
            {
                season = CurrentSerie.Seasons.Where(seas => seas.name == listBoxSeason.Items[e.Index].ToString()).ToArray()[0];
            }
            catch
            {
                return;
            }

            if (season == null)
                return;

            bool selected = (e.State & DrawItemState.Selected) == DrawItemState.Selected;

            bool red = true;
            foreach (var ep in season.Episodes)
            {
                if(ep.Exist)
                {
                    red = false;
                    break;
                }
            }

            if (red)
            {
                if (selected)
                    e.Graphics.FillRectangle(new SolidBrush(Color.DarkRed), e.Bounds);
                else
                    e.Graphics.FillRectangle(new SolidBrush(Color.Red), e.Bounds);
            }
            else if (season.Completata)
            {
                if (selected)
                    e.Graphics.FillRectangle(new SolidBrush(Color.Green), e.Bounds);
                else
                    e.Graphics.FillRectangle(new SolidBrush(Color.LightGreen), e.Bounds);
            }
            else if (season.Iniziata)
            {
                if (selected)
                    e.Graphics.FillRectangle(new SolidBrush(Color.FromArgb(255, 128, 0)), e.Bounds);
                else
                    e.Graphics.FillRectangle(new SolidBrush(Color.FromArgb(255, 192, 128)), e.Bounds);
            }
            else
            {
                if (selected)
                    e.Graphics.FillRectangle(new SolidBrush(Color.FromArgb(51, 153, 255)), e.Bounds);
                else
                    e.Graphics.FillRectangle(new SolidBrush(Color.White), e.Bounds);
            }
            e.Graphics.DrawString(((ListBox)sender).Items[e.Index].ToString(), e.Font, new SolidBrush(e.ForeColor), new PointF(e.Bounds.X, e.Bounds.Y));
        }

        private void listBoxEp_DrawItem(object sender, DrawItemEventArgs e)
        {
            if (e.Index == -1)
            {
                e.DrawBackground();
                return;
            }

            Episode episode;
            try
            {
                episode = CurrentSeason.Episodes.Where(ep => ep.titolo == listBoxEp.Items[e.Index].ToString()).ToArray()[0];
            }
            catch
            {
                return;
            }
            //e.DrawBackground(); //fa anche il selected

            bool selected = (e.State & DrawItemState.Selected) == DrawItemState.Selected;
            if(!episode.Exist)
            {
                if (selected)
                    e.Graphics.FillRectangle(new SolidBrush(Color.DarkRed), e.Bounds);
                else
                    e.Graphics.FillRectangle(new SolidBrush(Color.Red), e.Bounds);
            }
            else if (episode.visto)
            {
                if (selected)
                    e.Graphics.FillRectangle(new SolidBrush(Color.Green), e.Bounds);
                else
                    e.Graphics.FillRectangle(new SolidBrush(Color.LightGreen), e.Bounds);
            }
            else if (episode.tempoVisto != 0)
            {
                if (selected)
                    e.Graphics.FillRectangle(new SolidBrush(Color.FromArgb(255, 128, 0)), e.Bounds);
                else
                    e.Graphics.FillRectangle(new SolidBrush(Color.FromArgb(255, 192, 128)), e.Bounds);
            }
            else
            {
                if (selected)
                    e.Graphics.FillRectangle(new SolidBrush(Color.FromArgb(51, 153, 255)), e.Bounds);
                else
                    e.Graphics.FillRectangle(new SolidBrush(Color.White), e.Bounds);
            }
            e.Graphics.DrawString(((ListBox)sender).Items[e.Index].ToString(), e.Font, new SolidBrush(e.ForeColor), new PointF(e.Bounds.X, e.Bounds.Y));
        }



        private void listBoxEp_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Up && e.Shift)
            {
                int ind = listBoxEp.SelectedIndex;
                if (ind == 0)
                    return;

                List<Episode> episodi = CurrentSeason.Episodes;
                Episode ep = episodi[ind];
                if (ep.ID == ind + 1 + "")
                    ep.ID = ind + "";
                episodi.RemoveAt(ind);
                episodi.Insert(ind - 1, ep);


                listBoxEp.Items.RemoveAt(ind);
                listBoxEp.Items.Insert(ind - 1, ep.ToString());
            }
            else if (e.KeyCode == Keys.Down && e.Shift)
            {
                int ind = listBoxEp.SelectedIndex;
                if (ind == listBoxEp.Items.Count - 1)
                    return;

                List<Episode> episodi = CurrentSeason.Episodes;
                Episode ep = episodi[ind];
                if (ep.ID == ind + 1 + "")
                    ep.ID = ind + 2 + "";
                episodi.RemoveAt(ind);
                episodi.Insert(ind + 1, ep);

                listBoxEp.Items.RemoveAt(ind);
                listBoxEp.Items.Insert(ind + 1, ep.ToString());
                listBoxEp.SelectedIndex = ind++;

            }
            else if (e.KeyCode == Keys.Left && e.Shift)
            {
                listBoxSeason.Focus();
                listBoxSeason_SelectedIndexChanged(null, null);
                e.SuppressKeyPress = true;
            }
        }

        private void listBoxSeason_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Up && e.Shift)
            {
                int ind = listBoxSeason.SelectedIndex;
                if (ind == 0)
                    return;

                List<Season> stagioni = CurrentSerie.Seasons;
                Season ses = stagioni[ind];
                stagioni.RemoveAt(ind);
                stagioni.Insert(ind - 1, ses);

                listBoxSeason.Items.RemoveAt(ind);
                listBoxSeason.Items.Insert(ind - 1, ses.ToString());
            }
            else if (e.KeyCode == Keys.Down && e.Shift)
            {
                int ind = listBoxSeason.SelectedIndex;
                if (ind == listBoxSeason.Items.Count - 1)
                    return;

                List<Season> stagioni = CurrentSerie.Seasons;
                Season ses = stagioni[ind];
                stagioni.RemoveAt(ind);
                stagioni.Insert(ind + 1, ses);

                listBoxSeason.Items.RemoveAt(ind);
                listBoxSeason.Items.Insert(ind + 1, ses.ToString());
                listBoxSeason.SelectedIndex = ind++;
            }
            else if (e.KeyCode == Keys.Left && e.Shift)
            {
                listBoxSerie.Focus();
                listBoxSerie_SelectedIndexChanged(null, null);
                e.SuppressKeyPress = true;
            }
            if (e.KeyCode == Keys.Right && e.Shift)
            {
                listBoxEp.Focus();
                listBoxEp_SelectedIndexChanged(null, null);
                e.SuppressKeyPress = true;
            }
        }

        private void listBoxSerie_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Up && e.Shift)
            {
                int ind = listBoxSerie.SelectedIndex;
                if (ind == 0)
                    return;

                Serie ser = series[ind];
                series.RemoveAt(ind);
                series.Insert(ind - 1, ser);

                listBoxSerie.Items.RemoveAt(ind);
                listBoxSerie.Items.Insert(ind - 1, ser.ToString());
            }
            else if (e.KeyCode == Keys.Down && e.Shift)
            {
                int ind = listBoxSerie.SelectedIndex;
                if (ind == listBoxEp.Items.Count - 1)
                    return;

                Serie ser = series[ind];
                series.RemoveAt(ind);
                series.Insert(ind + 1, ser);

                listBoxSerie.Items.RemoveAt(ind);
                listBoxSerie.Items.Insert(ind + 1, ser.ToString());
                listBoxSerie.SelectedIndex = ind++;
            }
            else if (e.KeyCode == Keys.Right && e.Shift)
            {
                listBoxSeason.Focus();
                listBoxSeason_SelectedIndexChanged(null, null);
                e.SuppressKeyPress = true;
            }
        }
        
        private void listBoxSerie_DoubleClick(object sender, EventArgs e)
        {
            Visione visione = new Visione(CurrentSerie);
            visione.Show();
        }

        private void listBoxSeason_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            Visione visione = new Visione(CurrentSeason);
            visione.Show();
        }

        private void listBoxEp_DoubleClick(object sender, EventArgs e)
        {
            Visione visione = new Visione(CurrentEpisode);
            visione.Show();
        }
        #endregion

        #region Edit
        bool editMode = false;
        private void buttonEdit_Click(object sender, EventArgs e)
        {
            if(editMode)
            {
                buttonEdit.Text = "Edit";
                editMode = false;

                textBoxTitle.ReadOnly = true;
                textBoxPath.ReadOnly = true;
                buttonSelectpath.Enabled = false;
                textBoxDescription.ReadOnly = true;
            }
            else
            {
                buttonEdit.Text = "Stop Edit";
                editMode = true;

                textBoxTitle.ReadOnly = false;
                textBoxPath.ReadOnly = false;
                buttonSelectpath.Enabled = true;
                textBoxDescription.ReadOnly = false;
            }
        }

        string path3 = "";
        Types currentType = Types.Null;
        private void pictureBox1_Click(object sender, EventArgs e)
        {
            if(editMode && currentType != Types.Null)
            {
                OpenFileDialog dialog = new OpenFileDialog();
                dialog.Filter = "Image Files (*.png, *.bmp, *.jpg)|*.png;*.bmp;*.jpg";
                dialog.InitialDirectory = path3;

                if (dialog.ShowDialog() == DialogResult.OK)
                {
                    path3 = dialog.FileName;
                    Image image = Image.FromFile(dialog.FileName);

                    switch (currentType)
                    {
                        case Types.Serie:

                            CurrentSerie.preview = image;
                            /*
                            foreach (var ind in listBoxSerie.SelectedIndices)
                            {
                                series[ind].preview = image;
                            }*/
                            break;
                        case Types.Season:
                            CurrentSeason.preview = image;
                            break;
                        case Types.Episode:
                            CurrentEpisode.preview = image;
                            break;
                        default:
                            break;
                    }

                    pictureBox1.Image = image;
                }
            }
        }

        private void textBoxTitle_Leave(object sender, EventArgs e)
        {
            if (!editMode)
                return;

            switch (currentType)
            {
                case Types.Serie:
                    CurrentSerie.name = textBoxTitle.Text;
                    listBoxSerie.Items[listBoxSerie.SelectedIndex] = textBoxTitle.Text;
                    RefreshWidth(listBoxSerie);
                    break;
                case Types.Season:
                    CurrentSeason.name = textBoxTitle.Text;
                    listBoxSeason.Items[listBoxSeason.SelectedIndex] = textBoxTitle.Text;
                    RefreshWidth(listBoxSeason);
                    break;
                case Types.Episode:
                    CurrentEpisode.titolo = textBoxTitle.Text;
                    listBoxEp.Items[listBoxEp.SelectedIndex] = textBoxTitle.Text;
                    RefreshWidth(listBoxEp);
                    break;
                default:
                    break;
            }
        }

        private void textBoxPath_Leave(object sender, EventArgs e)
        {
            if (!editMode)
                return;

            switch (currentType)
            {
                case Types.Episode:
                    CurrentEpisode.path = textBoxPath.Text;
                    break;
                default:
                    break;
            }
        }

        string path4 = "";
        private void buttonSelectpath_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "Video Files (*.avi, *.mp4, *.flv, *.wmv, *.mkv)|*.avi;*.mp4;*.flv;*.wmv;*.mkv";
            dialog.InitialDirectory = path4;

            if (dialog.ShowDialog() == DialogResult.OK)
            {
                path4 = dialog.FileName;
                CurrentEpisode.path = dialog.FileName;
            }
        }

        private void textBoxDescription_Leave(object sender, EventArgs e)
        {
            if (!editMode)
                return;

            switch (currentType)
            {
                case Types.Serie:
                    CurrentSerie.info.description = textBoxDescription.Text;
                    break;
                case Types.Season:
                    CurrentSeason.info.description = textBoxDescription.Text;
                    break;
                case Types.Episode:
                    CurrentEpisode.info.description = textBoxDescription.Text;
                    break;
                default:
                    break;
            }
        }
        #endregion

        #region contextMenuStrip
        void RefreshAll()
        {
            listBoxSerie.Refresh();
            listBoxSeason.Refresh();
            listBoxEp.Refresh();
        }

        #region Global
        private void updateListsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            RefreshAll();
        }
        private void settingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Settings settings = new Settings();
            settings.Show();
        }
        private void infoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new Info().Show();
        }
        #endregion

        #region Serie
        private void addToolStripMenuItem_Click(object sender, EventArgs e)
        {
            contextMenuStripSerie.Hide();
            AggiungiSerie();
        }

        private void addEmptyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            series.Add(new Serie("Nuova serie"));
            listBoxSerie.Items.Add("Nuova serie");
        }

        private void removeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int bak = listBoxSerie.SelectedIndex;

            lock (serieLocked)
            {
                if (serieLocked.Contains(CurrentSerie.name))
                {
                    MessageBox.Show("Non puoi eliminare una serie in cui stai aggiungendo una stagione");
                    return;
                    /*
                    foreach (var T in TinUse)
                    {
                        T.Abort();
                    }

                    label2.Text = "Aggiunta bloccata per la rimozione della serie";
                    inUse = null;
                    progressBar2.Value = 0;
                    */
                }
            }

            series.RemoveAt(listBoxSerie.SelectedIndex);
            listBoxSerie.Items.RemoveAt(listBoxSerie.SelectedIndex);

            if (listBoxSerie.SelectedIndex == -1)
            {
                if (listBoxSerie.Items.Count == 0)
                {
                    listBoxEp.Items.Clear();
                    pictureBox1.Image = null;
                    textBoxTitle.Text = "";
                    textBoxPath.Text = "";
                    textBoxInfo.Text = "";
                    listBoxVisulizzazioni.Items.Clear();
                    textBoxDescription.Text = "";
                }
                else
                {
                    if (bak < listBoxSerie.Items.Count)
                        listBoxSerie.SelectedIndex = bak;
                    else
                        listBoxSerie.SelectedIndex = listBoxSerie.Items.Count - 1;
                }
            }
        }
        private void addEmptyToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            CurrentSerie.Seasons.Add(new Season("Nuova stagione"));
            listBoxSeason.Items.Add("Nuova stagione");
        }

        private void vistoToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            CurrentSerie.Completata = true;
            contextMenuStripSerie.Hide();
            RefreshAll();
        }

        private void vistoFalseToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            CurrentSerie.Completata = false;
            RefreshAll();
        }

        #region Get Info Internet
        private HttpClient client = new HttpClient();

        string PostHttp(string url, StringContent content)
        {
            var response = client.PostAsync(url, content).ConfigureAwait(false);
            return response.GetAwaiter().GetResult().Content.ReadAsStringAsync().Result;
        }

        string GetHttp(string url)
        {
            var response = client.GetAsync(url).ConfigureAwait(false);
            return response.GetAwaiter().GetResult().Content.ReadAsStringAsync().Result;
        }


        private void getInfoFromInternetToolStripMenuItem_Click(object sender, EventArgs e)
        {
            /*
            using (var client = new HttpClient())
            {
                var response = client.GetAsync("http://google.com").Result;

                if (response.IsSuccessStatusCode)
                {
                    var responseContent = response.Content;

                    // by calling .Result you are synchronously reading the result
                    string responseString = responseContent.ReadAsStringAsync().Result;

                    Console.WriteLine(responseString);
                }
            }
            */
            GetInternetSerieInfo(CurrentSerie);
            /*
            string urlAddress = "https://www.imdb.com/title/tt0436992/";

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(urlAddress);
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            
            HtmlDocument document = new HtmlDocument();
            string htmlString = "<html>blabla</html>";
            document.LoadHtml(htmlString);
            HtmlNodeCollection collection = document.DocumentNode.SelectNodes("//a");
            foreach (HtmlNode link in collection)
            {
                string target = link.Attributes["href"].Value;
            }


            if (response.StatusCode == HttpStatusCode.OK)
            {
                Stream receiveStream = response.GetResponseStream();
                StreamReader readStream = null;

                if (response.CharacterSet == null)
                {
                    readStream = new StreamReader(receiveStream);
                }
                else
                {
                    readStream = new StreamReader(receiveStream, Encoding.GetEncoding(response.CharacterSet));
                }

                string data = readStream.ReadToEnd();

                response.Close();
                readStream.Close();
            }
            */

        }
        static DateTime timeOfToken; //da salvare
        static string token; //da salvare
        private void GetInternetSerieInfo(Serie serie)
        {
            string responseString;
            if (token == null || DateTime.Now > timeOfToken.AddDays(1))
            {
                timeOfToken = DateTime.Now;
                var content = new StringContent("{  \"apikey\": \"KSCQOS9W4B5PVJRL\",  \"userkey\": \"XAYM87YI6W9Q5440\",  \"username\": \"h2848846cty\"}", Encoding.UTF8, "application/json");
                responseString = PostHttp("https://api.thetvdb.com/login", content);



                token = JsonConvert.DeserializeObject<Dictionary<string, string>>(responseString).First().Value;
            }

            client.DefaultRequestHeaders.Clear();
            if (client.DefaultRequestHeaders.Authorization == null)
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            }
            client.DefaultRequestHeaders.Add("Accept-Language", "it");

            //client.DefaultRequestHeaders.Add("Accept-Language", "en");

            responseString = GetHttp("https://api.thetvdb.com/search/series?name=" + serie.name.Replace(" ", "%20"));
            Root<SerieInfo> Serie = JsonConvert.DeserializeObject<Root<SerieInfo>>(responseString);

            if (Serie.data == null)
            {
                //client.DefaultRequestHeaders.Clear();
                //client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                //client.DefaultRequestHeaders.Add("Accept-Language", "en");

                //responseString = GetHttp("https://api.thetvdb.com/search/series?name=" + serie.name.Replace(" ", "%20"));
                //Serie = JsonConvert.DeserializeObject<Root<SerieInfo>>(responseString);

                if (Serie.data == null)
                {
                    if (MessageBox.Show("Non è stata trovata nessuna serie con questo nome\nPer avere le informazioni serve il nome preciso, vuoi cercarlo su google ?", "Search on Google Serie Name", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        Process.Start("https://www.google.com/search?q=" + serie.name);
                        Process.Start("https://www.thetvdb.com/search?q=" + serie.name + "&l=en");
                    }
                    return;
                }
            }

            client.DefaultRequestHeaders.Clear();
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            client.DefaultRequestHeaders.Add("Accept-Language", "it");
            RootAll<EpisodeInfo> episode = null;


            int id = Serie.data[0].id;
            responseString = GetHttp("https://api.thetvdb.com/series/" + id + "/episodes");
            episode = JsonConvert.DeserializeObject<RootAll<EpisodeInfo>>(responseString);

            //client.DefaultRequestHeaders.Clear();
            //client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            //client.DefaultRequestHeaders.Add("Accept-Language", "en");


            //responseString = GetHttp("https://api.thetvdb.com/series/" + id + "/episodes");
            //RootAll<EpisodeInfo> episodeEN = JsonConvert.DeserializeObject<RootAll<EpisodeInfo>>(responseString);

            List<EpisodeInfo>[] Episodes = new List<EpisodeInfo>[256];
            byte max = 0;
            for (int i = 0; i < episode.data.Count; i++)
            {
                if (episode.data[i] == null)
                    continue;

                if (Episodes[episode.data[i].airedSeason] == null)
                    Episodes[episode.data[i].airedSeason] = new List<EpisodeInfo>();

                Episodes[episode.data[i].airedSeason].Add(episode.data[i]);

                if (max < episode.data[i].airedSeason)
                    max = (byte)episode.data[i].airedSeason;

                /*
                if(CopyPropertyValues(episodeEN.data[i], episodeIT.data[i]))
                {
                    episodeIT.data[i].language.overview += " en";
                }
                */
            }


            AddSerie(SetupSerieInternet(serie, Episodes.TakeWhile(epi => epi != null).ToArray()));
            MessageBox.Show("fine");
        }

        private Serie SetupSerieInternet(Serie serie, List<EpisodeInfo>[] Episodes)
        {
            //prova a controllare se c'è una coerenza con li episodi che ho in locale e quelli che ho in remoto
            Serie newSerie = new Serie(serie);
            newSerie.Seasons = new List<Season>();



            List<Season> originalSeason = serie.Seasons;

            int indexSeason = 0;
            for (int i = 0; i < Episodes.Length; i++)
            {
                if (indexSeason < originalSeason.Count && Episodes[i].Count == originalSeason[indexSeason].Episodes.Count)
                {
                    newSerie.Seasons.Add(SetEpisodi(originalSeason[indexSeason], null, Episodes[i]));
                    indexSeason++;
                }
                else
                {
                    newSerie.Seasons.Add(SetEpisodi(null, "season " + (i + 1),  Episodes[i]));
                }

            }
            return newSerie;









            /*
            int diff = 0;
            for (int i = 0; i < serie.Seasons.Count; i++)
            {
                if (Episodes[i + diff] == null)
                {
                    i--;
                    diff++;
                    continue;
                }

                Season season = serie.Seasons[i];
                if (season.Episodes.Count != Episodes[i + diff].Count)
                {
                    if (MessageBox.Show($"Local: {season.Episodes.Count}\nRemote:{Episodes[i + diff].Count}\nCreare una stagione con questi dati?", "Season con episodi diversi da quelli previsti", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        Season created = new Season("season " + (i + diff), new Episode[Episodes[i + diff].Count].ToList());
                        SetEpisodi(created, Episodes[i + diff]);
                        serie.Seasons.Insert(i, created);
                    }
                    else
                    {
                        i--;
                        diff++;
                        continue;
                    }

                }
                else
                {
                    SetEpisodi(season, Episodes[i + diff]);
                }
            }
            */
        }

        private Season SetEpisodi(Season season, string newName, List<EpisodeInfo> Episodes)
        {
            Season newSeason;
            if (season == null)
            {
                newSeason = new Season(newName);
            }
            else
            {
                newSeason = new Season(season);
                newSeason.Episodes = new List<Episode>();
            }


            for (int j = 0; j < Episodes.Count; j++)
            {
                Episode newEpisode;
                if (season == null)
                {
                    newEpisode = new Episode();
                }
                else
                {
                    newEpisode = new Episode(season.Episodes[j]);
                }
                

                //if (Episodes[j].overview != null)
                //    newEpisode.info.overview = Episodes[j].overview;

                var responseString = GetHttp("https://api.thetvdb.com/episodes/" + Episodes[j].id);
                Console.WriteLine(responseString);
                RootObject fullEpisode = JsonConvert.DeserializeObject<RootObject>(responseString);
                  //newEpisode.info = fullEpisode.data;

                if (fullEpisode.data.filename != "")
                {
                    Image image = (Image)SaveImage("https://www.thetvdb.com/banners/" + fullEpisode.data.filename);
                    newEpisode.preview = image;
                }


                if (Episodes[j].episodeName != null)
                    newEpisode.titolo = Episodes[j].episodeName;
                else if (newEpisode.titolo == null)
                    newEpisode.titolo = "???";

                newSeason.Episodes.Add(newEpisode);
            }

            return newSeason;
        }

        public static Bitmap SaveImage(string imageUrl)
        {
            using (WebClient client = new WebClient())
            {
                using (Stream stream = client.OpenRead(imageUrl))
                {
                    return new Bitmap(stream);
                }
            }
        }
        #endregion

        private void setStartForSerieToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Episode selected = CurrentEpisode;
            foreach (var seas in CurrentSerie.Seasons)
            {
                foreach (var ep in seas.Episodes)
                {
                    ep.InizioSigla = selected.InizioSigla;
                    ep.FineSigla = selected.FineSigla;
                }
            }
        }

        private void setEndForSerieToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Episode selected = CurrentEpisode;
            foreach (var seas in CurrentSerie.Seasons)
            {
                foreach (var ep in seas.Episodes)
                {
                    ep.FineVideo = selected.FineVideo;
                }
            }
        }

        private void sortToolStripMenuItem_Click(object sender, EventArgs e)
        {
            listBoxSerie.Items.Clear();

            foreach (var ser in series)
            {
                listBoxSerie.Items.Add(ser.name);
            }
        }

        private void sortWithSortDataToolStripMenuItem_Click(object sender, EventArgs e)
        {
            series = series.OrderBy(o => o.name).ToList();
            sortToolStripMenuItem_Click(null, null);
        }

        private void removeDuplicateToolStripMenuItem_Click(object sender, EventArgs e)
        {
            listBoxSerie.Items.Clear();
            List<Serie> dummy = new List<Serie>();
            foreach (var ser in series)
            {
                if (dummy.Where(s => s.name == ser.name).Count() == 0)
                {
                    dummy.Add(ser);
                    listBoxSerie.Items.Add(ser.name);
                }
            }

            series.Clear();
            series.AddRange(dummy);
        }

        private void getInfoFromNameToolStripMenuItem_Click(object sender, EventArgs e)
        {
            InfoName infoName = new InfoName(CurrentSerie);
            infoName.ShowDialog();
            var output = infoName.output;

            if (output != null)
                AddSerie(output);
        }
        #endregion

        void AddSerie(Serie serie)
        {
            int sub;
            for (sub = 1; series.Where(s => s.name == serie.name + "(" + sub + ")").Count() != 0; sub++) { }
            serie.name = serie.name + "(" + sub + ")";

            int i;
            for (i = series.Count - 1; i > 0; i--)
            {
                if (series[i].name.StartsWith(serie.name))
                    break;
            }
            i++;
            series.Insert(i, serie);
            listBoxSerie.Items.Insert(i, serie.ToString());
        }


        #region Season
        private void toolStripMenuItemSeasonAdd_Click(object sender, EventArgs e)
        {
            contextMenuStripSeason.Hide();
            AggiungiStagione();
        }
        private void toolStripMenuItemSeasonRemove_Click(object sender, EventArgs e)
        {
            int bak = listBoxSeason.SelectedIndex;
            CurrentSerie.Seasons.RemoveAt(listBoxSeason.SelectedIndex);
            listBoxSeason.Items.RemoveAt(listBoxSeason.SelectedIndex);
            if (listBoxSeason.SelectedIndex == -1)
            {
                if (listBoxSeason.Items.Count == 0)
                {
                    pictureBox1.Image = null;
                    textBoxTitle.Text = "";
                    textBoxPath.Text = "";
                    textBoxInfo.Text = "";
                    listBoxVisulizzazioni.Items.Clear();
                    textBoxDescription.Text = "";
                }
                else
                {
                    if (bak < listBoxSeason.Items.Count)
                        listBoxSeason.SelectedIndex = bak;
                    else
                        listBoxSeason.SelectedIndex = listBoxSeason.Items.Count - 1;
                }
            }
        }

        private void vistoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CurrentSeason.Completata = true;
            contextMenuStripSeason.Hide();
            RefreshAll();
        }

        private void vistoFalseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CurrentSeason.Completata = false;
            RefreshAll();
        }
        #endregion

        #region Ep

        string path5 = "";
        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "Video Files (*.avi, *.mp4, *.flv, *.wmv, *.mkv)|*.avi;*.mp4;*.flv;*.wmv;*.mkv";
            dialog.InitialDirectory = path5;

            if (dialog.ShowDialog() == DialogResult.OK)
            {
                path5 = dialog.FileName;
                var episodes = CurrentSeason.Episodes;
                episodes.Add(new Episode(dialog.FileName, episodes.Count + ""));
            }
        }

        private void addEmptyToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            CurrentSeason.Episodes.Add(new Episode());
            CurrentSeason.Episodes.Last().NoDATA = true;
            listBoxEp.Items.Add("???");
        }

        private void toolStripMenuItem3_Click(object sender, EventArgs e)
        {
            if (listBoxEp.SelectedIndex == -1)
                return;

            CurrentSeason.Episodes.RemoveAt(listBoxEp.SelectedIndex);
            listBoxEp.Items.RemoveAt(listBoxEp.SelectedIndex);
        }

        private void toolStripMenuItem4_Click(object sender, EventArgs e)
        {
            CurrentEpisode.visto = true;
            contextMenuStripEp.Hide();
            listBoxEp.Refresh();
        }

        private void toolStripMenuItem5_Click(object sender, EventArgs e)
        {
            CurrentEpisode.visto = false;
            contextMenuStripEp.Hide();
            listBoxEp.Refresh();
        }
        #endregion

        #endregion

        private void timerSaveStuff_Tick(object sender, EventArgs e)
        {
            new Thread(() => SaveInfo()).Start();
        }
        
        private void textBoxSearch_TextChanged(object sender, EventArgs e)
        {
            listBoxSerie.Items.Clear();
            foreach (var ser in series)
            {
                if (ser.name.ToLower().Contains(textBoxSearch.Text.ToLower()))
                    listBoxSerie.Items.Add(ser.name);
            }

            if(listBoxSerie.Items.Count != 0)
                listBoxSerie.SelectedIndex = 0;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //string dd = "{\"links\":{\"first\":1,\"last\":1,\"next\":null,\"prev\":null},\"data\":[{\"absoluteNumber\":1,\"airedEpisodeNumber\":1,\"airedSeason\":1,\"airedSeasonID\":494694,\"dvdEpisodeNumber\":1,\"dvdSeason\":1,\"episodeName\":\"Trappola per turisti\",\"firstAired\":\"2012-06-15\",\"id\":4344073,\"language\":{\"episodeName\":\"it\",\"overview\":\"it\"},\"lastUpdated\":1452621600,\"overview\":\"Due fratelli, Dipper e Mabel Pines, vengono mandati a passare l'estate dal loro prozio Stan che abita a Gravity Falls, città dell'Oregon. Stan ha trasformato la sua casa nel Mistery Shack, un fasullo museo dei misteri dove presenta le trovate più assurde per spillare soldi ai turisti ingenui. Nel museo lavorano anche il grasso tuttofare Soos e la commessa Wendy. Un giorno, nel fitto della foresta, Dipper trova un libro dove un uomo descrive dettagliamene le centinaia di misteriose creature presenti a Gravity Falls. Tra le tante pagine, trova la descrizione degli Zombi e pensa che il nuovo ragazzo di sua sorella Mabel sia uno di loro. La sorella non gli dà ascolto ma, durante un appuntamento, scopre che il suo ragazzo è formato da una piramide umana di gnomi che vogliono sposarla. Rifiutando la proposta, Mabel viene rapita e soltanto con l'aiuto di Dipper riesce a liberarsi. Gli gnomi li inseguono unendosi tra loro in una sorta di gnomo-gigante, ma Mabel inganna il capo degli gnomi rispedendolo nel bosco. Dipper pensa che la città nasconda più cose di quello che sembrano.\"},{\"absoluteNumber\":21,\"airedEpisodeNumber\":1,\"airedSeason\":2,\"airedSeasonID\":528922,\"dvdEpisodeNumber\":null,\"dvdSeason\":null,\"episodeName\":null,\"firstAired\":\"2014-08-01\",\"id\":4653528,\"language\":{\"episodeName\":\"\",\"overview\":\"\"},\"lastUpdated\":1457304478,\"overview\":null},{\"absoluteNumber\":null,\"airedEpisodeNumber\":1,\"airedSeason\":0,\"airedSeasonID\":554213,\"dvdEpisodeNumber\":null,\"dvdSeason\":null,\"episodeName\":\"La cosa (Guida di Dipper all'inspiegabile)\",\"firstAired\":\"2013-10-14\",\"id\":4682639,\"language\":{\"episodeName\":\"it\",\"overview\":\"\"},\"lastUpdated\":1484430380,\"overview\":null},{\"absoluteNumber\":2,\"airedEpisodeNumber\":2,\"airedSeason\":1,\"airedSeasonID\":494694,\"dvdEpisodeNumber\":2,\"dvdSeason\":1,\"episodeName\":\"La leggenda del drago trita-ossa\",\"firstAired\":\"2012-06-29\",\"id\":4344074,\"language\":{\"episodeName\":\"it\",\"overview\":\"it\"},\"lastUpdated\":1452621613,\"overview\":\"Il prozio Stan porta Dipper e Mabel al lago di Gravity Falls per pescare insieme ma, sentendo le dicerie di un vecchietto, i due partono con Soos alla ricerca del leggendario drago trita-ossa armati di un set di macchine fotografiche. Dopo aver raggiunto l'isola dall'altra parte del lago, i tre trovano il misterioso drago e cercano di scattargli una foto ma la furia del mostro non gli permette di farlo. Dopo un inseguimento, il mostro viene colpito in testa e si incastra all'entrata di una caverna, dove Dipper scopre che il drago è in realtà un robot controllato da quello stesso vecchietto comparso all'inizio (l'uomo pare solito costruire robot per attirare l'attenzione dei parenti sentendosi solo). A quel punto, Dipper e Mabel capiscono di aver abbandonato Stan e ritornano da lui, facendosi perdonare e passando la giornata insieme.\"},{\"absoluteNumber\":null,\"airedEpisodeNumber\":2,\"airedSeason\":0,\"airedSeasonID\":554213,\"dvdEpisodeNumber\":null,\"dvdSeason\":null,\"episodeName\":\"Il tatuaggio di Stan (Guida di Dipper all'inspiegabile)\",\"firstAired\":\"2013-10-14\",\"id\":4684168,\"language\":{\"episodeName\":\"it\",\"overview\":\"\"},\"lastUpdated\":1484430358,\"overview\":null},{\"absoluteNumber\":22,\"airedEpisodeNumber\":2,\"airedSeason\":2,\"airedSeasonID\":528922,\"dvdEpisodeNumber\":null,\"dvdSeason\":null,\"episodeName\":null,\"firstAired\":\"2014-08-04\",\"id\":4908804,\"language\":{\"episodeName\":\"\",\"overview\":\"\"},\"lastUpdated\":1457402393,\"overview\":null},{\"absoluteNumber\":3,\"airedEpisodeNumber\":3,\"airedSeason\":1,\"airedSeasonID\":494694,\"dvdEpisodeNumber\":3,\"dvdSeason\":1,\"episodeName\":\"Cacciatore di teste\",\"firstAired\":\"2012-06-30\",\"id\":4344075,\"language\":{\"episodeName\":\"it\",\"overview\":\"it\"},\"lastUpdated\":1452621624,\"overview\":\"Mentre sta pulendo la casa, Soos trova una porta camuffata ed, insieme a Dipper e Mabel, decide di entrarci. All'interno della stanza sono presenti numerose statue di cera appartenenti al prozio Stan. Esse facevano parte di una mostra, chiusa poi da Stan. Mabel decide di aprire al pubblico la collezione di statue e ve ne aggiunge una nuova creata da lei, che raffigura il prozio Stan. Una sera, però, la nuova statua viene trovata senza testa e Dipper, fingendosi investigatore, cerca il colpevole con i pochi indizi a sua disposizione. Dopo aver interrogato inutilmente diversi cittadini, Dipper e Mabel si arrendono ma, subito dopo, le statue di cera prendono vita e spiegano ai due che sono in realtà statue stregate. Le statue volevano uccidere Stan ma per sbaglio hanno colpito la statua al suo posto. I due fratelli riescono a distruggere col fuoco la maggior parte di esse tranne la statua di Sherlock Holmes con cui Dipper ingaggia un duello. Alla fine il ragazzino attira la statua sul tetto facendolo sciogliere appena arriva l'alba.\"},{\"absoluteNumber\":null,\"airedEpisodeNumber\":3,\"airedSeason\":0,\"airedSeasonID\":554213,\"dvdEpisodeNumber\":null,\"dvdSeason\":null,\"episodeName\":\"La cassetta delle lettere (Guida di Dipper all'inspiegabile)\",\"firstAired\":\"2013-10-15\",\"id\":4684169,\"language\":{\"episodeName\":\"it\",\"overview\":\"\"},\"lastUpdated\":1510418256,\"overview\":null},{\"absoluteNumber\":23,\"airedEpisodeNumber\":3,\"airedSeason\":2,\"airedSeasonID\":528922,\"dvdEpisodeNumber\":null,\"dvdSeason\":null,\"episodeName\":null,\"firstAired\":\"2014-08-11\",\"id\":4917013,\"language\":{\"episodeName\":\"\",\"overview\":\"\"},\"lastUpdated\":1453316112,\"overview\":null},{\"absoluteNumber\":4,\"airedEpisodeNumber\":4,\"airedSeason\":1,\"airedSeasonID\":494694,\"dvdEpisodeNumber\":4,\"dvdSeason\":1,\"episodeName\":\"Il magico Gideon\",\"firstAired\":\"2012-07-06\",\"id\":4344076,\"language\":{\"episodeName\":\"it\",\"overview\":\"it\"},\"lastUpdated\":1452621637,\"overview\":\"Al Mistery Shack gli affari vanno male perché tutti i turisti preferiscono andare al padiglione del magico Gideon, un bambino che dice di avere poteri paranormali. Soos, Dipper e Mabel vanno ad uno dei suoi spettacoli ed il giorno dopo Gideon inizia a frequentare in gran segreto la ragazza essendosene innamorato. Ella però non contraccambia i sentimenti di Gideon, ma non riesce a rifiutare i suoi inviti a cena. Dipper decide di aiutarla ed è molto diretto nel confessare al bambino che sua sorella non lo ama. Gideon, però, va su tutte le furie ed organizza una trappola per Dipper dove rivela di avere poteri telecinetici che gli consentono di muovere le cose. Mabel capisce cosa deve fare e rompe con Gideon di persona, nel fare ciò ruba anche l'amuleto magico che gli conferisce i poteri e lo distrugge salvando il fratello. A fine episodio, Dipper è preoccupato perché Gideon ha giurato vendetta su lui e Stan, colpevoli di aver rovinato la sua storia d'amore.\"},{\"absoluteNumber\":null,\"airedEpisodeNumber\":4,\"airedSeason\":0,\"airedSeasonID\":554213,\"dvdEpisodeNumber\":null,\"dvdSeason\":null,\"episodeName\":\"Lato sinistro (Guida di Dipper all'inspiegabile)\",\"firstAired\":\"2013-10-16\",\"id\":4684170,\"language\":{\"episodeName\":\"it\",\"overview\":\"\"},\"lastUpdated\":1484427339,\"overview\":null},{\"absoluteNumber\":24,\"airedEpisodeNumber\":4,\"airedSeason\":2,\"airedSeasonID\":528922,\"dvdEpisodeNumber\":null,\"dvdSeason\":null,\"episodeName\":null,\"firstAired\":\"2014-09-08\",\"id\":4955554,\"language\":{\"episodeName\":\"\",\"overview\":\"\"},\"lastUpdated\":1453316140,\"overview\":null},{\"absoluteNumber\":5,\"airedEpisodeNumber\":5,\"airedSeason\":1,\"airedSeasonID\":494694,\"dvdEpisodeNumber\":5,\"dvdSeason\":1,\"episodeName\":\"Il fantas-market\",\"firstAired\":\"2012-07-13\",\"id\":4344077,\"language\":{\"episodeName\":\"it\",\"overview\":\"it\"},\"lastUpdated\":1452621647,\"overview\":\"Dipper si innamora di Wendy, la commessa adolescente del Mistery Shack, e per fare colpo su di lei decide di frequentare anch'egli i suoi amici mentendo sulla sua età. Alla prima uscita, i ragazzi, Wendy, Dipper e Mabel entrano in un mini-market abbandonato dove tempo prima erano morte due persone. Tutto va inizialmente bene ed i ragazzi si divertono ma, quando uno di loro si stende sulla sagoma del cadavere, la situazione si complica. Tutto inizia a fluttuare ed alcuni ragazzi vengono trasformati in altro o rinchiusi in qualcosa mentre Mabel (andata fuori di testa a causa di un'indigestione causata da una polverina zuccherosa) viene posseduta. Grazie al libro, Dipper deduce che i fantasmi odiano gli adolescenti e rivela loro che egli non è ancora adolescente perché ha ancora dodici anni. I due fantasmi (i due anziani che gestivano il negozio quand'erano in vita) decidono allora di lasciarli andare se in cambio Dipper farà qualche numero simpatico per loro. Il ragazzino è costretto allora a esibirsi in un balletto che faceva da piccolo con un costume da agnellino. Una volta liberi tutti, Wendy racconta loro che Dipper li ha salvati evitando però il particolare del balletto.\"},{\"absoluteNumber\":25,\"airedEpisodeNumber\":5,\"airedSeason\":2,\"airedSeasonID\":528922,\"dvdEpisodeNumber\":null,\"dvdSeason\":null,\"episodeName\":null,\"firstAired\":\"2014-09-22\",\"id\":4964464,\"language\":{\"episodeName\":\"\",\"overview\":\"\"},\"lastUpdated\":1457304490,\"overview\":null},{\"absoluteNumber\":null,\"airedEpisodeNumber\":5,\"airedSeason\":0,\"airedSeasonID\":554213,\"dvdEpisodeNumber\":null,\"dvdSeason\":null,\"episodeName\":\"Il dente (Guida di Dipper all'inspiegabile)\",\"firstAired\":\"2013-10-17\",\"id\":4684171,\"language\":{\"episodeName\":\"it\",\"overview\":\"\"},\"lastUpdated\":1510418283,\"overview\":null},{\"absoluteNumber\":6,\"airedEpisodeNumber\":6,\"airedSeason\":1,\"airedSeasonID\":494694,\"dvdEpisodeNumber\":6,\"dvdSeason\":1,\"episodeName\":\"Dipper e la mascolinità\",\"firstAired\":\"2012-07-20\",\"id\":4344078,\"language\":{\"episodeName\":\"it\",\"overview\":\"it\"},\"lastUpdated\":1452621659,\"overview\":\"Dipper pensa di non essere un uomo vero e ne ha la conferma quando non riesce a superare una prova della macchina che misura la forza. Disperato, si reca nel bosco dove incontra una comunità di Minotauri convincendoli a insegnargli come diventare uomo. I Minotauri lo sottopongono a diverse prove che Dipper supera. Nel frattempo, Mabel capisce che al prozio Stan piace Susan la pigra, proprietaria della tavola calda di Gravity Falls. Decide allora di provare a cambiarlo per fargli conquistare Susan. Come ultima prova di mascolinità, il capo dei Minotauri ordina a Dipper di uccidere il Multi-Orso, una creatura formata dai corpi di più orsi fusi insieme. Trovata la creatura, il ragazzino scopre però che hanno gusti simili e non se la sente di ucciderlo. Dipper capisce che le opinioni degli altri non contano e si allontana dai Minotauri. Mabel capisce invece che non riuscirà mai a cambiare Stan ed intuisce anche che lui, in realtà, piace a Susan proprio perché è fatto così.\"},{\"absoluteNumber\":26,\"airedEpisodeNumber\":6,\"airedSeason\":2,\"airedSeasonID\":528922,\"dvdEpisodeNumber\":null,\"dvdSeason\":null,\"episodeName\":null,\"firstAired\":\"2014-10-04\",\"id\":4999789,\"language\":{\"episodeName\":\"\",\"overview\":\"\"},\"lastUpdated\":1453316271,\"overview\":null},{\"absoluteNumber\":null,\"airedEpisodeNumber\":6,\"airedSeason\":0,\"airedSeasonID\":554213,\"dvdEpisodeNumber\":null,\"dvdSeason\":null,\"episodeName\":null,\"firstAired\":\"2013-10-18\",\"id\":4684172,\"language\":{\"episodeName\":\"\",\"overview\":\"\"},\"lastUpdated\":1484430336,\"overview\":null},{\"absoluteNumber\":7,\"airedEpisodeNumber\":7,\"airedSeason\":1,\"airedSeasonID\":494694,\"dvdEpisodeNumber\":7,\"dvdSeason\":1,\"episodeName\":\"I sosia di Dipper\",\"firstAired\":\"2012-08-10\",\"id\":4359793,\"language\":{\"episodeName\":\"it\",\"overview\":\"it\"},\"lastUpdated\":1452621672,\"overview\":\"Il prozio Stan dà una festa e, per pubblicizzarla, ordina a Dipper di fotocopiare dei volantini con una vecchia fotocopiatrice. Usandola, Dipper si fotocopia per sbaglio il braccio e questo prende vita, staccandosi dal foglio. Con un po' d'acqua, però, la copia si dissolve. Tornato alla festa, Dipper vuole stare con Wendy e per conquistarla ha elaborato un piano di passaggi dettagliatamente ordinati. Il piano viene però ostacolato da molte cose, come l'arrivo di Robbie (suo rivale in amore) o altro. Dipper decide così di copiarsi dieci volte e di affidare ai sosia vari compiti. Nonostante tutto, Dipper infrange per un momento il piano e solo in quell'occasione si diverte davvero con Wendy. Le copie però si arrabbiano per questo e lo attaccano. Nel frattempo, Mabel si fa due nuove amiche molto strane ed impopolari, Candy e Grenda. Quando le due vengono prese di mira dalla popolarissima Pacifica, Mabel la sfida per il titolo di regina della festa. Pacifica però corrompe tutti vincendo la sfida, tuttavia Candy e Grenda ringraziano Mabel per averle difese ottenendo la loro ammirazione. Dipper riesce a dissolvere tutte le copie tranne la prima che aveva fatto (\"numero 2\", che poi si dissolverà a causa di una bevanda che ingurgiterà); parlando i due capiscono l'errore di Dipper di cercare sempre di pianificare tutto quando invece dovrebbe essere più spontaneo.\"},{\"absoluteNumber\":null,\"airedEpisodeNumber\":7,\"airedSeason\":0,\"airedSeasonID\":554213,\"dvdEpisodeNumber\":null,\"dvdSeason\":null,\"episodeName\":\"La guida di Mabel all'arte della conquista (Guida di Mabel alla vita)\",\"firstAired\":\"2014-02-03\",\"id\":4790287,\"language\":{\"episodeName\":\"it\",\"overview\":\"\"},\"lastUpdated\":1484430365,\"overview\":null},{\"absoluteNumber\":27,\"airedEpisodeNumber\":7,\"airedSeason\":2,\"airedSeasonID\":528922,\"dvdEpisodeNumber\":null,\"dvdSeason\":null,\"episodeName\":null,\"firstAired\":\"2014-10-27\",\"id\":5008306,\"language\":{\"episodeName\":\"\",\"overview\":\"\"},\"lastUpdated\":1453316305,\"overview\":null},{\"absoluteNumber\":8,\"airedEpisodeNumber\":8,\"airedSeason\":1,\"airedSeasonID\":494694,\"dvdEpisodeNumber\":8,\"dvdSeason\":1,\"episodeName\":\"L'irrazionalità è un tesoro\",\"firstAired\":\"2012-08-17\",\"id\":4359794,\"language\":{\"episodeName\":\"it\",\"overview\":\"it\"},\"lastUpdated\":1452621684,\"overview\":\"A Gravity Falls si festeggia la giornata in cui fu fondata la città. Il fondatore pare essere un avo di Pacifica e fu da allora che la famiglia di quest'ultima ebbe tanta popolarità e ricchezza; la ragazza non perde occasione per dileggiare Mabel per la sua eccentricità. Sul libro dei misteri di Dipper, però, si trova un codice indecifrabile che potrebbe rivelare il contrario. Grazie alle bizzarie di Mabel il codice viene decifrato ed i due arrivano ad uno stanzino sotterraneo dove un documento prova che in realtà Gravity Falls fu fondata da Sir Lord Quentin Trembley. Costui divenne anche presidente per un breve periodo, ma la sua irrazionalità portò il governo americano a cancellare tutti i ricordi della sua presidenza. I due fratelli trovano lo stesso Trembley ibernato all'interno di un ammasso di croccante, poiché l'uomo era convinto che l'avrebbe conservato in vita. Sfortunatamente i due poliziotti della città, che avevano il compito di tenere il segreto, li arrestano e li spediscono in una cassa a Washington. Ma durante il viaggio Mabel libera accidentalmente Trembley, ancora vivo, che ordina ai due poliziotti di andare in vacanza e fingere di non averli visti. Con il documento, Dipper fa capire a Pacifica che la sua famiglia non è così importante come crede; Mabel capisce invece che la sua originalità un po' stupida è un dono prezioso.\"},{\"absoluteNumber\":null,\"airedEpisodeNumber\":8,\"airedSeason\":0,\"airedSeasonID\":554213,\"dvdEpisodeNumber\":null,\"dvdSeason\":null,\"episodeName\":\"La guida di Mabel agli adesivi (Guida di Mabel alla vita)\",\"firstAired\":\"2014-02-04\",\"id\":4790288,\"language\":{\"episodeName\":\"it\",\"overview\":\"\"},\"lastUpdated\":1484430315,\"overview\":null},{\"absoluteNumber\":28,\"airedEpisodeNumber\":8,\"airedSeason\":2,\"airedSeasonID\":528922,\"dvdEpisodeNumber\":null,\"dvdSeason\":null,\"episodeName\":null,\"firstAired\":\"2014-11-10\",\"id\":5030466,\"language\":{\"episodeName\":\"\",\"overview\":\"\"},\"lastUpdated\":1453316336,\"overview\":null},{\"absoluteNumber\":9,\"airedEpisodeNumber\":9,\"airedSeason\":1,\"airedSeasonID\":494694,\"dvdEpisodeNumber\":9,\"dvdSeason\":1,\"episodeName\":\"Il maialino e l'uomo che viaggiava nel tempo\",\"firstAired\":\"2012-08-24\",\"id\":4359795,\"language\":{\"episodeName\":\"it\",\"overview\":\"it\"},\"lastUpdated\":1452621694,\"overview\":\"Il prozio Stan organizza nuovamente l'annuale fiera in cui guadagna molto grazie a dei giochi truccati. In uno di questi, Mabel riesce a vincere un maialino a cui si affeziona molto e che chiama Dondolo. Dipper trascorre invece una bella giornata con Wendy fino a quando, lanciando una pallina, ferisce accidentalmente la ragazza; a soccorrerla giunge Robbie (uno dei compagni di Wendy, nonché l'unico che odia Dipper) che le chiede anche di uscire insieme. Dipper rimane sconvolto nel vedere che Wendy accetta la proposta e cade in una forte disperazione. A tarda sera, trova insieme a Mabel un uomo venuto dal futuro, di nome Blendin, che viaggia nel tempo grazie ad uno strano macchinario portatile. I due lo rubano con l'inganno e cercano di rimediare all'errore di Dipper. Tutti i tentativi falliscono tranne uno; in quello, però, Mabel perde il suo maialino che finisce a Pacifica. Vedendo la sorella disperata, Dipper rinuncia allora all'unica linea temporale in cui stava con Wendy e prende nuovamente Dondolo per Mabel. Grazie al maialino però Robbie si rende ridicolo agli occhi di Wendy. Blendin viene arrestato dai suoi superiori ed obbligato a rimediare alle anomalie temporali causate dai due fratelli.\"},{\"absoluteNumber\":null,\"airedEpisodeNumber\":9,\"airedSeason\":0,\"airedSeasonID\":554213,\"dvdEpisodeNumber\":null,\"dvdSeason\":null,\"episodeName\":\"La guida di Mabel alla moda (Guida di Mabel alla vita)\",\"firstAired\":\"2014-02-05\",\"id\":4790289,\"language\":{\"episodeName\":\"it\",\"overview\":\"\"},\"lastUpdated\":1484430321,\"overview\":null},{\"absoluteNumber\":29,\"airedEpisodeNumber\":9,\"airedSeason\":2,\"airedSeasonID\":528922,\"dvdEpisodeNumber\":null,\"dvdSeason\":null,\"episodeName\":null,\"firstAired\":\"2014-11-24\",\"id\":5034993,\"language\":{\"episodeName\":\"\",\"overview\":\"\"},\"lastUpdated\":1453316364,\"overview\":null},{\"absoluteNumber\":10,\"airedEpisodeNumber\":10,\"airedSeason\":1,\"airedSeasonID\":494694,\"dvdEpisodeNumber\":10,\"dvdSeason\":1,\"episodeName\":\"Avversari irriducibili\",\"firstAired\":\"2012-09-14\",\"id\":4393844,\"language\":{\"episodeName\":\"it\",\"overview\":\"it\"},\"lastUpdated\":1452621703,\"overview\":\"Wendy è al campeggio con la sua famiglia e Dipper si trova a litigare con Robbie a causa dei rispettivi sentimenti che provano per lei finendo accidentalmente per rompergli il telefono. Robbie sfida allora Dipper a lottare corpo a corpo e fissa l'appuntamento alle due del pomeriggio. Dipper pensa di non presentarsi e si rintana nella sala giochi di Gravity Falls. Giocando ad un gioco di combattimento, il ragazzo trova una combinazione speciale di tasti con la quale, senza saperlo, anima un personaggio del videogioco. Precisamente sceglie Rumble McSkirmish, protagonista del gioco (le cui movenze ed esclamazioni sono un omaggio ai classici picchiaduro come Street Fighter e Final Fight). Dipper lo convince con una bugia a combattere Robbie per lui, ma Rumble si rivela troppo violento. Nel frattempo, Mabel aiuta il prozio Stan a superare la sua paura dell'altezza portandolo con l'inganno sulla cisterna della città. Sulla stessa cisterna si rifugia Robbie, in fuga da Rumble. Vedendo i tre in pericolo, Dipper confessa a Rumble la sua bugia ed è costretto ad accettare la sua sfida. Dipper capisce di non avere speranza e si lascia mettere KO per far comparire il game over che fa sparire Rumble. Scoperto che Wendy odia i ragazzi violenti, Dipper e Robbie decidono di limitarsi a odiarsi in segreto.\"},{\"absoluteNumber\":null,\"airedEpisodeNumber\":10,\"airedSeason\":0,\"airedSeasonID\":554213,\"dvdEpisodeNumber\":null,\"dvdSeason\":null,\"episodeName\":\"La guida di Mabel al colore (Guida di Mabel alla vita)\",\"firstAired\":\"2014-02-06\",\"id\":4790290,\"language\":{\"episodeName\":\"it\",\"overview\":\"\"},\"lastUpdated\":1510418406,\"overview\":null},{\"absoluteNumber\":30,\"airedEpisodeNumber\":10,\"airedSeason\":2,\"airedSeasonID\":528922,\"dvdEpisodeNumber\":null,\"dvdSeason\":null,\"episodeName\":null,\"firstAired\":\"2015-02-16\",\"id\":5111877,\"language\":{\"episodeName\":\"\",\"overview\":\"\"},\"lastUpdated\":1453316391,\"overview\":null},{\"absoluteNumber\":11,\"airedEpisodeNumber\":11,\"airedSeason\":1,\"airedSeasonID\":494694,\"dvdEpisodeNumber\":11,\"dvdSeason\":1,\"episodeName\":\"Il ''piccolo'' Dipper\",\"firstAired\":\"2012-09-28\",\"id\":4393845,\"language\":{\"episodeName\":\"it\",\"overview\":\"it\"},\"lastUpdated\":1461944143,\"overview\":\"Gideon ha deciso di appropriarsi del Mistery Shack, ma i suoi tentativi di imbrogliare Stan vanno a vuoto. Nel frattempo Mabel si sveglia un po' più alta di un millimetro e comincia a prendere in giro Dipper poiché lui è più basso di lei. Offeso ed arrabbiato, Dipper legge sul suo libro dei misteri che una pietra magica, nel bosco, ingrandisce e rimpicciolisce vari animali. Trovato il cristallo, Dipper ne prende un frammento e lo applica sulla sua torcia facendola diventare capace di ingrandire o rimpicciolire a seconda del lato del cristallo. Mabel lo scopre e durante il loro litigio la torcia finisce nelle mani di Gideon. Come prima azione decide di rimpicciolire i veri proprietari della torcia, li rinchiude a casa sua e si dirige da Stan con la stessa intenzione. Dipper e Mabel riescono a raggiungere la loro casa e, dopo aver appianato le loro divergenze (Mabel confessa che prendeva in giro il fratello perché lui era sempre più bravo di lei in tutto), riescono a fermare Gideon, che viene cacciato da Stan e perde la torcia. Tornati normali, i due decidono di rompere il cristallo. Una volta a casa, Gideon confessa a suo padre che il Mistery Shack nasconde un grande segreto.\"},{\"absoluteNumber\":null,\"airedEpisodeNumber\":11,\"airedSeason\":0,\"airedSeasonID\":554213,\"dvdEpisodeNumber\":null,\"dvdSeason\":null,\"episodeName\":\"La guida di Mabel all'arte (Guida di Mabel alla vita)\",\"firstAired\":\"2014-02-07\",\"id\":4790291,\"language\":{\"episodeName\":\"it\",\"overview\":\"\"},\"lastUpdated\":1484430310,\"overview\":null},{\"absoluteNumber\":31,\"airedEpisodeNumber\":11,\"airedSeason\":2,\"airedSeasonID\":528922,\"dvdEpisodeNumber\":null,\"dvdSeason\":null,\"episodeName\":null,\"firstAired\":\"2015-03-09\",\"id\":5130600,\"language\":{\"episodeName\":\"\",\"overview\":\"\"},\"lastUpdated\":1453316438,\"overview\":null},{\"absoluteNumber\":12,\"airedEpisodeNumber\":12,\"airedSeason\":1,\"airedSeasonID\":494694,\"dvdEpisodeNumber\":12,\"dvdSeason\":1,\"episodeName\":\"Summerween\",\"firstAired\":\"2012-10-05\",\"id\":4412099,\"language\":{\"episodeName\":\"it\",\"overview\":\"it\"},\"lastUpdated\":1452621726,\"overview\":\"A Gravity Falls si festeggia Halloween due volte all'anno e la seconda capita d'estate; quel giorno viene chiamato Summerween. Mabel è già pronta e non vede l'ora di bussare alle porte con Dipper come solito, ma il ragazzo preferirebbe andare ad una festa con Wendy. Alla porta del Mistery Shack bussa poi un mostro orribile; egli è il Giustiziere, un mostro (dalle sembianze di uno spaventapasseri) che punisce i ragazzi ostili al giorno di Summerween. Dipper si comporta in modo sgarbato attirando le sue ire e minaccia di divorarli tutti. Dipper, Mabel, Candy e Grenda dovranno riuscire a raccogliere cinquecento dolcetti per placarlo e, per farlo, Dipper indossa anche lui un costume. Raccolte tutte le caramelle, Dipper le getta accidentalmente in un laghetto per evitare di farsi vedere in costume da Wendy e Robbie ed il Giustiziere si infuria. Grazie a Soos, i ragazzi scampano al mostro che assume una seconda forma, simile ad un ragno e li insegue fin dentro un negozio di costumi. Nel frattempo, il prozio Stan cerca di spaventare due ragazzi, inutilmente. Purtroppo Soos viene mangiato e Dipper e Mabel scoprono che il Giustiziere è un ammasso di tutte le caramelle e i dolci scartati dai bambini e finiti nella discarica. Soos, però, trova quelle caramelle buone e mangia il mostro dall'interno; quest'ultimo, felice di piacere a qualcuno, si distrugge. I ragazzi si riuniscono da Stan dove Dipper trova anche Wendy, che aveva lasciato la festa, e fa pace con Mabel.\"},{\"absoluteNumber\":null,\"airedEpisodeNumber\":12,\"airedSeason\":0,\"airedSeasonID\":554213,\"dvdEpisodeNumber\":null,\"dvdSeason\":null,\"episodeName\":\"La golf car (Fai da te con Soos)\",\"firstAired\":\"2014-04-21\",\"id\":4861755,\"language\":{\"episodeName\":\"it\",\"overview\":\"\"},\"lastUpdated\":1484430374,\"overview\":null},{\"absoluteNumber\":32,\"airedEpisodeNumber\":12,\"airedSeason\":2,\"airedSeasonID\":528922,\"dvdEpisodeNumber\":null,\"dvdSeason\":null,\"episodeName\":null,\"firstAired\":\"2015-07-13\",\"id\":5250240,\"language\":{\"episodeName\":\"\",\"overview\":\"\"},\"lastUpdated\":1453316481,\"overview\":null},{\"absoluteNumber\":13,\"airedEpisodeNumber\":13,\"airedSeason\":1,\"airedSeasonID\":494694,\"dvdEpisodeNumber\":13,\"dvdSeason\":1,\"episodeName\":\"Mabel il capo\",\"firstAired\":\"2013-02-15\",\"id\":4420022,\"language\":{\"episodeName\":\"it\",\"overview\":\"it\"},\"lastUpdated\":1452621736,\"overview\":\"Mabel pensa che i modi del prozio Stan nei confronti di clienti e dipendenti siano sbagliati, nonostante guadagni molti soldi. I due decidono allora che Mabel sarà capo del Mistery Shack per tre giorni mentre lo zio parteciperà ad uno show in TV. Chi guadagnerà di più infliggerà una punizione all'altro. Mabel si dimostra sempre gentile ed evita i modi sgarbati; non conosce inoltre il valore effettivo dei soldi e per questo offre sempre rimborsi a tutti. Inoltre, dà una giornata libera a Wendy e chiede a Dipper di trovarle un vero mostro. Il ragazzo porta al Mistery Shack un Gremloblin, incrocio tra Gremlin e Goblin; la creatura è capace di mostrare i peggiori incubi di chiunque lo guardi negli occhi e diventa più forte con l'acqua. Prima di scappare nel bosco dopo essere rimasta vittima del suo stesso potere grazie ad uno specchio, la creatura causa molti danni. Mabel, infuriata, perde la ragione e convoca nuovamente Wendy e Soos; dopo averli sgridati comincia ad usare i metodi di Stan. Con essi, nonostante sia un po' sgarbata, guadagna molti soldi e ripara il Mistery Shack. Pur guadagnando alla fine solo un dollaro, Mabel vince la scommessa dato che Stan perde tutti i soldi che stava vincendo nel quiz nell'ultima prova. Mabel restituisce il ruolo di capo al prozio, ma lo costringe lo stesso ad una punizione umiliante.\"},{\"absoluteNumber\":null,\"airedEpisodeNumber\":13,\"airedSeason\":0,\"airedSeasonID\":554213,\"dvdEpisodeNumber\":null,\"dvdSeason\":null,\"episodeName\":\"L'orologio a cucù (Fai da te con Soos)\",\"firstAired\":\"2014-04-22\",\"id\":4861847,\"language\":{\"episodeName\":\"it\",\"overview\":\"\"},\"lastUpdated\":1484430330,\"overview\":null},{\"absoluteNumber\":33,\"airedEpisodeNumber\":13,\"airedSeason\":2,\"airedSeasonID\":528922,\"dvdEpisodeNumber\":null,\"dvdSeason\":null,\"episodeName\":null,\"firstAired\":\"2015-08-03\",\"id\":5276237,\"language\":{\"episodeName\":\"\",\"overview\":\"\"},\"lastUpdated\":1453316531,\"overview\":null},{\"absoluteNumber\":14,\"airedEpisodeNumber\":14,\"airedSeason\":1,\"airedSeasonID\":494694,\"dvdEpisodeNumber\":14,\"dvdSeason\":1,\"episodeName\":\"Pozzo senza fondo!\",\"firstAired\":\"2013-03-01\",\"id\":4507024,\"language\":{\"episodeName\":\"it\",\"overview\":\"it\"},\"lastUpdated\":1452621745,\"overview\":\"A pochi passi dal Mistery Shack c'è una voragine di medie dimensioni, chiamata \"Pozzo senza Fondo\" che il prozio Stan usa come discarica per gli oggetti inutili. Un giorno, una vento forte spinge Stan, Soos, Dipper e Mabel all'interno del pozzo ed i quattro si ritrovano nel vuoto assoluto. Mentre cade, ognuno di loro comincia a raccontare una storia per perdere tempo:\r\n- Nella sua storia Dipper scopre di avere una voce stridula per cui viene preso in giro dagli amici. Per cui prende una pozione datogli dal vecchio pazzo della città che gli dà una voce più profonda e da adulto. Ma la nuova voce gli procura solo problemi e alla fine capisce che per tutti i suoi amici la sua voce e perfetta così com'è, un po' stridula ma d'effetto.\r\n- Nella sua storia Soos cerca invece di battere il record ad un vecchio flipper di Stan, ma, sotto consiglio di Dipper e Mabel, bara scuotendolo. Il flipper si rivela stregato e per punizione i tre vengono risucchiati all'interno. Mentre i due fratelli distraggono il flipper, Soos trova un bottone che lo spegne liberandoli e scarificando il suo record che viene cancellato.\r\n- Nella storia di Mabel, la ragazzina, stanca delle bugie di Stan, si procura dei denti magici (trovati tramite il libro di Dipper) che costringono il prozio a dire sempre la verità. Ma la cosa le si ritorce contro ed alla fine lei stessa è costretta a mentire per evitare a Stan di finire in prigione. Mabel getta i denti in una cassa che poi butterà nel pozzo.\r\nAll'improvviso, i quattro escono dalla buca ritrovandosi esattamente da dove erano caduti; inoltre Dipper nota che è passato poco tempo e capisce che il pozzo è tunnel spazio-temporale.\"},{\"absoluteNumber\":null,\"airedEpisodeNumber\":14,\"airedSeason\":0,\"airedSeasonID\":554213,\"dvdEpisodeNumber\":null,\"dvdSeason\":null,\"episodeName\":\"Episodio 1 (La TV di Gravity Falls)\",\"firstAired\":\"2014-04-23\",\"id\":4861857,\"language\":{\"episodeName\":\"it\",\"overview\":\"\"},\"lastUpdated\":1484428060,\"overview\":null},{\"absoluteNumber\":34,\"airedEpisodeNumber\":14,\"airedSeason\":2,\"airedSeasonID\":528922,\"dvdEpisodeNumber\":null,\"dvdSeason\":null,\"episodeName\":null,\"firstAired\":\"2015-08-24\",\"id\":5276238,\"language\":{\"episodeName\":\"\",\"overview\":\"\"},\"lastUpdated\":1453316577,\"overview\":null},{\"absoluteNumber\":15,\"airedEpisodeNumber\":15,\"airedSeason\":1,\"airedSeasonID\":494694,\"dvdEpisodeNumber\":15,\"dvdSeason\":1,\"episodeName\":\"Mermando il tritone\",\"firstAired\":\"2013-03-15\",\"id\":4519484,\"language\":{\"episodeName\":\"it\",\"overview\":\"it\"},\"lastUpdated\":1452621759,\"overview\":\"A Gravity Falls è arrivata un'improvvisa ondata di calore e tutti gli abitanti della città si rinfrescano in piscina. Qui, Dipper ottiene un lavoro come aiutante bagnino, dopo aver visto che il bagnino è Wendy. Mabel si innamora invece di un ragazzo che non esce mai dall'acqua; dopo essersi conosciuti a fondo la ragazza scopre che il misterioso individuo si chiama Mermando e che egli è un tritone, essere metà uomo e metà pesce. Egli viveva nel mare ma, un brutto giorno, fu catturato e per sbaglio finì accidentalmente nella piscina mentre cercava di scappare. Nel frattempo, Dipper delude una specie di sovrintendente (dato che lui e Wendy pensano più a divertirsi che lavorare) e per mantenere il lavoro deve fare la guardia di notte. Mabel decide di liberare Mermando usando l'attrezzatura della piscina. Inconsapevole della missione, Dipper cerca di fermare la sorella dopo averla beccata rubare un frigo (usato in realtà per trasportare il tritone). I due arrivano al lago dove svela la verità a Dipper che è costretto a salvare la vita di Mermando con la respirazione artificiale. Mermando ringrazia Mabel dandole il suo primo bacio e dal lago risale la cascata in modo da raggiungere il mare. Dipper viene però licenziato per le attrezzature perse ma, con grande felicità, sa che anche Wendy è stata licenziata.\"},{\"absoluteNumber\":null,\"airedEpisodeNumber\":15,\"airedSeason\":0,\"airedSeasonID\":554213,\"dvdEpisodeNumber\":null,\"dvdSeason\":null,\"episodeName\":\"Episodio 2 (La TV di Gravity Falls)\",\"firstAired\":\"2014-04-24\",\"id\":4861858,\"language\":{\"episodeName\":\"it\",\"overview\":\"\"},\"lastUpdated\":1484459639,\"overview\":null},{\"absoluteNumber\":35,\"airedEpisodeNumber\":15,\"airedSeason\":2,\"airedSeasonID\":528922,\"dvdEpisodeNumber\":null,\"dvdSeason\":null,\"episodeName\":null,\"firstAired\":\"2015-09-07\",\"id\":5305160,\"language\":{\"episodeName\":\"\",\"overview\":\"\"},\"lastUpdated\":1463078077,\"overview\":null},{\"absoluteNumber\":16,\"airedEpisodeNumber\":16,\"airedSeason\":1,\"airedSeasonID\":494694,\"dvdEpisodeNumber\":16,\"dvdSeason\":1,\"episodeName\":\"Il segreto del tappeto\",\"firstAired\":\"2013-04-05\",\"id\":4524045,\"language\":{\"episodeName\":\"it\",\"overview\":\"it\"},\"lastUpdated\":1452621766,\"overview\":\"Mabel passa continuamente del tempo con Grenda e Candy; dopo l'ultimo pigiama-party che lo costringe a dormire fuori, Dipper litiga con la sorella. I due decidono perciò di non condividere più la stessa stanza e, quando una nuova stanza viene trovata da Soos dietro una libreria, entrano in competizione per ottenere il possesso della nuova camera cercando di avere i favori del prozio Stan. In essa è presente l'esperimento 78, un tappeto capace di manipolare gli atomi e, di conseguenza, di far scambiare di corpo due o più elementi tramite l'energia statica. Dipper e Mabel finiscono per scambiarsi di corpo ed ognuno cerca di far perdere la competizione all'altro finendo però in situazione imbarazzanti con le amiche di Mabel e con Stan. Nel frattempo anche Soos e Dondolo finiscono per scambiarsi di corpo. Stan decide di dare la stanza a Dipper così Mabel (nel corpo del fratello) cerca di chiudersi nella stanza che però viene presa d'assalto da diversi personaggi. Nel giro dello scambio di corpi vengono coinvolti anche Soos, Dondolo, Candy, Grenda, il vecchio pazzo della città, lo sceriffo Blubs ed il sovrintendente Durlan. Quando tutto torna normale, Dipper spiega Mabel che si sentiva escluso e la sorella rinuncia alla stanza. Alla fine Dipper però ci ripensa e, di conseguenza, i due tornano nella stessa stanza. La nuova camera diventa la stanza personale di Soos ed il tappeto viene messo via.\"},{\"absoluteNumber\":null,\"airedEpisodeNumber\":16,\"airedSeason\":0,\"airedSeasonID\":554213,\"dvdEpisodeNumber\":null,\"dvdSeason\":null,\"episodeName\":\"Un film d'azione (L'album dei ricordi di Mabel)\",\"firstAired\":\"2014-06-02\",\"id\":4894567,\"language\":{\"episodeName\":\"it\",\"overview\":\"\"},\"lastUpdated\":1484428175,\"overview\":null},{\"absoluteNumber\":36,\"airedEpisodeNumber\":16,\"airedSeason\":2,\"airedSeasonID\":528922,\"dvdEpisodeNumber\":null,\"dvdSeason\":null,\"episodeName\":null,\"firstAired\":\"2015-09-21\",\"id\":5305161,\"language\":{\"episodeName\":\"\",\"overview\":\"\"},\"lastUpdated\":1453316657,\"overview\":null},{\"absoluteNumber\":17,\"airedEpisodeNumber\":17,\"airedSeason\":1,\"airedSeasonID\":494694,\"dvdEpisodeNumber\":17,\"dvdSeason\":1,\"episodeName\":\"Il controllo della mente\",\"firstAired\":\"2013-04-19\",\"id\":4524046,\"language\":{\"episodeName\":\"it\",\"overview\":\"it\"},\"lastUpdated\":1452621776,\"overview\":\"A Gravity Falls è arrivata una famosissima band, i Sev'ral Timez, e Mabel desidera andare al loro concerto. Insieme a Candy e Grenda, la ragazza entra nel camerino della band e scopre che i componenti sono dei cloni prodotti e allevati come animali da laboratorio, sotto minaccia di essere sostituiti dal loro perfido produttore se non seguiranno i suoi ordini. Mabel il libera e li porta al Mistery Shack, promette di liberarli solo quando il produttore malvagio smetterà di cercarli. Il produttore dei Sev'ral Timez viene arrestato, ma Mabel rifiuta di lasciarli andare rivoltandosi contro le sue stesse amiche. Nel frattempo Wendy rompe il fidanzamento con Robbie ma, facendole ascoltare una strana canzone, il ragazzo le fa cambiare idea. Sbalordito e preoccupato, Dipper chiede aiuto al prozio Stan ipotizzando che nella canzone ci sia un messaggio per il controllo della mente ed i due esaminano a fondo la canzone. Ascoltando la canzona al rallentatore non sentono nulla ma, invertendo le parole, scovano una frase che causa una specie di ipnosi. Grazie a Stan, che si mostra particolarmente coinvolto, Dipper raggiunge Wendy e Robbie confessa di aver copiato quella canzone da un altro gruppo. Wendy si dispera e, quando Dipper le chiede poco opportunamente di passare del tempo insieme, gli dà dell'egoista e se ne va via piangendo. Stan cerca di consolare Dipper. Alla fine, dopo aver ascoltato una loro canzone, Mabel cambia idea e lascia libera la band nella natura facendo pace con le sue amiche.\"},{\"absoluteNumber\":null,\"airedEpisodeNumber\":17,\"airedSeason\":0,\"airedSeasonID\":554213,\"dvdEpisodeNumber\":null,\"dvdSeason\":null,\"episodeName\":\"Lo zoo delle carezze (L'album dei ricordi di Mabel)\",\"firstAired\":\"2014-06-02\",\"id\":4894568,\"language\":{\"episodeName\":\"it\",\"overview\":\"\"},\"lastUpdated\":1484428197,\"overview\":null},{\"absoluteNumber\":37,\"airedEpisodeNumber\":17,\"airedSeason\":2,\"airedSeasonID\":528922,\"dvdEpisodeNumber\":null,\"dvdSeason\":null,\"episodeName\":null,\"firstAired\":\"2015-10-12\",\"id\":5339875,\"language\":{\"episodeName\":\"\",\"overview\":\"\"},\"lastUpdated\":1508982370,\"overview\":null},{\"absoluteNumber\":18,\"airedEpisodeNumber\":18,\"airedSeason\":1,\"airedSeasonID\":494694,\"dvdEpisodeNumber\":18,\"dvdSeason\":1,\"episodeName\":\"Alla ricerca del maialino perduto\",\"firstAired\":\"2013-06-28\",\"id\":4596512,\"language\":{\"episodeName\":\"it\",\"overview\":\"it\"},\"lastUpdated\":1452621786,\"overview\":\"In città si stanno verificando degli strani eventi, le cui cause sembrano attribuibili ad una bestia volante primitiva e feroce. Dipper e Soos non vedono l'ora di fotografarla, ma Soos rovina tutto. Mabel cerca invece di stabilire un legame tra Dondolo e Stan ma, quando il prozio lascia il maialino solo nel giardino, la bestia feroce che si rivela uno pterodattilo lo cattura e lo porta via. Stan, mentendo, dice a Mabel di aver lottato per salvare Dondolo. Seguendo le tracce della strana creatura, Dipper, Soos, Mabel e Stan giungono ad una casa abbandonata dove trovano anche McGucket; il vecchietto si unisce a loro. Calandosi in un buco nel pavimento, i cinque trovano una miniera abbandonata in cui le piante sembrano preistoriche. Esplorando meglio la caverna, Dipper e Soos scovano anche dei dinosauri vivi, rimasti ibernati nella resina. Il caldo, però, sta facendo sciogliere la resina ed qualche dinosauro potrebbe essere già libero. Lo pterodattilo appare e li attacca; Mabel, Dipper, Soos e McGucket rimangono bloccati nel suo nido con un cucciolo appena nato. Per salvarsi, Stan deve obbligatoriamente collaborare con Dondolo finendo sul serio per lottare con lo pterodattilo e ciò permette a Mabel di perdonare il prozio mentre Soos grazie ad una sua idea riesce a salvarli dal cucciolo. Usciti dalla miniera, la casa crolla e chiude il passaggio per il mondo esterno.\"},{\"absoluteNumber\":38,\"airedEpisodeNumber\":18,\"airedSeason\":2,\"airedSeasonID\":528922,\"dvdEpisodeNumber\":null,\"dvdSeason\":null,\"episodeName\":null,\"firstAired\":\"2015-10-26\",\"id\":5370543,\"language\":{\"episodeName\":\"\",\"overview\":\"\"},\"lastUpdated\":1508982520,\"overview\":null},{\"absoluteNumber\":null,\"airedEpisodeNumber\":18,\"airedSeason\":0,\"airedSeasonID\":554213,\"dvdEpisodeNumber\":null,\"dvdSeason\":null,\"episodeName\":null,\"firstAired\":\"2014-11-03\",\"id\":5517698,\"language\":{\"episodeName\":\"\",\"overview\":\"\"},\"lastUpdated\":1505854845,\"overview\":null},{\"absoluteNumber\":null,\"airedEpisodeNumber\":19,\"airedSeason\":0,\"airedSeasonID\":554213,\"dvdEpisodeNumber\":null,\"dvdSeason\":null,\"episodeName\":null,\"firstAired\":\"2015-04-19\",\"id\":5229413,\"language\":{\"episodeName\":\"\",\"overview\":\"\"},\"lastUpdated\":1484521928,\"overview\":null},{\"absoluteNumber\":19,\"airedEpisodeNumber\":19,\"airedSeason\":1,\"airedSeasonID\":494694,\"dvdEpisodeNumber\":19,\"dvdSeason\":1,\"episodeName\":\"Il catturasogni\",\"firstAired\":\"2013-07-12\",\"id\":4602885,\"language\":{\"episodeName\":\"it\",\"overview\":\"it\"},\"lastUpdated\":1452621797,\"overview\":\"Dipper è offeso perché tutti i compiti difficili vengono affidati a lui e pensa addirittura che il prozio Stan lo odi. Gideon cerca invece di ottenere la combinazione della cassaforte in cui c'è l'atto di proprietà del Mistery Shack. Dopo un ennesimo fallimento, Gideon decide di risvegliare una creatura magica, la più potente secondo il libro segreto che ha lui. Esso è Bill Cipher, un essere triangolare capace di entrare nella mente altrui e di governarla. Gideon ordina a Bill di entrare nella mente di Stan e trovare la combinazione della cassaforte. Seguendo le indicazioni sul suo libro, Dipper riesce a portare anche se stesso, Mabel e Soos all'interno della mente di Stan. Entrati, si trovano in una versione deforme del Mistery Shack ed, all'interno di esso, ci sono delle porte che mostrano i ricordi. Dipper si distacca un attimo dal gruppo e trova un ricordo dove il prozio spiega a Soos che tratta Dipper molto male per insegnargli a sopravvivere al mondo esterno; in realtà è molto fiero di lui. Nel frattempo, Bill Cipher riesce con l'inganno a farsi aiutare da Mabel ma, scoperto il piano della creatura triangolare, la ragazza lo ferma prima che possa comunicare la combinazione. Dipper scopre che dentro la mente può immaginare ciò che deciderà; così interviene per salvare Mabel e Soos dall'ira di Bill. I tre insieme lo sconfiggono e Bill, impressionato, se ne va avvertendoli di un male futuro. Risvegliatisi tutti, scoprono che Gideon ha distrutto la cassaforte con la dinamite ed, ottenuto l'atto di proprietà, comincia a far distruggere il Mistery Shack da suo padre.\"},{\"absoluteNumber\":39,\"airedEpisodeNumber\":19,\"airedSeason\":2,\"airedSeasonID\":528922,\"dvdEpisodeNumber\":null,\"dvdSeason\":null,\"episodeName\":null,\"firstAired\":\"2015-11-23\",\"id\":5389618,\"language\":{\"episodeName\":\"\",\"overview\":\"\"},\"lastUpdated\":1508982640,\"overview\":null},{\"absoluteNumber\":null,\"airedEpisodeNumber\":20,\"airedSeason\":0,\"airedSeasonID\":554213,\"dvdEpisodeNumber\":null,\"dvdSeason\":null,\"episodeName\":null,\"firstAired\":\"2015-04-19\",\"id\":5229414,\"language\":{\"episodeName\":\"\",\"overview\":\"\"},\"lastUpdated\":1484521954,\"overview\":null},{\"absoluteNumber\":20,\"airedEpisodeNumber\":20,\"airedSeason\":1,\"airedSeasonID\":494694,\"dvdEpisodeNumber\":20,\"dvdSeason\":1,\"episodeName\":\"Gideonland\",\"firstAired\":\"2013-08-02\",\"id\":4615292,\"language\":{\"episodeName\":\"it\",\"overview\":\"it\"},\"lastUpdated\":1455623713,\"overview\":\"Gideon ora possiede il Mistery Shack e annuncia ai cittadini che costruirà al suo posto Gideonland, un parco divertimenti. In realtà, Gideon confessa al padre che il vero motivo per cui voleva quella casa era per trovare il libro segreto nº 1 perché, avendo anche il nº 2, potrà avere poteri illimitati. Stan e agli altri nel frattempo si sono trasferiti dalla nonna di Soos. Stan non può più permettersi di tenere i pronipoti, quindi decide di rispedirli a casa loro. Dipper e Mabel però non vogliono arrendersi e progettano di riprendersi la casa. I due chiedono aiuto agli gnomi ma Gideon, con uno strano fischietto, riesce a neutralizzarli. Dallo scontro, Gideon ottiene anche il libro di Dipper non sapendo che è il numero 3 e non il primo. Dipper e Mabel sono sull'autobus che li porterà dai loro genitori ma Gideon, convinto che Dipper abbia anche il primo libro, cerca di fermarli con un robot gigante a sua immagine (costruito dal vecchio McGucket). Gideon rapisce Mabel e poi cerca di scappare dopo aver rinfacciato a Dipper di essere inutile senza il libro; Dipper riesce però a farlo cadere da un dirupo e distrugge così il suo robot. I due gemelli si salvano grazie all'arpione che Mabel aveva fin dal primo episodio. Stan dimostra inoltre che il piccoletto è un imbroglione: non aveva poteri paranormali ma spiava gli abitanti di Gravity Falls con delle microspie piazzate nelle spille. Gideon viene messo in prigione mentre il Mistery Shack viene ricostruito; Dipper parla a Stan del suo libro, ma il prozio finge che siano tutte sciocchezze. In realtà, poco dopo, vediamo Stan andare in un sotterraneo segreto dove, con tutti e tre i volumi del libro (i quali vanno a formare un codice), attiva un'enorme macchina.\"},{\"absoluteNumber\":40,\"airedEpisodeNumber\":20,\"airedSeason\":2,\"airedSeasonID\":528922,\"dvdEpisodeNumber\":null,\"dvdSeason\":null,\"episodeName\":null,\"firstAired\":\"2016-02-15\",\"id\":5432171,\"language\":{\"episodeName\":\"\",\"overview\":\"\"},\"lastUpdated\":1505773232,\"overview\":null},{\"absoluteNumber\":null,\"airedEpisodeNumber\":21,\"airedSeason\":0,\"airedSeasonID\":554213,\"dvdEpisodeNumber\":null,\"dvdSeason\":null,\"episodeName\":null,\"firstAired\":\"2015-04-19\",\"id\":5229415,\"language\":{\"episodeName\":\"\",\"overview\":\"\"},\"lastUpdated\":1484521965,\"overview\":null},{\"absoluteNumber\":null,\"airedEpisodeNumber\":22,\"airedSeason\":0,\"airedSeasonID\":554213,\"dvdEpisodeNumber\":null,\"dvdSeason\":null,\"episodeName\":null,\"firstAired\":\"2015-04-19\",\"id\":5229416,\"language\":{\"episodeName\":\"\",\"overview\":\"\"},\"lastUpdated\":1510419648,\"overview\":null},{\"absoluteNumber\":null,\"airedEpisodeNumber\":23,\"airedSeason\":0,\"airedSeasonID\":554213,\"dvdEpisodeNumber\":null,\"dvdSeason\":null,\"episodeName\":null,\"firstAired\":\"2015-04-19\",\"id\":5229417,\"language\":{\"episodeName\":\"\",\"overview\":\"\"},\"lastUpdated\":1484522023,\"overview\":null},{\"absoluteNumber\":null,\"airedEpisodeNumber\":24,\"airedSeason\":0,\"airedSeasonID\":554213,\"dvdEpisodeNumber\":null,\"dvdSeason\":null,\"episodeName\":null,\"firstAired\":\"2015-04-19\",\"id\":5229418,\"language\":{\"episodeName\":\"\",\"overview\":\"\"},\"lastUpdated\":1484522031,\"overview\":null},{\"absoluteNumber\":null,\"airedEpisodeNumber\":25,\"airedSeason\":0,\"airedSeasonID\":554213,\"dvdEpisodeNumber\":null,\"dvdSeason\":null,\"episodeName\":null,\"firstAired\":\"2015-04-19\",\"id\":5229419,\"language\":{\"episodeName\":\"\",\"overview\":\"\"},\"lastUpdated\":1484522045,\"overview\":null},{\"absoluteNumber\":null,\"airedEpisodeNumber\":26,\"airedSeason\":0,\"airedSeasonID\":554213,\"dvdEpisodeNumber\":null,\"dvdSeason\":null,\"episodeName\":null,\"firstAired\":\"2015-04-19\",\"id\":5229420,\"language\":{\"episodeName\":\"\",\"overview\":\"\"},\"lastUpdated\":1510419698,\"overview\":null},{\"absoluteNumber\":null,\"airedEpisodeNumber\":27,\"airedSeason\":0,\"airedSeasonID\":554213,\"dvdEpisodeNumber\":null,\"dvdSeason\":null,\"episodeName\":null,\"firstAired\":\"2015-04-19\",\"id\":5229421,\"language\":{\"episodeName\":\"\",\"overview\":\"\"},\"lastUpdated\":1484522060,\"overview\":null},{\"absoluteNumber\":null,\"airedEpisodeNumber\":28,\"airedSeason\":0,\"airedSeasonID\":554213,\"dvdEpisodeNumber\":null,\"dvdSeason\":null,\"episodeName\":null,\"firstAired\":\"2015-04-19\",\"id\":5229422,\"language\":{\"episodeName\":\"\",\"overview\":\"\"},\"lastUpdated\":1484522067,\"overview\":null},{\"absoluteNumber\":null,\"airedEpisodeNumber\":29,\"airedSeason\":0,\"airedSeasonID\":554213,\"dvdEpisodeNumber\":null,\"dvdSeason\":null,\"episodeName\":null,\"firstAired\":\"2015-04-20\",\"id\":5517692,\"language\":{\"episodeName\":\"\",\"overview\":\"\"},\"lastUpdated\":1505854840,\"overview\":null},{\"absoluteNumber\":null,\"airedEpisodeNumber\":30,\"airedSeason\":0,\"airedSeasonID\":554213,\"dvdEpisodeNumber\":null,\"dvdSeason\":null,\"episodeName\":null,\"firstAired\":\"2015-04-20\",\"id\":5517694,\"language\":{\"episodeName\":\"\",\"overview\":\"\"},\"lastUpdated\":1505854842,\"overview\":null},{\"absoluteNumber\":null,\"airedEpisodeNumber\":31,\"airedSeason\":0,\"airedSeasonID\":554213,\"dvdEpisodeNumber\":null,\"dvdSeason\":null,\"episodeName\":null,\"firstAired\":\"2015-04-20\",\"id\":5517696,\"language\":{\"episodeName\":\"\",\"overview\":\"\"},\"lastUpdated\":1505854843,\"overview\":null},{\"absoluteNumber\":null,\"airedEpisodeNumber\":32,\"airedSeason\":0,\"airedSeasonID\":554213,\"dvdEpisodeNumber\":null,\"dvdSeason\":null,\"episodeName\":null,\"firstAired\":\"2015-04-20\",\"id\":5517697,\"language\":{\"episodeName\":\"\",\"overview\":\"\"},\"lastUpdated\":1505854844,\"overview\":null},{\"absoluteNumber\":null,\"airedEpisodeNumber\":33,\"airedSeason\":0,\"airedSeasonID\":554213,\"dvdEpisodeNumber\":null,\"dvdSeason\":null,\"episodeName\":null,\"firstAired\":\"2015-10-23\",\"id\":5388060,\"language\":{\"episodeName\":\"\",\"overview\":\"\"},\"lastUpdated\":1505731572,\"overview\":null},{\"absoluteNumber\":null,\"airedEpisodeNumber\":34,\"airedSeason\":0,\"airedSeasonID\":554213,\"dvdEpisodeNumber\":null,\"dvdSeason\":null,\"episodeName\":null,\"firstAired\":\"2015-10-24\",\"id\":5388061,\"language\":{\"episodeName\":\"\",\"overview\":\"\"},\"lastUpdated\":1505731573,\"overview\":null},{\"absoluteNumber\":null,\"airedEpisodeNumber\":35,\"airedSeason\":0,\"airedSeasonID\":554213,\"dvdEpisodeNumber\":null,\"dvdSeason\":null,\"episodeName\":null,\"firstAired\":\"2015-10-24\",\"id\":5388062,\"language\":{\"episodeName\":\"\",\"overview\":\"\"},\"lastUpdated\":1505731574,\"overview\":null},{\"absoluteNumber\":null,\"airedEpisodeNumber\":36,\"airedSeason\":0,\"airedSeasonID\":554213,\"dvdEpisodeNumber\":null,\"dvdSeason\":null,\"episodeName\":null,\"firstAired\":\"2015-10-25\",\"id\":5388063,\"language\":{\"episodeName\":\"\",\"overview\":\"\"},\"lastUpdated\":1505731575,\"overview\":null},{\"absoluteNumber\":null,\"airedEpisodeNumber\":37,\"airedSeason\":0,\"airedSeasonID\":554213,\"dvdEpisodeNumber\":null,\"dvdSeason\":null,\"episodeName\":null,\"firstAired\":\"2015-11-08\",\"id\":5401036,\"language\":{\"episodeName\":\"\",\"overview\":\"\"},\"lastUpdated\":1505743141,\"overview\":null},{\"absoluteNumber\":null,\"airedEpisodeNumber\":38,\"airedSeason\":0,\"airedSeasonID\":554213,\"dvdEpisodeNumber\":null,\"dvdSeason\":null,\"episodeName\":null,\"firstAired\":\"2015-11-11\",\"id\":5401037,\"language\":{\"episodeName\":\"\",\"overview\":\"\"},\"lastUpdated\":1505743142,\"overview\":null},{\"absoluteNumber\":null,\"airedEpisodeNumber\":39,\"airedSeason\":0,\"airedSeasonID\":554213,\"dvdEpisodeNumber\":null,\"dvdSeason\":null,\"episodeName\":null,\"firstAired\":\"2015-11-15\",\"id\":5405299,\"language\":{\"episodeName\":\"\",\"overview\":\"\"},\"lastUpdated\":1505747060,\"overview\":null},{\"absoluteNumber\":null,\"airedEpisodeNumber\":40,\"airedSeason\":0,\"airedSeasonID\":554213,\"dvdEpisodeNumber\":null,\"dvdSeason\":null,\"episodeName\":null,\"firstAired\":\"2015-11-15\",\"id\":5405302,\"language\":{\"episodeName\":\"\",\"overview\":\"\"},\"lastUpdated\":1505747063,\"overview\":null},{\"absoluteNumber\":null,\"airedEpisodeNumber\":41,\"airedSeason\":0,\"airedSeasonID\":554213,\"dvdEpisodeNumber\":null,\"dvdSeason\":null,\"episodeName\":null,\"firstAired\":\"2015-11-15\",\"id\":5405304,\"language\":{\"episodeName\":\"\",\"overview\":\"\"},\"lastUpdated\":1505747065,\"overview\":null},{\"absoluteNumber\":null,\"airedEpisodeNumber\":42,\"airedSeason\":0,\"airedSeasonID\":554213,\"dvdEpisodeNumber\":null,\"dvdSeason\":null,\"episodeName\":null,\"firstAired\":\"2015-11-16\",\"id\":5406268,\"language\":{\"episodeName\":\"\",\"overview\":\"\"},\"lastUpdated\":1505748007,\"overview\":null},{\"absoluteNumber\":null,\"airedEpisodeNumber\":43,\"airedSeason\":0,\"airedSeasonID\":554213,\"dvdEpisodeNumber\":null,\"dvdSeason\":null,\"episodeName\":null,\"firstAired\":\"2015-11-18\",\"id\":5408655,\"language\":{\"episodeName\":\"\",\"overview\":\"\"},\"lastUpdated\":1505750232,\"overview\":null},{\"absoluteNumber\":null,\"airedEpisodeNumber\":44,\"airedSeason\":0,\"airedSeasonID\":554213,\"dvdEpisodeNumber\":null,\"dvdSeason\":null,\"episodeName\":null,\"firstAired\":\"2015-11-19\",\"id\":5410349,\"language\":{\"episodeName\":\"\",\"overview\":\"\"},\"lastUpdated\":1505752259,\"overview\":null},{\"absoluteNumber\":null,\"airedEpisodeNumber\":45,\"airedSeason\":0,\"airedSeasonID\":554213,\"dvdEpisodeNumber\":null,\"dvdSeason\":null,\"episodeName\":null,\"firstAired\":\"2015-11-20\",\"id\":5410350,\"language\":{\"episodeName\":\"\",\"overview\":\"\"},\"lastUpdated\":1505752260,\"overview\":null},{\"absoluteNumber\":null,\"airedEpisodeNumber\":46,\"airedSeason\":0,\"airedSeasonID\":554213,\"dvdEpisodeNumber\":null,\"dvdSeason\":null,\"episodeName\":null,\"firstAired\":\"2015-11-21\",\"id\":5410838,\"language\":{\"episodeName\":\"\",\"overview\":\"\"},\"lastUpdated\":1505752715,\"overview\":null},{\"absoluteNumber\":null,\"airedEpisodeNumber\":47,\"airedSeason\":0,\"airedSeasonID\":554213,\"dvdEpisodeNumber\":null,\"dvdSeason\":null,\"episodeName\":null,\"firstAired\":\"2015-11-21\",\"id\":5410839,\"language\":{\"episodeName\":\"\",\"overview\":\"\"},\"lastUpdated\":1505752716,\"overview\":null},{\"absoluteNumber\":null,\"airedEpisodeNumber\":48,\"airedSeason\":0,\"airedSeasonID\":554213,\"dvdEpisodeNumber\":null,\"dvdSeason\":null,\"episodeName\":null,\"firstAired\":\"2015-11-21\",\"id\":5410840,\"language\":{\"episodeName\":\"\",\"overview\":\"\"},\"lastUpdated\":1505752717,\"overview\":null},{\"absoluteNumber\":null,\"airedEpisodeNumber\":49,\"airedSeason\":0,\"airedSeasonID\":554213,\"dvdEpisodeNumber\":null,\"dvdSeason\":null,\"episodeName\":null,\"firstAired\":\"2015-11-21\",\"id\":5410842,\"language\":{\"episodeName\":\"\",\"overview\":\"\"},\"lastUpdated\":1505752720,\"overview\":null},{\"absoluteNumber\":null,\"airedEpisodeNumber\":50,\"airedSeason\":0,\"airedSeasonID\":554213,\"dvdEpisodeNumber\":null,\"dvdSeason\":null,\"episodeName\":null,\"firstAired\":\"2016-02-08\",\"id\":5509514,\"language\":{\"episodeName\":\"\",\"overview\":\"\"},\"lastUpdated\":1510405983,\"overview\":null},{\"absoluteNumber\":null,\"airedEpisodeNumber\":51,\"airedSeason\":0,\"airedSeasonID\":554213,\"dvdEpisodeNumber\":null,\"dvdSeason\":null,\"episodeName\":null,\"firstAired\":\"2016-02-11\",\"id\":5654849,\"language\":{\"episodeName\":\"\",\"overview\":\"\"},\"lastUpdated\":1484521095,\"overview\":null},{\"absoluteNumber\":null,\"airedEpisodeNumber\":52,\"airedSeason\":0,\"airedSeasonID\":554213,\"dvdEpisodeNumber\":null,\"dvdSeason\":null,\"episodeName\":null,\"firstAired\":\"2016-02-15\",\"id\":5654850,\"language\":{\"episodeName\":\"\",\"overview\":\"\"},\"lastUpdated\":1484430973,\"overview\":null},{\"absoluteNumber\":null,\"airedEpisodeNumber\":53,\"airedSeason\":0,\"airedSeasonID\":554213,\"dvdEpisodeNumber\":null,\"dvdSeason\":null,\"episodeName\":null,\"firstAired\":\"\",\"id\":5700065,\"language\":{\"episodeName\":\"\",\"overview\":\"\"},\"lastUpdated\":1498424628,\"overview\":null}],\"errors\":{\"invalidLanguage\":\"Some translations were not available in the specified language\"}}";

            //RootAll<SerieInfo> ser = JsonConvert.DeserializeObject<RootAll<SerieInfo>>(dd);

            string part = "s01e02e03";
            string seType = "s$e&e&";

            string DummyNseason = "";
            string DummyNEpisode = "";
            string DummyNEpisode2 = "";
            int indPart = 0;
            for (int j = 0; j < seType.Length; j++)
            {
                if (indPart == part.Length)
                {
                    DummyNEpisode = "";
                    DummyNEpisode2 = "";
                    break;
                }

                if (seType[j] == '$')
                {
                    DummyNseason += part[indPart];
                    if (indPart + 1 != part.Length && char.IsNumber(part[indPart + 1]))
                    {
                        indPart++;
                        DummyNseason += part[indPart];
                    }
                }
                else if (seType[j] == '&')
                {
                    if (DummyNEpisode == "")
                    {
                        DummyNEpisode += part[indPart];
                        if (indPart + 1 != part.Length && char.IsNumber(part[indPart + 1]))
                        {
                            indPart++;
                            DummyNEpisode += part[indPart];
                        }
                    }
                    else
                    {
                        DummyNEpisode2 += part[indPart];
                        if (indPart + 1 != part.Length && char.IsNumber(part[indPart + 1]))
                        {
                            indPart++;
                            DummyNEpisode2 += part[indPart];
                        }
                    }
                }
                else if (char.ToLower(seType[j]) != char.ToLower(part[indPart]))
                {
                    DummyNEpisode = "";
                    DummyNEpisode2 = "";
                    break;
                }

                indPart++;
            }

            if (!seType.Contains("$"))
                DummyNseason = "0";

            int.TryParse(DummyNEpisode2, out int Nepisode2);

            int.TryParse(DummyNseason, out int Nseason);
            int.TryParse(DummyNEpisode, out int Nepisode);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            //Serie s = new Serie(CurrentSerie);
            //s.Seasons = null;
            //Console.WriteLine();


            string responseString = "{\"data\":{\"id\":5155243,\"airedSeason\":1,\"airedSeasonID\":615993,\"airedEpisodeNumber\":1,\"episodeName\":\"The Strongest Man\",\"firstAired\":\"2015-10-04\",\"guestStars\":[],\"director\":\"Shingo Natsume\",\"directors\":[\"Shingo Natsume\"],\"writers\":[\"Tomohiro Suzuki\"],\"overview\":\"Saitama is a guy who’s a hero for fun. After saving a child from certain death, he decided to become a hero and trained hard for three years. Though he’s now so strong he can defeat any opponent with a single punch, lately he feels as if, in exchange for overwhelming power, he’s lost something even more important.\",\"language\":{\"episodeName\":\"en\",\"overview\":\"en\"},\"productionCode\":\"\",\"showUrl\":\"\",\"lastUpdated\":1514138888,\"dvdDiscid\":\"\",\"dvdSeason\":1,\"dvdEpisodeNumber\":1,\"dvdChapter\":null,\"absoluteNumber\":1,\"filename\":\"episodes/293088/5155243.jpg\",\"seriesId\":293088,\"lastUpdatedBy\":489502,\"airsAfterSeason\":null,\"airsBeforeSeason\":null,\"airsBeforeEpisode\":null,\"thumbAuthor\":356189,\"thumbAdded\":\"2015-09-19 12:05:12\",\"thumbWidth\":\"400\",\"thumbHeight\":\"225\",\"imdbId\":\"tt4569062\",\"siteRating\":7.5,\"siteRatingCount\":4}}";
            RootObject fullEpisode = JsonConvert.DeserializeObject<RootObject>(responseString);
        }
        
        private void button3_Click(object sender, EventArgs e)
        {
            //AddInfo addInfo = new AddInfo(new string[] { "" }, listBoxSerie);
            //addInfo.ShowDialog();

            //Episode prima = series[5].Seasons[0].Episodes[0];
            //prima.visulizzazioni.Add(DateTime.Now);
            //Episode dopo = new Episode(prima);
            //dopo.visulizzazioni.Add(DateTime.Now);


            //FlashWindow.Flash(this);
            foreach (RecognizerInfo ri in SpeechRecognitionEngine.InstalledRecognizers())
            {
                Console.WriteLine(ri.Culture);
            }
        }
    }


    #region Class For Info from Internet
    public class Root<T>
    {
        public List<T> data { get; set; }
    }

    public class RootAll<T>
    {
        public Links links { get; set; }
        public List<T> data { get; set; }
        public Errors errors { get; set; }
    }
    
    public class SerieInfo
    {
        public List<string> aliases { get; set; }
        public string banner { get; set; }
        public string firstAired { get; set; }
        public int id { get; set; }
        public string network { get; set; }
        public string overview { get; set; }
        public string seriesName { get; set; }
        public string status { get; set; }

        public int MyProperty { get; set; }
    }

    public class Links
    {
        public int first { get; set; }
        public int last { get; set; }
        public object next { get; set; }
        public object prev { get; set; }
    }

    public class Language
    {
        public string episodeName { get; set; }
        public string overview { get; set; }
    }

    public class EpisodeInfo
    {
        public int? absoluteNumber { get; set; }
        public int airedEpisodeNumber { get; set; }
        public int airedSeason { get; set; }
        public int airedSeasonID { get; set; }
        public int? dvdEpisodeNumber { get; set; }
        public int? dvdSeason { get; set; }
        public string episodeName { get; set; }
        public string firstAired { get; set; }
        public int id { get; set; }
        public Language language { get; set; }
        public int lastUpdated { get; set; }
        public string overview { get; set; }

        public override string ToString()
        {
            return episodeName == null ? "null" : episodeName;
        }
    }
    
    public class FullEpisodeInfo
    {
        public int id { get; set; }
        public object airedSeason { get; set; }
        public object airedSeasonID { get; set; }
        public int airedEpisodeNumber { get; set; }
        public string episodeName { get; set; }
        public string firstAired { get; set; }
        public List<object> guestStars { get; set; }
        public string director { get; set; }
        public List<object> directors { get; set; }
        public List<object> writers { get; set; }
        public string overview { get; set; }
        public Language language { get; set; }
        public string productionCode { get; set; }
        public string showUrl { get; set; }
        public int lastUpdated { get; set; }
        public string dvdDiscid { get; set; }
        public object dvdSeason { get; set; }
        public object dvdEpisodeNumber { get; set; }
        public object dvdChapter { get; set; }
        public object absoluteNumber { get; set; }
        public string filename { get; set; }
        public int seriesId { get; set; }
        public int lastUpdatedBy { get; set; }
        public object airsAfterSeason { get; set; }
        public object airsBeforeSeason { get; set; }
        public object airsBeforeEpisode { get; set; }
        public int thumbAuthor { get; set; }
        public string thumbAdded { get; set; }
        public object thumbWidth { get; set; }
        public object thumbHeight { get; set; }
        public string imdbId { get; set; }
        public double siteRating { get; set; }
        public int siteRatingCount { get; set; }



        public Image preview { get; set; }
    }
    public class Errors
    {
        public List<string> invalidFilters { get; set; }
        public string invalidLanguage { get; set; }
        public List<string> invalidQueryParams { get; set; }
    }
    

    public class RootObject
    {
        public FullEpisodeInfo data { get; set; }
    }
    #endregion
}
