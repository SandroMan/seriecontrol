﻿using System.Collections.Generic;
using System.Drawing;
using System.IO;

namespace SerieControl
{
    public class Serie
    {
        #region SetUp
        public Serie(Serie serie)
        {
            CopyProperties.CopyPropertiesTo(serie, this);
        }

        public Serie(string name, List<Season> seasons)
        {
            this.name = name;
            Seasons = seasons;
        }

        public Serie(string name)
        {
            this.name = name;
            Seasons = new List<Season>();
        }
        #endregion

        public List<Season> Seasons { get; set; }

        #region Info
        public string name
        {
            get { return infos.Count == 0 ? null : info.name; }
            set { if (infos.Count != 0) info.name = value; }
        }
        public SerieMyInfo info
        {
            get { return infos.Count == 0 ? null : infos[indexInfo]; }
            set { if (infos.Count != 0) infos[indexInfo] = value; }
        }

        int indexInfo = 0;
        public List<SerieMyInfo> infos { get; set; } = new List<SerieMyInfo>() { new SerieMyInfo() };

        public List<Person> Ideatori { get; set; }

        #region Info sull'ep che non cambiano in base alla lingua
        public Image preview { get; set; }
        
        //public string BestPictiureFormat { get; set; } //1080p/i |1080x720|16:9 HDTV
        //public string BestAudioFormat { get; set; }
        
        public Dictionary<Site, double> sitesRatings { get; set; }
        
        //anno inizio e fine
        #endregion
        #endregion

        #region Proprieties
        public bool Completata
        {
            set
            {
                foreach (var seas in Seasons)
                {
                    seas.Completata = value;
                }
            }
            get
            { //return Episodes[Episodes.Count].visto; 
                foreach (var seas in Seasons)
                {
                    if (!seas.Completata)
                        return false;
                }

                return true;
            }
        }

        public int DaVedere
        {
            get
            { //return Episodes[Episodes.Count].visto; 
                for (int i = 0; i < Seasons.Count; i++)
                {
                    if (Seasons[i].DaVedere != -1)
                        return i;
                }

                return -1;
            }
        }

        public int DaIniziare
        {
            get
            { //return Episodes[Episodes.Count].visto; 
                for (int i = 0; i < Seasons.Count; i++)
                {
                    if (Seasons[i].DaIniziare == 0)
                        return i;
                }

                return -1;
            }
        }

        public bool Iniziata
        {
            get
            { //return Episodes[Episodes.Count].visto; 
                foreach (var ses in Seasons)
                {
                    if (ses.Iniziata)
                        return true;
                }

                return false;
            }
        }

        public override string ToString()
        {
            return name;
        }
        #endregion
    }
}
