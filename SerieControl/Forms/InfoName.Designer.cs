﻿namespace SerieControl
{
    partial class InfoName
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelInEsame = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.listBoxSE = new System.Windows.Forms.ListBox();
            this.textBoxAddSE = new System.Windows.Forms.TextBox();
            this.buttonAddSE = new System.Windows.Forms.Button();
            this.labelError = new System.Windows.Forms.Label();
            this.buttonRetry = new System.Windows.Forms.Button();
            this.buttonRemoveDot = new System.Windows.Forms.Button();
            this.buttonChangeTitle = new System.Windows.Forms.Button();
            this.textBoxChangeTitle = new System.Windows.Forms.TextBox();
            this.checkBoxcorruptedText = new System.Windows.Forms.CheckBox();
            this.richTextBoxType = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            // 
            // labelInEsame
            // 
            this.labelInEsame.AutoSize = true;
            this.labelInEsame.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelInEsame.Location = new System.Drawing.Point(12, 9);
            this.labelInEsame.Name = "labelInEsame";
            this.labelInEsame.Size = new System.Drawing.Size(92, 24);
            this.labelInEsame.TabIndex = 0;
            this.labelInEsame.Text = "In esame:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(12, 48);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(146, 24);
            this.label3.TabIndex = 2;
            this.label3.Text = "Stringa da usare";
            // 
            // listBoxSE
            // 
            this.listBoxSE.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.listBoxSE.FormattingEnabled = true;
            this.listBoxSE.Location = new System.Drawing.Point(703, 9);
            this.listBoxSE.Name = "listBoxSE";
            this.listBoxSE.Size = new System.Drawing.Size(165, 108);
            this.listBoxSE.TabIndex = 4;
            // 
            // textBoxAddSE
            // 
            this.textBoxAddSE.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxAddSE.Location = new System.Drawing.Point(703, 123);
            this.textBoxAddSE.Name = "textBoxAddSE";
            this.textBoxAddSE.Size = new System.Drawing.Size(100, 20);
            this.textBoxAddSE.TabIndex = 5;
            // 
            // buttonAddSE
            // 
            this.buttonAddSE.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonAddSE.Location = new System.Drawing.Point(809, 121);
            this.buttonAddSE.Name = "buttonAddSE";
            this.buttonAddSE.Size = new System.Drawing.Size(59, 23);
            this.buttonAddSE.TabIndex = 6;
            this.buttonAddSE.Text = "Add";
            this.buttonAddSE.UseVisualStyleBackColor = true;
            this.buttonAddSE.Click += new System.EventHandler(this.buttonAddSE_Click);
            // 
            // labelError
            // 
            this.labelError.AutoSize = true;
            this.labelError.Location = new System.Drawing.Point(81, 135);
            this.labelError.Name = "labelError";
            this.labelError.Size = new System.Drawing.Size(116, 13);
            this.labelError.TabIndex = 7;
            this.labelError.Text = "Errore con questo titolo";
            this.labelError.Visible = false;
            // 
            // buttonRetry
            // 
            this.buttonRetry.Location = new System.Drawing.Point(16, 130);
            this.buttonRetry.Name = "buttonRetry";
            this.buttonRetry.Size = new System.Drawing.Size(59, 23);
            this.buttonRetry.TabIndex = 8;
            this.buttonRetry.Text = "Riprova";
            this.buttonRetry.UseVisualStyleBackColor = true;
            this.buttonRetry.Visible = false;
            this.buttonRetry.Click += new System.EventHandler(this.buttonRetry_Click);
            // 
            // buttonRemoveDot
            // 
            this.buttonRemoveDot.Location = new System.Drawing.Point(16, 101);
            this.buttonRemoveDot.Name = "buttonRemoveDot";
            this.buttonRemoveDot.Size = new System.Drawing.Size(88, 23);
            this.buttonRemoveDot.TabIndex = 9;
            this.buttonRemoveDot.Text = "Remove Dot";
            this.buttonRemoveDot.UseVisualStyleBackColor = true;
            this.buttonRemoveDot.Click += new System.EventHandler(this.buttonRemoveDot_Click);
            // 
            // buttonChangeTitle
            // 
            this.buttonChangeTitle.Location = new System.Drawing.Point(110, 101);
            this.buttonChangeTitle.Name = "buttonChangeTitle";
            this.buttonChangeTitle.Size = new System.Drawing.Size(88, 23);
            this.buttonChangeTitle.TabIndex = 10;
            this.buttonChangeTitle.Text = "Change Title";
            this.buttonChangeTitle.UseVisualStyleBackColor = true;
            this.buttonChangeTitle.Click += new System.EventHandler(this.buttonChangeTitle_Click);
            // 
            // textBoxChangeTitle
            // 
            this.textBoxChangeTitle.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxChangeTitle.Location = new System.Drawing.Point(204, 103);
            this.textBoxChangeTitle.Name = "textBoxChangeTitle";
            this.textBoxChangeTitle.Size = new System.Drawing.Size(493, 20);
            this.textBoxChangeTitle.TabIndex = 11;
            this.textBoxChangeTitle.Visible = false;
            // 
            // checkBoxcorruptedText
            // 
            this.checkBoxcorruptedText.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBoxcorruptedText.AutoSize = true;
            this.checkBoxcorruptedText.Checked = true;
            this.checkBoxcorruptedText.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxcorruptedText.Location = new System.Drawing.Point(560, 125);
            this.checkBoxcorruptedText.Name = "checkBoxcorruptedText";
            this.checkBoxcorruptedText.Size = new System.Drawing.Size(140, 17);
            this.checkBoxcorruptedText.TabIndex = 12;
            this.checkBoxcorruptedText.Text = "Check for corrupted text";
            this.checkBoxcorruptedText.UseVisualStyleBackColor = true;
            this.checkBoxcorruptedText.CheckedChanged += new System.EventHandler(this.checkBoxcorruptedText_CheckedChanged);
            // 
            // richTextBoxType
            // 
            this.richTextBoxType.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.richTextBoxType.Location = new System.Drawing.Point(16, 75);
            this.richTextBoxType.Name = "richTextBoxType";
            this.richTextBoxType.Size = new System.Drawing.Size(681, 22);
            this.richTextBoxType.TabIndex = 13;
            this.richTextBoxType.Text = "";
            this.richTextBoxType.TextChanged += new System.EventHandler(this.richTextBoxType_TextChanged);
            // 
            // InfoName
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(880, 168);
            this.Controls.Add(this.richTextBoxType);
            this.Controls.Add(this.checkBoxcorruptedText);
            this.Controls.Add(this.textBoxChangeTitle);
            this.Controls.Add(this.buttonChangeTitle);
            this.Controls.Add(this.buttonRemoveDot);
            this.Controls.Add(this.buttonRetry);
            this.Controls.Add(this.labelError);
            this.Controls.Add(this.buttonAddSE);
            this.Controls.Add(this.textBoxAddSE);
            this.Controls.Add(this.listBoxSE);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.labelInEsame);
            this.Name = "InfoName";
            this.Text = "InfoName";
            this.Load += new System.EventHandler(this.InfoName_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelInEsame;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ListBox listBoxSE;
        private System.Windows.Forms.TextBox textBoxAddSE;
        private System.Windows.Forms.Button buttonAddSE;
        private System.Windows.Forms.Label labelError;
        private System.Windows.Forms.Button buttonRetry;
        private System.Windows.Forms.Button buttonRemoveDot;
        private System.Windows.Forms.Button buttonChangeTitle;
        private System.Windows.Forms.TextBox textBoxChangeTitle;
        private System.Windows.Forms.CheckBox checkBoxcorruptedText;
        private System.Windows.Forms.RichTextBox richTextBoxType;
    }
}