﻿namespace SerieControl
{
    public class Location
    {
        public ulong ID { get; set; } //salvare l'ID e non tutta la classe

        public string language { get; set; }
        public string name { get; set; }

        public int DDNord { get; set; }
        public int DDEst { get; set; }
    }
}