﻿using NReco.VideoInfo;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;

namespace SerieControl
{
    public class Episode
    {
        static FFProbe ffProbe = new FFProbe();

        #region SetUp
        public Episode()
        {
            infos.Add(new EpisodeMyInfo());
        }

        public Episode(Episode episode)
        {
            CopyProperties.CopyPropertiesTo(episode, this);
            infos = new List<EpisodeMyInfo>();
            foreach (var info in episode.infos)
            {
                EpisodeMyInfo toAdd = new EpisodeMyInfo();
                CopyProperties.CopyPropertiesTo(episode.infos, toAdd);
                infos.Add(toAdd);
            }

            visulizzazioni = new List<DateTime>(visulizzazioni);
        }

        public Episode(string pathPrima, string ID, string title = "", string description = "", Image image = null)
        {
            infos.Add(new EpisodeMyInfo());

            TagLib.File file;
            try
            {
                file = TagLib.File.Create(pathPrima);
            }
            catch
            {
                path = pathPrima;
                if (title == "")
                {
                    title = Path.GetFileNameWithoutExtension(pathPrima);
                }

                this.titolo = title;
                NoDATA = true;
                return;
            }
            
            Lenght = file.Properties.Duration.TotalMilliseconds;
            Width = file.Properties.VideoWidth;
            Height = file.Properties.VideoHeight;
            

            this.ID = ID;

            if (title == "")
            {
                if (file.Tag.Title != null)
                    title = file.Tag.Title;
                else
                    title = Path.GetFileNameWithoutExtension(pathPrima);
            }

            titolo = title;

            if (description == "" && file.Tag.Comment != null)
                description = file.Tag.Comment;
            this.info.description = description;

            /*
            if (image == null)
                image = file.Tag.Pictures[0];
                */
            preview = image;

            MediaInfo videoInfo = ffProbe.GetMediaInfo(pathPrima);
            fps = (int)videoInfo.Streams[0].FrameRate;

            //string finale = directoryDestinazione + $"[{ID}] " + title + Path.GetExtension(pathPrima);

            //Controller.ChangeStatus($"Creazione episodio: [{ID}] {titolo}");
            //Controller.Copy(pathPrima, finale);

            FineVideo = Lenght - TimeSpan.FromMilliseconds(Lenght).Milliseconds; //tolgo i millisecondi finali
            path = pathPrima;
        }
        #endregion

        public bool NoDATA { get; set; } = false;

        public string ID { get; set; } = "N/A";
        
        #region Proprieties
        public bool visto
        {
            set
            {
                if (value)
                {
                    tempoVisto = FineVideo;
                    if (visulizzazioni.Count == 0)
                        visulizzazioni.Add(DateTime.Now);

                }
                else
                {
                    tempoVisto = 0;
                    visulizzazioni.Clear();
                }
            }
            get { return FineVideo != 0 && tempoVisto >= FineVideo; }
        }

        public double tempoVisto { get; set; } = 0;
        public string BetterTempoVisto
        {
            get { return TimeSpan.FromMilliseconds(tempoVisto).ToString(@"h\:m\:s\.fff"); }
        }

        public bool Exist
        {
            get { return File.Exists(path); }
        }
        public string BetterLenght
        {
            get { return TimeSpan.FromMilliseconds(Lenght).ToString(@"h\:m\:s\.fff"); }
        }
        public string BetterInizioSigla
        {
            get { return TimeSpan.FromMilliseconds(InizioSigla).ToString(@"h\:m\:s\.fff"); }
        }
        public string BetterFineSigla
        {
            get { return TimeSpan.FromMilliseconds(FineSigla).ToString(@"h\:m\:s\.fff"); }
        }

        public string BetterFineVideo
        {
            get { return TimeSpan.FromMilliseconds(FineVideo).ToString(@"h\:m\:s\.fff"); }
        }
        public override string ToString()
        {
            return titolo;
        }
        #endregion

        #region File Info
        public string path { get; set; } = "";

        public double Lenght { get; set; } = 0;

        public int Height { get; set; } = 0;
        public int Width { get; set; } = 0;
        public double fps { get; set; } = 0;
        #endregion

        #region Episode Info
        public string titolo
        {
            get { return infos.Count == 0 ? null : info.title; }
            set { if(infos.Count != 0) info.title = value; }
        }
        public EpisodeMyInfo info
        {
            get { return infos.Count == 0 ? null : infos[indexInfo]; }
            set { if (infos.Count != 0) infos[indexInfo] = value; }
        }

        int indexInfo = 0;
        public List<EpisodeMyInfo> infos { get; set; } = new List<EpisodeMyInfo>();

        public List<Location> locations { get; set; }
        public Person registra { get; set; }
        public List<Person> scrittori { get; set; }
        public List<Colonna> colonneSonore { get; set; }
        public List<Personaggio> personaggi { get; set; }

        #region Info sull'ep che non cambiano in base alla lingua
        public Image preview { get; set; }
        
        public string firstAired { get; set; } 
        
        public string BestPictiureFormat { get; set; } //1080p/i |1080x720|16:9 HDTV
        public string BestAudioFormat { get; set; }

        //public string dvdDiscid { get; set; }
        //public object dvdSeason { get; set; }
        //public object dvdEpisodeNumber { get; set; }
        //public object dvdChapter { get; set; }
        //public Dictionary<Site, double> sitesRatings { get; set; }
        #endregion
        #endregion


        #region New Episode Info
        public double InizioSigla { get; set; } = 0;
        public double FineSigla { get; set; } = 0;

        public double FineVideo { get; set; } = 0;
        public List<DateTime> visulizzazioni { get; set; } = new List<DateTime>();
        #endregion
    }
}
