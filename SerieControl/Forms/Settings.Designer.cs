﻿namespace SerieControl
{
    partial class Settings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.checkBoxAutoSaveTimer = new System.Windows.Forms.CheckBox();
            this.checkBoxSaveExit = new System.Windows.Forms.CheckBox();
            this.comboBoxCulture = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.buttonRefresh = new System.Windows.Forms.Button();
            this.checkBoxRefreshFocus = new System.Windows.Forms.CheckBox();
            this.checkBoxNoEpisode = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // checkBoxAutoSaveTimer
            // 
            this.checkBoxAutoSaveTimer.AutoSize = true;
            this.checkBoxAutoSaveTimer.Location = new System.Drawing.Point(15, 40);
            this.checkBoxAutoSaveTimer.Name = "checkBoxAutoSaveTimer";
            this.checkBoxAutoSaveTimer.Size = new System.Drawing.Size(99, 17);
            this.checkBoxAutoSaveTimer.TabIndex = 31;
            this.checkBoxAutoSaveTimer.Text = "AutoSaveTimer";
            this.checkBoxAutoSaveTimer.UseVisualStyleBackColor = true;
            this.checkBoxAutoSaveTimer.CheckedChanged += new System.EventHandler(this.checkBoxAutoSaveTimer_CheckedChanged);
            // 
            // checkBoxSaveExit
            // 
            this.checkBoxSaveExit.AutoSize = true;
            this.checkBoxSaveExit.Location = new System.Drawing.Point(15, 63);
            this.checkBoxSaveExit.Name = "checkBoxSaveExit";
            this.checkBoxSaveExit.Size = new System.Drawing.Size(103, 17);
            this.checkBoxSaveExit.TabIndex = 30;
            this.checkBoxSaveExit.Text = "Save before exit";
            this.checkBoxSaveExit.UseVisualStyleBackColor = true;
            this.checkBoxSaveExit.CheckedChanged += new System.EventHandler(this.checkBoxSaveExit_CheckedChanged);
            // 
            // comboBoxCulture
            // 
            this.comboBoxCulture.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxCulture.FormattingEnabled = true;
            this.comboBoxCulture.Location = new System.Drawing.Point(197, 12);
            this.comboBoxCulture.Name = "comboBoxCulture";
            this.comboBoxCulture.Size = new System.Drawing.Size(251, 21);
            this.comboBoxCulture.TabIndex = 32;
            this.comboBoxCulture.SelectedIndexChanged += new System.EventHandler(this.comboBoxCulture_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(179, 13);
            this.label1.TabIndex = 33;
            this.label1.Text = "Lingue per il riconoscimento vocale: ";
            // 
            // buttonRefresh
            // 
            this.buttonRefresh.Location = new System.Drawing.Point(454, 10);
            this.buttonRefresh.Name = "buttonRefresh";
            this.buttonRefresh.Size = new System.Drawing.Size(38, 23);
            this.buttonRefresh.TabIndex = 34;
            this.buttonRefresh.Text = "Ref.";
            this.buttonRefresh.UseVisualStyleBackColor = true;
            this.buttonRefresh.Click += new System.EventHandler(this.buttonRefresh_Click);
            // 
            // checkBoxRefreshFocus
            // 
            this.checkBoxRefreshFocus.AutoSize = true;
            this.checkBoxRefreshFocus.Location = new System.Drawing.Point(133, 40);
            this.checkBoxRefreshFocus.Name = "checkBoxRefreshFocus";
            this.checkBoxRefreshFocus.Size = new System.Drawing.Size(191, 17);
            this.checkBoxRefreshFocus.TabIndex = 35;
            this.checkBoxRefreshFocus.Text = "Refresh Listbox when click on form";
            this.checkBoxRefreshFocus.UseVisualStyleBackColor = true;
            this.checkBoxRefreshFocus.CheckedChanged += new System.EventHandler(this.checkBoxRefreshFocus_CheckedChanged);
            // 
            // checkBoxNoEpisode
            // 
            this.checkBoxNoEpisode.AutoSize = true;
            this.checkBoxNoEpisode.Location = new System.Drawing.Point(133, 63);
            this.checkBoxNoEpisode.Name = "checkBoxNoEpisode";
            this.checkBoxNoEpisode.Size = new System.Drawing.Size(251, 17);
            this.checkBoxNoEpisode.TabIndex = 36;
            this.checkBoxNoEpisode.Text = "Add Season/serie even if they have no episode";
            this.checkBoxNoEpisode.UseVisualStyleBackColor = true;
            this.checkBoxNoEpisode.CheckedChanged += new System.EventHandler(this.checkBoxNoEpisode_CheckedChanged);
            // 
            // Settings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(501, 99);
            this.Controls.Add(this.checkBoxNoEpisode);
            this.Controls.Add(this.checkBoxRefreshFocus);
            this.Controls.Add(this.buttonRefresh);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.comboBoxCulture);
            this.Controls.Add(this.checkBoxAutoSaveTimer);
            this.Controls.Add(this.checkBoxSaveExit);
            this.MaximumSize = new System.Drawing.Size(517, 138);
            this.MinimumSize = new System.Drawing.Size(418, 138);
            this.Name = "Settings";
            this.Text = "Settings";
            this.Load += new System.EventHandler(this.Settings_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox checkBoxAutoSaveTimer;
        private System.Windows.Forms.CheckBox checkBoxSaveExit;
        private System.Windows.Forms.ComboBox comboBoxCulture;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button buttonRefresh;
        private System.Windows.Forms.CheckBox checkBoxRefreshFocus;
        private System.Windows.Forms.CheckBox checkBoxNoEpisode;
    }
}