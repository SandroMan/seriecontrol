﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SerieControl
{
    public partial class InfoName : Form
    {
        public static List<string> SE { get; set; } = new List<string>() { "$x&", "$e&", "s$e&e&", "s$-e&-e&", "s$e&" };
        public static bool checkcorrupted { get; set; } = true;
        public static string type { get; set; } = "* §SE§ - §name§ *";



        Serie serie;
        public InfoName(Serie serie)
        {
            InitializeComponent();
            this.serie = serie;
        }

        private void InfoName_Load(object sender, EventArgs e)
        {
            foreach (var se in SE)
            {
                listBoxSE.Items.Add(se);
            }
            checkBoxcorruptedText.Checked = checkcorrupted;
            richTextBoxType.Text = type;

            CheckKeyword(richTextBoxType);
            new Thread(() => GetInfo()).Start();
        }





        Object changeText = new Object();

        string title = "";
        public Serie output { get; set; }
        private void GetInfo()
        {
            bool risposta;
            Season[] seasons = new Season[256];

            for (int s = 0; s < serie.Seasons.Count; s++)
            {
                Season seas = serie.Seasons[s];
                for (int e = 0; e < seas.Episodes.Count; e++)
                {
                    if(title == "")
                    {
                        title = seas.Episodes[e].titolo;
                        if (!title.Contains(' '))
                            title = title.Replace('.', ' ');
                    }

                    

                    
                    this.Invoke((Action)delegate { labelInEsame.Text = "In Esame: " + title; });


                    int Nseason = -1;
                    int Nepisode = -1;
                    int Nepisode2 = 0;
                    string Name = "";
                    string tipologia = (string)richTextBoxType.Invoke(new Func<string>(() => richTextBoxType.Text));


                    try
                    {
                        #region Get Info
                        int t = 0;
                        string special = "";
                        string part = "";
                        for (int i = 0; i < title.Length; i++)
                        {
                            if (t >= tipologia.Length)
                                break;

                            char c = title[i];

                            if (tipologia[t] == '*')
                            {
                                if (t + 1 == tipologia.Length)
                                    break;


                                if (tipologia[t + 1] == title[i])
                                {
                                    t += 2;
                                    i++;
                                }
                                /*if (tipologia[t + 1] == title[i] && tipologia[t + 2] == title[i + 1])
                                {
                                    t += 3;
                                    i += 2;
                                }
                                else if (tipologia[t + 2] == '*' || tipologia[t + 2] == '?' || tipologia[t + 2] == '§')
                                {
                                    t += 2;
                                    i+= 1;
                                }*/
                                else
                                    continue;

                            }

                            if (tipologia[t] == '?') // non mi interessa di che carattere è
                            {
                                t++;
                            }
                            else if (tipologia[t] == '§')
                            {
                                if (special == "")
                                {
                                    t++;
                                    while (tipologia[t] != '§')
                                    {
                                        special += tipologia[t];
                                        t++;
                                    }
                                    part = "";
                                }
                                else
                                {
                                    if (((t + 1 != tipologia.Length && tipologia[t + 1] == title[i]) || (t + 1 == tipologia.Length && i + 1 == title.Length)) && 
                                        ((t + 2 >= tipologia.Length && i + 2 >= title.Length) || (t + 2 < tipologia.Length && (tipologia[t + 2] == '*' || tipologia[t + 2] == '§' || tipologia[t + 2] == title[i + 1])))
                                        
                                        ) //&& (t + 2 == tipologia.Length || tipologia[t + 2] == '*' || tipologia[t + 2] == '§' || tipologia[t + 2] == title[i + 1]))
                                    {
                                        if(t + 1 == tipologia.Length && i + 1 == title.Length)
                                            part += title[i];

                                        t += 2;
                                        part = part.Trim();

                                        if (special == "SE")
                                        {
                                            #region SE
                                            string DummyNseason, DummyNEpisode, DummyNEpisode2;
                                            int indPart;
                                            foreach (var seType in SE)
                                            {
                                                DummyNseason = "";
                                                DummyNEpisode = "";
                                                DummyNEpisode2 = "";
                                                indPart = 0;

                                                for (int j = 0; j < seType.Length; j++)
                                                {
                                                    if (indPart == part.Length)
                                                    {
                                                        DummyNEpisode = "";
                                                        DummyNEpisode2 = "";
                                                        break;
                                                    }

                                                    if (seType[j] == '$')
                                                    {
                                                        DummyNseason += part[indPart];
                                                        if (indPart + 1 != part.Length && char.IsNumber(part[indPart + 1]))
                                                        {
                                                            indPart++;
                                                            DummyNseason += part[indPart];
                                                        }
                                                    }
                                                    else if (seType[j] == '&')
                                                    {
                                                        if (DummyNEpisode == "")
                                                        {
                                                            DummyNEpisode += part[indPart];
                                                            if (indPart + 1 != part.Length && char.IsNumber(part[indPart + 1]))
                                                            {
                                                                indPart++;
                                                                DummyNEpisode += part[indPart];
                                                            }
                                                        }
                                                        else
                                                        {
                                                            DummyNEpisode2 += part[indPart];
                                                            if (indPart + 1 != part.Length && char.IsNumber(part[indPart + 1]))
                                                            {
                                                                indPart++;
                                                                DummyNEpisode2 += part[indPart];
                                                            }
                                                        }
                                                    }
                                                    else if (char.ToLower(seType[j]) != char.ToLower(part[indPart]))
                                                    {
                                                        DummyNEpisode = "";
                                                        DummyNEpisode2 = "";
                                                        break;
                                                    }

                                                    indPart++;
                                                }

                                                if (!seType.Contains("$"))
                                                    DummyNseason = "1";
                                                
                                                int.TryParse(DummyNEpisode2, out Nepisode2);

                                                if (int.TryParse(DummyNseason, out Nseason) && int.TryParse(DummyNEpisode, out Nepisode))
                                                    break;
                                            }
                                            #endregion
                                        }
                                        else if (special == "name")
                                        {
                                            Name = part;
                                        }


                                        special = "";
                                    }
                                }

                                part += title[i];
                            }
                            else if (tipologia[t] == title[i])
                            {
                                t++;
                            }
                            else
                            {
                                //Errore
                                this.Invoke((Action)delegate { buttonRetry.Visible = true; labelError.Text = "Errore con questo titolo"; labelError.Visible = true; });
                                lock (changeText)
                                {
                                    Monitor.Wait(changeText);
                                }
                                t = 0;
                                i = -1;
                                tipologia = richTextBoxType.Text;
                                continue;
                            }

                        }
                        #endregion
                    }
                    catch
                    {
                        Nseason = -1;
                        Nepisode = -1;
                        Name = "";
                    }

                    if(Nseason == -1 || Nepisode == -1 || Nepisode == 0 || Name == "")
                    {
                        /*
                        if(Nseason == -1 && (Nepisode == -1 || Nepisode == 0) && !tipologia.Contains("§SE§"))
                        {
                            Nseason = s;
                            Nepisode = e;
                        }
                        else 
                        */
                        if ((MessageBox.Show($"Numero di season: {Nseason}\nNumero di episode: {Nepisode}\nNome dell'episodio: {Name}|\ncontinuare comunque ?", "Numero di episodio / Numero di season / Nome dell'episodio mancanti, continuare comunque ?", MessageBoxButtons.YesNo) == DialogResult.No))
                        {
                            risposta = false;
                            //Errore
                            e--;
                            this.Invoke((Action)delegate { buttonRetry.Visible = true; labelError.Text = "Errore con questo titolo"; labelError.Visible = true; });
                            lock (changeText)
                            {
                                Monitor.Wait(changeText);
                            }

                            this.BeginInvoke((Action)delegate { tipologia = richTextBoxType.Text; });
                            continue;
                        }
                        else
                        {
                            risposta = true;
                        }

                        if (Nseason == -1)
                            Nseason = s + 1;

                        if (Nepisode == -1)
                            Nepisode = e;
                    }
                    else if (checkBoxcorruptedText.Checked && Name.Contains('�'))
                    {
                        e--;
                        this.Invoke((Action)delegate { buttonRetry.Visible = true; labelError.Text = "E' stato rilevato un carattere \"corrotto\" nel nome dell'episodio, cambia il titolo o spunta la casella a lato"; labelError.Visible = true; });
                        lock (changeText)
                        {
                            Monitor.Wait(changeText);
                        }
                        continue;
                    }

                    title = "";

                    Episode ep = new Episode(seas.Episodes[e]);

                    if (Name.Trim() != "")
                        ep.titolo = Name.Trim();
                    else
                        ep.titolo = "???";

                    if (Nepisode != -1)
                    {
                        if(Nepisode2 == 0)
                            ep.ID = Nepisode + "";
                        else
                            ep.ID = Nepisode + "-" + Nepisode2;
                    }

                    Nseason--;
                    if (seasons[Nseason] == null)
                    {
                        if (serie.Seasons.Count - 1 < Nseason)
                        {
                            seasons[Nseason] = new Season("season " + (Nseason + 1));
                        }
                        else
                        {
                            seasons[Nseason] = new Season(serie.Seasons[Nseason]);
                            seasons[Nseason].Episodes = new List<Episode>();
                        }
                    }

                    if (Nepisode == -1)
                        seasons[Nseason].Episodes.Add(ep);
                    else
                    {
                        bool set = false;
                        for (int i = 0; i < seasons[Nseason].Episodes.Count; i++)
                        {
                            if (int.Parse(seasons[Nseason].Episodes[i].ID.Split('-')[0]) > Nepisode)
                            {
                                seasons[Nseason].Episodes.Insert(i, ep);
                                set = true;
                                break;
                            }
                        }

                        if (!set)
                            seasons[Nseason].Episodes.Add(ep);
                    }
                }
            }
            output = new Serie(serie);
            output.Seasons = seasons.Where(s => s != null).ToList();
            this.Invoke((Action)delegate { this.Close(); });
        }

        #region Buttons
        private void buttonRetry_Click(object sender, EventArgs e)
        {
            buttonRetry.Visible = false;
            labelError.Visible = false;
            lock (changeText)
            {
                Monitor.Pulse(changeText);
            }
        }

        private void buttonRemoveDot_Click(object sender, EventArgs e)
        {
            title = title.Replace('.', ' ');
            labelInEsame.Text = "In Esame: " + title;
        }

        private void buttonChangeTitle_Click(object sender, EventArgs e)
        {
            if(textBoxChangeTitle.Visible)
            {
                title = textBoxChangeTitle.Text;
                labelInEsame.Text = "In Esame: " + title;
            }
            else
            {
                textBoxChangeTitle.Text = title;
            }

            textBoxChangeTitle.Visible = !textBoxChangeTitle.Visible;
        }

        private void buttonAddSE_Click(object sender, EventArgs e)
        {
            if (textBoxAddSE.Text.Trim() != "")
            {
                SE.Add(textBoxAddSE.Text);
                listBoxSE.Items.Add(textBoxAddSE.Text);
                textBoxAddSE.Text = "";
            }
        }

        #endregion

        #region RichTextBox
        private void richTextBoxType_TextChanged(object sender, EventArgs e)
        {
            

            int ind = richTextBoxType.SelectionStart;
            if (ind != 0 && richTextBoxType.Text[ind - 1] == '\n')
                ind--;

            richTextBoxType.Text = richTextBoxType.Text.Replace("\n", "");
            
            CheckKeyword(richTextBoxType);

            richTextBoxType.SelectionStart = ind;
            richTextBoxType.SelectionLength = 0;
        }
        
        Dictionary<string, Color> TYPES = new Dictionary<string, Color> { { "SE", Color.Green }, { "name", Color.Purple } };
        private void CheckKeyword(RichTextBox Rchtxt)
        {
            Rchtxt.SelectAll();
            Rchtxt.SelectionColor = Color.Black;

            int startTYPE = -1;
            for (int i = 0; i < Rchtxt.Text.Length; i++)
            {
                if(Rchtxt.Text[i] == '§')
                {
                    if(startTYPE == -1)
                    {
                        startTYPE = i;
                    }
                    else
                    {
                        string type = Rchtxt.Text.Substring(startTYPE + 1, i - startTYPE - 1);
                        Color color = Color.Red;
                        foreach (var t in TYPES)
                        {
                            if(t.Key == type)
                            {
                                color = t.Value;
                            }
                        }


                        Rchtxt.Select(startTYPE, i - startTYPE + 1);
                        Rchtxt.SelectionColor = color;
                        startTYPE = -1;
                    }
                }
                else if (Rchtxt.Text[i] == '*')
                {
                    Rchtxt.Select(i, 1);
                    Rchtxt.SelectionColor = Color.Orange;
                }
            }
        }
        #endregion

        private void checkBoxcorruptedText_CheckedChanged(object sender, EventArgs e)
        {
            checkcorrupted = checkBoxcorruptedText.Checked;
        }
    }
}
