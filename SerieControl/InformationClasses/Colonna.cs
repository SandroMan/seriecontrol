﻿namespace SerieControl
{
    public class Colonna
    {
        public ulong ID { get; set; } //salvare l'ID e non tutta la classe

        public string language { get; set; }

        public int startMilliSecond { get; set; }
        public int endMilliSecond { get; set; }
        public string name { get; set; }
    }
}
