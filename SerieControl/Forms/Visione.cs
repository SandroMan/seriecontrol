﻿using Gma.System.MouseKeyHook;
using NReco.VideoConverter;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Speech.Recognition;
using System.Speech.Recognition.SrgsGrammar;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SerieControl
{
    public partial class Visione : Form
    {
        List<Episode> playlist = new List<Episode>();
        int PlaylistIndex;
        Object lockPlayListIndex = new Object();

        public static byte volume { get; set; } = 100;
        public static bool saveEpEndNext { get; set; } = true;
        public static bool Precisino { get; set; } = false;
        public static bool VoiceRecognition { get; set; } = true;

        #region SetUp
        static IKeyboardMouseEvents m_GolbalHook = Hook.GlobalEvents();
        public Visione(Serie serie)
        {
            InitializeComponent();

            int add = 0;
            PlaylistIndex = serie.DaVedere == -1 ? 0 : serie.DaVedere;
            foreach (var seas in serie.Seasons)
            {
                if (PlaylistIndex == -1 && seas.DaVedere != -1)
                    PlaylistIndex = add + seas.DaVedere;

                foreach (var ep in seas.Episodes)
                {
                    if (ep.Exist)
                    {
                        playlist.Add(ep);
                        axVLCPlugin21.playlist.add(new Uri(ep.path).AbsoluteUri);
                        add++;
                    }
                }
            }

            //axVLCPlugin21.playlist.playItem(PlaylistIndex);
            SetUpVisione();

            //http://blogs.microsoft.co.il/sasha/2009/03/06/windows-7-taskbar-thumbnail-clip-and-custom-previews/
            //https://www.google.it/search?q=C%23+change+Thumbnail+ToolBar+image&biw=1920&bih=974&tbm=isch&source=iu&ictx=1&fir=fa93I4LOT47GzM%253A%252CbPrB8S5H2s1YgM%252C_&usg=__szLoUKkngMrxpy3b9LjtpzZfb5E%3D&sa=X&ved=0ahUKEwj6gLKP193aAhUkKcAKHckXD2UQ9QEIKjAA#imgrc=Rj8t3_syM3aipM:
        }

        public Visione(Season season)
        {
            InitializeComponent();

            PlaylistIndex = season.DaVedere == -1 ? 0 : season.DaVedere;
            foreach (var ep in season.Episodes)
            {
                if(ep.Exist)
                {
                    playlist.Add(ep);
                    axVLCPlugin21.playlist.add(new Uri(ep.path).AbsoluteUri);
                }

            }

            //axVLCPlugin21.playlist.playItem(PlaylistIndex);
            SetUpVisione();
        }

        public Visione(Episode episode)
        {
            InitializeComponent();
            //var uri = new Uri(@"C:\Users\SM\Desktop\Serie Animate\Gumball è di pessimo umore _ Lo straordinario mondo di Gumball _ Cartoon Network (480p_25fps_H264-128kbit_AAC).mp4");
            //var uri = new Uri(@"C:\Users\SM\Desktop\Serie Animate\FILTHY_FRANK_EXPOSES_HIMSELF.mp4");

            if(episode.Exist)
            {
                PlaylistIndex = 0;
                buttonPrev.Enabled = false;
                buttonNext.Enabled = false;
                playlist.Add(episode);
                axVLCPlugin21.playlist.add(new Uri(episode.path).AbsoluteUri);
            }

            SetUpVisione();
        }

        void SetUpVisione()
        {
            if(playlist.Count == 0)
            {
                Load += (s, e) => Close();
                return;
            }
            SetUpVoiceRecognition();

            this.Text = "Guardando: " + playlist[PlaylistIndex].titolo;
            axVLCPlugin21.volume = volume;
            labelVolume.Text = volume + "%";
            trackBar1.Value = volume;

            checkBoxPrecisino.Checked = Precisino;
            checkBoxVR.Checked = VoiceRecognition;
            checkBoxNextEpSave.Checked = saveEpEndNext;




            startMedia();
            m_GolbalHook.MouseMove += Visione_MouseMove;
            m_GolbalHook.KeyDown += Visione_KeyDown;

            trackBarVolume.Value = axVLCPlugin21.volume;
        }
        #endregion

        #region Voice Recognition
        static SpeechRecognitionEngine sre = null;
        void SetUpVoiceRecognition()
        {
            if (sre != null)
                return;

            if(Settings.cultureVR == null)
            {
                MessageBox.Show("Non è stata rilevata nessuna lingua per il Speech Recognition, per selezionarne una vai su File > Settings");
                checkBoxVR.Visible = false;
                return;
            }

            sre = new SpeechRecognitionEngine(Settings.cultureVR);

            SrgsDocument document = new SrgsDocument();
            document.Culture = new CultureInfo("en-US"); // add Culture Info


            var NSmall = new string[] { "one", "two", "three", "five", "ten", "fifteen", "twenty" };
            var N25 = new string[] { "zero", "twenty-five", "fifty", "seventy-five", "one-hundred", "one-hundred-twenty-five", "one-hundred-fifty" };
            sre.LoadGrammar(CreateGrammar(new string[] { "play", "pause", "restart", "fullscreen", "screenshot", "precisino", "previous", "next", "mute", "reverse" }));
            sre.LoadGrammar(CreateGrammar(new string[] { "episode", "sigla", "song" }, new string[] { "start", "end", "delede" }));
            sre.LoadGrammar(CreateGrammar(new string[] { "skip", "set" }, NSmall, new string[] { "second", "minute" }));
            sre.LoadGrammar(CreateGrammar(new string[] { "music" }, new string[] { "up", "down" }, NSmall));
            sre.LoadGrammar(CreateGrammar(new string[] { "music" }, new string[] { "set" }, N25));
            sre.LoadGrammar(CreateGrammar(new string[] { "music" }, new string[] { "mute" }));




            sre.SpeechRecognized +=
              new EventHandler<SpeechRecognizedEventArgs>(recognizer_SpeechRecognized);
            sre.SpeechRecognitionRejected += Recognizer_SpeechRecognitionRejected;

            // Configure the input to the SpeechRecognitionEngine object.
            sre.SetInputToDefaultAudioDevice();

            sre.EndSilenceTimeout = new TimeSpan(0, 0, 0, 1, 0);
            sre.EndSilenceTimeoutAmbiguous = new TimeSpan(0, 0, 0, 1, 0);

            // Start asynchronous recognition.
            sre.RecognizeAsync(RecognizeMode.Multiple);


        }

        static Grammar CreateGrammar(params string[][] lines)
        {
            SrgsDocument document = new SrgsDocument();
            document.Culture = new CultureInfo("en-US"); // add Culture Info

            SrgsRule root = new SrgsRule("root");
            root.Scope = SrgsRuleScope.Public;

            List<SrgsRule> rules = new List<SrgsRule>() { root };
            int RuleID = 0;
            foreach (var lin in lines)
            {
                SrgsRule rule = new SrgsRule("R" + RuleID);
                rule.Add(new SrgsOneOf(lin));

                root.Add(new SrgsRuleRef(rule, "conR" + RuleID));
                rules.Add(rule);
                RuleID++;
            }

            document.Rules.Add(rules.ToArray());
            document.Root = root;
            return new Grammar(document, "root");
        }

        //static string[] N100 = new string[] { "zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen", "twenty", "twenty-one", "twenty-two", "twenty-three", "twenty-four", "twenty-five", "twenty-six", "twenty-seven", "twenty-eight", "twenty-nine", "thirty", "thirty-one", "thirty-two", "thirty-three", "thirty-four", "thirty-five", "thirty-six", "thirty-seven", "thirty-eight", "thirty-nine", "forty", "forty-one", "forty-two", "forty-three", "forty-four", "forty-five", "forty-six", "forty-seven", "forty-eight", "forty-nine", "fifty", "fifty-one", "fifty-two", "fifty-three", "fifty-four", "fifty-five", "fifty-six", "fifty-seven", "fifty-eight", "fifty-nine", "sixty", "sixty-one", "sixty-two", "sixty-three", "sixty-four", "sixty-five", "sixty-six", "sixty-seven", "sixty-eight", "sixty-nine", "seventy", "seventy-one", "seventy-two", "seventy-three", "seventy-four", "seventy-five", "seventy-six", "seventy-seven", "seventy-eight", "seventy-nine", "eighty", "eighty-one", "eighty-two", "eighty-three", "eighty-four", "eighty-five", "eighty-six", "eighty-seven", "eighty-eight", "eighty-nine", "ninety", "ninety-one", "ninety-two", "ninety-three", "ninety-four", "ninety-five", "ninety-six", "ninety-seven", "ninety-eight", "ninety-nine", "one hundred" };
        static string[] N100 = new string[] { "zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen", "twenty", "twenty-one", "twenty-two", "twenty-three", "twenty-four", "twenty-five", "twenty-six", "twenty-seven", "twenty-eight", "twenty-nine", "thirty", "thirty-one", "thirty-two", "thirty-three", "thirty-four", "thirty-five", "thirty-six", "thirty-seven", "thirty-eight", "thirty-nine", "forty", "forty-one", "forty-two", "forty-three", "forty-four", "forty-five", "forty-six", "forty-seven", "forty-eight", "forty-nine", "fifty", "fifty-one", "fifty-two", "fifty-three", "fifty-four", "fifty-five", "fifty-six", "fifty-seven", "fifty-eight", "fifty-nine", "sixty", "sixty-one", "sixty-two", "sixty-three", "sixty-four", "sixty-five", "sixty-six", "sixty-seven", "sixty-eight", "sixty-nine", "seventy", "seventy-one", "seventy-two", "seventy-three", "seventy-four", "seventy-five", "seventy-six", "seventy-seven", "seventy-eight", "seventy-nine", "eighty", "eighty-one", "eighty-two", "eighty-three", "eighty-four", "eighty-five", "eighty-six", "eighty-seven", "eighty-eight", "eighty-nine", "ninety", "ninety-one", "ninety-two", "ninety-three", "ninety-four", "ninety-five", "ninety-six", "ninety-seven", "ninety-eight", "ninety-nine", "one-hundred" };
        private void recognizer_SpeechRecognized(object sender, SpeechRecognizedEventArgs e)
        {
            voiceCommand(e.Result.Text);
            //Console.WriteLine(e.Result.Text);
            //foreach (var al in e.Result.Alternates)E:\Assistant\Assistant\ListBoxNoScollBar.csE:\Assistant\Assistant\ListBoxNoScollBar.cs
            //{
            //    Console.WriteLine("   " + al.Text);
            //}
        }

        private void Recognizer_SpeechRecognitionRejected(object sender, SpeechRecognitionRejectedEventArgs e)
        {
            voiceCommand(e.Result.Text);

            //Console.WriteLine("?");
            //Console.WriteLine("Più vicino:" + e.Result.Text);
            //foreach (var al in e.Result.Alternates)
            //{
            //    Console.WriteLine("   " + al.Text);
            //}
        }

        string reversePart = "";
        void voiceCommand(string text)
        {
            if (!IsActive() || !checkBoxVR.Checked || text.Trim() == "")
                return;

            //sre.LoadGrammar(CreateGrammar(new string[] { "play", "pause", "restart", "fullscreen", "screenshot", "precisino", "mute" }));
            //sre.LoadGrammar(CreateGrammar(new string[] { "episode", "sigla", "song" }, new string[] { "start", "end", "reset" }));
            //sre.LoadGrammar(CreateGrammar(new string[] { "skip" }, NSmall, new string[] { "tick", "second", "minute" }));
            //sre.LoadGrammar(CreateGrammar(new string[] { "music" }, new string[] { "up", "down" }, NSmall));
            //sre.LoadGrammar(CreateGrammar(new string[] { "music" }, new string[] { "set" }, N25));
            //sre.LoadGrammar(CreateGrammar(new string[] { "music" }, new string[] { "mute" }));

            listBoxSpeech.Visible = true;
            if (text == "reverse")
            {
                listBoxSpeech.Items.Add(text + ": " + reversePart);
                text = reversePart;
            }
            else
                listBoxSpeech.Items.Add(text);

            if(listBoxSpeech.Items.Count > 5)
            {
                listBoxSpeech.Items.RemoveAt(0);
            }


            string[] parts = text.Split(' ');
            switch (parts[0])
            {
                case "play":
                    if (buttonStart.Text == "Play")
                    {
                        buttonStart_Click(this, null);
                        reversePart = "pause";
                    }

                    break;
                case "pause":
                    if (buttonStart.Text == "Pause")
                    {
                        buttonStart_Click(this, null);
                        reversePart = "play";
                    }

                    break;
                case "restart":
                    reversePart = "set " + trackBar1.Value + " millisecond";
                    buttonRestart_Click(this, null);
                    break;
                case "fullscreen":
                    reversePart = "fullscreen";
                    buttonFullScreen_Click(this, null);
                    break;
                case "screenshot":
                    lock (lockTHIS)
                    {
                        if (canSaveTime && buttonScreenshot.Enabled)
                        {
                            reversePart = "";
                            buttonScreenshot_Click(this, null);
                        }
                    }

                    break;
                case "precisino":
                    reversePart = "precisino";
                    checkBoxPrecisino.Checked = !checkBoxPrecisino.Checked;
                    break;
                case "mute":
                    reversePart = "music setINT " + trackBarVolume.Value;
                    changeVoulme(0);
                    break;
                case "previous":
                    if (buttonPrev.Enabled)
                    {
                        reversePart = "next";
                        buttonPrev_Click(this, null);
                    }

                    break;
                case "next":
                    if (buttonNext.Enabled)
                    {
                        reversePart = "previous";
                        buttonNext_Click(this, null);
                    }

                    break;






                #region episode sigla canzone
                case "episode":
                    lock (lockTHIS)
                    {
                        if (!canSaveTime)
                            return;
                    }

                    lock (lockPlayListIndex)
                    {
                        switch (parts[1])
                        {
                            case "start":
                                break;
                            case "end":
                                reversePart = "episode end " + playlist[PlaylistIndex].FineVideo;
                                playlist[PlaylistIndex].FineVideo = trackBar1.Value; break;
                            case "delede":
                                //playlist[PlaylistIndex].FineVideo = 0; break;
                            default:
                                break;
                        }
                    }
                    break;
                case "sigla":
                    lock (lockTHIS)
                    {
                        if (!canSaveTime)
                            return;
                    }
                    
                    lock (lockPlayListIndex)
                    {
                        switch (parts[1])
                        {
                            case "start":
                                reversePart = "sigla start " + playlist[PlaylistIndex].InizioSigla;
                                playlist[PlaylistIndex].InizioSigla = trackBar1.Value; break;
                            case "end":
                                reversePart = "sigla end " + playlist[PlaylistIndex].InizioSigla;
                                playlist[PlaylistIndex].FineSigla = trackBar1.Value; break;
                            case "delede":
                                //playlist[PlaylistIndex].InizioSigla = 0;
                                //playlist[PlaylistIndex].FineSigla = 0; 
                                break;
                            default:
                                break;
                        }
                    }
                    break;
                case "canzone":
                    lock (lockTHIS)
                    {
                        if (!canSaveTime)
                            return;
                    }

                    break;
                #endregion

                #region Set
                case "set":
                    reversePart = "set " + trackBar1.Value + " millisecond";
                    int value = Array.IndexOf(N100, parts[1]);
                    switch (parts[2])
                    {
                        case "frame":
                            SetFrame(value);
                            break;
                        case "millisecond":
                            SetMilliSeconds(int.Parse(parts[1])); break; // solo programma
                        case "second":
                            SetSec(value); break;
                        case "minute":
                            SetSec(value * 60); break;
                        default:
                            break;
                    }
                    break;
                #endregion

                #region Skip
                case "skip":
                    reversePart = "set " + trackBar1.Value + " millisecond";
                    value = Array.IndexOf(N100, parts[1]);
                    switch (parts[2])
                    {
                        case "frame":
                            AddFrame(value);
                            break;
                        case "second":
                            AddSec(value); break;
                        case "minute":
                            AddSec(value * 60); break;
                        default:
                            break;
                    }
                    break;
                #endregion

                #region Music
                case "music":
                    reversePart = "music setINT " + trackBarVolume.Value;

                    if (parts[1] == "mute")
                    {
                        changeVoulme(0);
                        break;
                    }

                    value = Array.IndexOf(N100, string.Join(" ", parts.Skip(2)));
                    switch (parts[1])
                    {
                        case "up":
                            changeVoulme(trackBarVolume.Value + value); break;
                        case "down":
                            changeVoulme(trackBarVolume.Value - value); break;
                        case "set":
                            changeVoulme(value); break;
                        case "setINT":
                            changeVoulme(int.Parse(parts[2])); break; //solo programma
                        default:
                            break;
                    }
                    break;
                #endregion
                default:
                    break;
            }

            new Thread(() =>
            {
                Thread.Sleep(2000);
                //if (!this.IsDisposed)
                try
                {
                    this.Invoke((Action)delegate { listBoxSpeech.Visible = false; });
                }
                catch { }
            }).Start();

        }
        #endregion

        #region Input Control
        private void Visione_KeyDown(object sender, KeyEventArgs e)
        {
            if (IsActive())
            {
                switch (e.KeyCode)
                {
                    case Keys.Space:
                        buttonStart_Click(this, null);
                        e.Handled = true;
                        break;
                    case Keys.Left:
                        buttonLess5_Click(this, null);
                        e.Handled = true;
                        break;
                    case Keys.Right:
                        buttonPlus5_Click(this, null);
                        e.Handled = true;
                        break;
                    case Keys.R:
                        buttonRestart_Click(this, null);
                        break;
                    case Keys.F:
                        buttonFullScreen_Click(this, null);
                        break;
                    case Keys.S:
                        lock (lockTHIS)
                        {
                            if (!canSaveTime)
                                return;
                        }

                        if (buttonScreenshot.Enabled)
                            buttonScreenshot_Click(this, null);
                        break;
                    case Keys.Z:
                        if (buttonStartSigla.Enabled)
                            buttonStartSigla_Click(this, null);
                        break;
                    case Keys.X:
                        if (buttonEndSigla.Enabled)
                            buttonEndSigla_Click(this, null);
                        break;
                    case Keys.C:
                        if (buttonEndEp.Enabled)
                            buttonEndEp_Click(this, null);
                        break;
                    case Keys.N:
                        if (buttonPrev.Enabled)
                            buttonPrev_Click(this, null);
                        break;
                    case Keys.M:
                        if (buttonNext.Enabled)
                            buttonNext_Click(this, null);
                        break;
                    default:
                        break;
                }
            }
        }
        #endregion

        #region Video Control
        //axVLCPlugin21.input.length è la lunghezza del video in millisecondi
        private void AxVLCPlugin21_MediaPlayerPlaying(object sender, EventArgs e)
        {
            int HBefore = this.Height - axVLCPlugin21.Height;
            int WBefore = this.Width - axVLCPlugin21.Width;
            this.Height = axVLCPlugin21.video.height + HBefore;
            this.Width = axVLCPlugin21.video.width + WBefore;
            //trackBar1.Maximum = (int)axVLCPlugin21.input.length;
            trackBar1.TickFrequency = 1000 * 60; //ogni min
        }

        void changeVideoView(int time)
        {
            lock (this)
            {
                if (trackBar1.Maximum < time)
                {
                    lock (lockPlayListIndex)
                    {
                        playlist[PlaylistIndex].tempoVisto = playlist[PlaylistIndex].Lenght;
                    }
                    return;
                }

                if (checkBoxPrecisino.Checked)
                    label1.Text = $"percentuale: {Math.Round(axVLCPlugin21.input.position * 100, 3)}, tempo: {TimeSpan.FromMilliseconds(axVLCPlugin21.input.time).ToString(@"h\:m\:s\.fff")}/{TimeSpan.FromMilliseconds(axVLCPlugin21.input.length).ToString(@"h\:m\:s\.fff")}";
                else
                    label1.Text = $"percentuale: {(int)(axVLCPlugin21.input.position * 100)}, tempo: {TimeSpan.FromMilliseconds(axVLCPlugin21.input.time).ToString(@"h\:m\:s")}/{TimeSpan.FromMilliseconds(axVLCPlugin21.input.length).ToString(@"h\:m\:s")}";

                lock (lockTHIS)
                {
                    if (canSaveTime && playlist[PlaylistIndex].tempoVisto < time)
                    {
                        playlist[PlaylistIndex].tempoVisto = time;
                    }
                }
            }
        }


        private void axVLCPlugin21_MediaPlayerTimeChanged(object sender, AxAXVLC.DVLCEvents_MediaPlayerTimeChangedEvent e)
        {
            changeVideoView(e.time);
            trackBar1.Value = e.time;
        }

        private void trackBar1_Scroll(object sender, EventArgs e)
        {
            changeVideoView(trackBar1.Value);
            axVLCPlugin21.input.time = trackBar1.Value;
        }


        private void axVLCPlugin21_MediaPlayerEndReached(object sender, EventArgs e)
        {
            //playlist.RemoveAt(0);
            NextVideo();
        }

        Object lockTHIS = new Object();
        bool canSaveTime = true;
        void startMedia()
        {
            //lock (waitThread)
            //{
            //    waitThread = new Thread(() => { });
            //}

            //this.Invoke((Action)delegate {

            axVLCPlugin21.playlist.playItem(PlaylistIndex);


            //axVLCPlugin21.playlist.items.clear();
            //axVLCPlugin21.playlist.add(playlist[PlaylistIndex].path);
            //axVLCPlugin21.playlist.play();


            //axVLCPlugin21.playlist.pause();


            if (playlist[PlaylistIndex].NoDATA)
            {
                playlist[PlaylistIndex].Width = axVLCPlugin21.video.width;
                playlist[PlaylistIndex].Height = axVLCPlugin21.video.height;
                playlist[PlaylistIndex].Lenght = axVLCPlugin21.input.length;
                playlist[PlaylistIndex].fps = axVLCPlugin21.input.fps;

                playlist[PlaylistIndex].NoDATA = false;
            }



            //this.Invoke((Action)delegate {
            //Console.WriteLine(axVLCPlugin21.input.time);

            axVLCPlugin21.input.time = playlist[PlaylistIndex].visto ? 0 : playlist[PlaylistIndex].tempoVisto;
            trackBar1.Maximum = (int)playlist[PlaylistIndex].Lenght;
            trackBar1.Value = playlist[PlaylistIndex].visto ? 0 : (int)playlist[PlaylistIndex].tempoVisto;
            buttonStart.Text = "Pause";
            //});

            playlist[PlaylistIndex].visulizzazioni.Add(DateTime.Now);

            //new Thread(() =>
            //{
            //    Thread.Sleep(1000);
            //    axVLCPlugin21.playlist.play();
            //}).Start();

            lock (lockTHIS)
            {
                canSaveTime = true;
                buttonScreenshot.Enabled = true;
                buttonStartSigla.Enabled = true;
                buttonEndSigla.Enabled = true;
                buttonEndEp.Enabled = true;
            }
        }


        Object lockwaitNP = new Object();
        int waitNP = -1;
        Thread waitThread = new Thread(() => { });
        void NextVideo()
        {
            lock (lockTHIS)
            {
                canSaveTime = false;
                buttonScreenshot.Enabled = false;
                buttonStartSigla.Enabled = false;
                buttonEndSigla.Enabled = false;
                buttonEndEp.Enabled = false;
            }

            lock (lockPlayListIndex)
            {
                if (PlaylistIndex == playlist.Count - 1)
                    return;

                PlaylistIndex++;
                this.Text = "Guardando: " + playlist[PlaylistIndex].titolo;

                if (PlaylistIndex == playlist.Count - 1)
                    buttonNext.Enabled = false;
                else if (PlaylistIndex == 1)
                    buttonPrev.Enabled = true;
            }

            lock (lockwaitNP)
            {
                waitNP = 1000;
            }

            setUpThread();
        }

        void PrevVideo()
        {
            lock (lockTHIS)
            {
                canSaveTime = false;
            }

            lock (lockPlayListIndex)
            {
                if (PlaylistIndex == 0)
                    return;

                PlaylistIndex--;
                this.Text = "Guardando: " + playlist[PlaylistIndex].titolo;

                if (PlaylistIndex == 0)
                    buttonPrev.Enabled = false;
                else if (PlaylistIndex == playlist.Count - 2)
                    buttonNext.Enabled = true;
            }

            lock (lockwaitNP)
            {
                waitNP = 1000;
            }

            setUpThread();
        }


        private void setUpThread()
        {
            waitThread = new Thread(() =>
            {
                while (true)
                {
                    lock (lockwaitNP)
                    {
                        if (waitNP <= 0)
                            break;

                        waitNP -= 10;
                    }

                    Thread.Sleep(10);
                }

                this.Invoke((Action)delegate {
                    //axVLCPlugin21.playlist.stop();
                    //axVLCPlugin21.playlist.items.clear();
                    //lock (lockPlayListIndex)
                    //{
                    //    axVLCPlugin21.playlist.add(playlist[PlaylistIndex].path);
                    //}
                    startMedia();
                });
            });
            waitThread.Start();
        }
        #endregion

        #region Video Buttons
        private void buttonStart_Click(object sender, EventArgs e)
        {
            if (buttonStart.Text == "Play")
            {
                axVLCPlugin21.playlist.play();
                buttonStart.Text = "Pause";
            }
            else
            {
                axVLCPlugin21.playlist.pause();
                buttonStart.Text = "Play";
            }
        }

        private void buttonRestart_Click(object sender, EventArgs e)
        {
            axVLCPlugin21.input.time = 0;
            trackBar1.Value = 0;
        }



        private void buttonNext_Click(object sender, EventArgs e)
        {
            NextVideo();
            /*
            buttonNext.Enabled = false;
            NextVideo();
            new Thread(() => {
                Thread.Sleep(50);
                this.Invoke((Action)delegate { buttonNext.Enabled = true; });
            }).Start();
            */
        }

        private void buttonPrev_Click(object sender, EventArgs e)
        {
            PrevVideo();
            /*
            buttonPrev.Enabled = false;
            new Thread(() => {
                Thread.Sleep(50);
                this.Invoke((Action)delegate { buttonPrev.Enabled = true; });
            }).Start();
            */
        }

        #endregion

        #region Video Info Buttons
        private void buttonStartSigla_Click(object sender, EventArgs e)
        {
            playlist[PlaylistIndex].InizioSigla = trackBar1.Value;
        }

        private void buttonEndSigla_Click(object sender, EventArgs e)
        {
            playlist[PlaylistIndex].FineSigla = trackBar1.Value;
        }

        private void buttonEndEp_Click(object sender, EventArgs e)
        {
            playlist[PlaylistIndex].FineVideo = trackBar1.Value;
        }

        #endregion

        #region FullScreen
        bool fullscreen = false;
        int prevH, prevW;
        private void buttonFullScreen_Click(object sender, EventArgs e)
        {
            if (buttonFullScreen.Text == "FullScreen")
            {
                //disabilito il form maximato per riuscire a nasconde anche la barra di sotto di windows
                WindowState = FormWindowState.Normal;

                FormBorderStyle = FormBorderStyle.None;
                WindowState = FormWindowState.Maximized;
                buttonFullScreen.Text = "TogleScreen";

                prevH = axVLCPlugin21.Height;
                prevW = axVLCPlugin21.Width;

                axVLCPlugin21.Location = new Point(0, 0);
                axVLCPlugin21.Height = this.Height;
                axVLCPlugin21.Width = this.Width;

                fullscreen = true;
                panel1.Visible = false;
                Cursor.Hide();
            }
            else
            {
                axVLCPlugin21.Location = new Point(12, 12);
                axVLCPlugin21.Size = new Size(prevW, prevH);

                FormBorderStyle = FormBorderStyle.Sizable;
                WindowState = FormWindowState.Normal;
                buttonFullScreen.Text = "FullScreen";

                fullscreen = false;
                panel1.Visible = true;
                Cursor.Show();
            }
        }

        private void Visione_MouseMove(object sender, MouseEventArgs e)
        {
            if (fullscreen)
            {
                Cursor.Show();
                Point panel = panel1.PointToScreen(Point.Empty);

                if (e.Y >= panel.Y && e.X >= panel.X && e.X <= panel.X + this.Width)//if (panel1.Bounds.Contains(e.Location))
                {
                    //Console.WriteLine("check");
                    if (!panel1.Visible)
                    {
                        panel1.Visible = true;
                    }
                }
                else if (panel1.Visible)
                {
                    panel1.Visible = false;
                }
            }
        }
        #endregion

        #region Volume
        private void trackBarVolume_Scroll(object sender, EventArgs e)
        {
            axVLCPlugin21.volume = trackBarVolume.Value;
            labelVolume.Text = trackBarVolume.Value + "%";
            volume = (byte)trackBarVolume.Value;
        }

        void changeVoulme(int perc)
        {
            if (perc < 0)
                perc = 0;
            else if (perc > trackBarVolume.Maximum)
                perc = trackBarVolume.Maximum;

            axVLCPlugin21.volume = perc;
            trackBarVolume.Value = perc;
            labelVolume.Text = perc + "%";
        }
        #endregion

        #region ScreenShot
        static FFMpegConverter conv = new FFMpegConverter();
        private void buttonScreenshot_Click(object sender, EventArgs e)
        {
            buttonScreenshot.Enabled = false;
            int index = trackBar1.Value;

            new Thread(() => {
                /*
                Process process = new Process();
                process.StartInfo.RedirectStandardOutput = true;
                process.StartInfo.RedirectStandardError = true;
                process.StartInfo.FileName = AppDomain.CurrentDomain.BaseDirectory + "ffmpeg.exe";

                process.StartInfo.Arguments = $"-ss {(int)(index / 1000)} -i \"{thisEpisode.path}\" -vframes 1 temp.png";
                process.StartInfo.UseShellExecute = false;
                process.StartInfo.CreateNoWindow = true;
                process.Start();

                process.WaitForExit();
                */

                conv.GetVideoThumbnail(playlist[PlaylistIndex].path, AppDomain.CurrentDomain.BaseDirectory + "temp.png", index / 1000);
                //thisEpisode.preview = Image.FromFile(AppDomain.CurrentDomain.BaseDirectory + "temp.png");
                using (var bmpTemp = new Bitmap(AppDomain.CurrentDomain.BaseDirectory + "temp.png"))
                {
                    playlist[PlaylistIndex].preview = new Bitmap(bmpTemp);
                }
                File.Delete(AppDomain.CurrentDomain.BaseDirectory + "temp.png");
                this.Invoke((Action)delegate {
                    buttonScreenshot.Enabled = true;
                });
            }).Start();
        }
        public static Image TakeCroppedScreenShot(Control control)
        {
            Bitmap bmp = new Bitmap(control.Width, control.Height);
            Graphics g = Graphics.FromImage(bmp);
            Rectangle r = control.RectangleToScreen(control.ClientRectangle);
            g.CopyFromScreen(r.Location, Point.Empty, control.Size);
            return (Image)bmp;
        }

        #endregion

        #region Time
        #region Frame
        private void buttonPuls_Click(object sender, EventArgs e)
        {
            AddFrame(1);
        }

        private void buttonLess_Click(object sender, EventArgs e)
        {
            AddFrame(-1);
        }

        void AddFrame(int frame)
        {
            //1sec / numero di frame in 1 sec = n sec x 1 frame
            int toAdd = (int)(1000 / axVLCPlugin21.input.fps) * frame;
            if (axVLCPlugin21.input.time == -1 && trackBar1.Value + toAdd > trackBar1.Maximum)
                return;

            axVLCPlugin21.input.time += toAdd;
            trackBar1.Value += toAdd;
        }

        void SetFrame(int frame)
        {
            //1sec / numero di frame in 1 sec = n sec x 1 frame
            int toAdd = (int)(1000 / axVLCPlugin21.input.fps) * frame;
            if (axVLCPlugin21.input.time == -1)
                return;

            axVLCPlugin21.input.time += toAdd;
            trackBar1.Value += toAdd;
        }
        void RemoveFrame(int frame)
        {
            //1sec / numero di frame in 1 sec = n sec x 1 frame
            int toAdd = (int)(1000 / axVLCPlugin21.input.fps) * frame;
            if (axVLCPlugin21.input.time == -1 || trackBar1.Value - toAdd < 0)
                return;

            axVLCPlugin21.input.time -= toAdd;
            trackBar1.Value -= toAdd;
        }

        #endregion

        #region MilliSeconds
        void AddMilliSeconds(int milli)
        {
            if (axVLCPlugin21.input.time == -1 || trackBar1.Value + milli > trackBar1.Maximum)
                return;

            axVLCPlugin21.input.time += milli;
            trackBar1.Value += milli;
        }

        void SetMilliSeconds(int milli)
        {
            if (axVLCPlugin21.input.time == -1)
                return;

            axVLCPlugin21.input.time = milli;
            trackBar1.Value = milli;
        }
        void RemoveMilliSeconds(int milli)
        {
            if (axVLCPlugin21.input.time == -1 || trackBar1.Value < milli)
                return;

            axVLCPlugin21.input.time -= milli;
            trackBar1.Value -= milli;
        }
        #endregion

        #region Second
        private void buttonPlus5_Click(object sender, EventArgs e)
        {
            AddSec(5);
        }

        private void buttonLess5_Click(object sender, EventArgs e)
        {
            RemoveSec(5);
        }

        void AddSec(int second)
        {
            AddMilliSeconds(second * 1000);
        }

        void SetSec(int second)
        {
            SetMilliSeconds(second * 1000);
        }

        void RemoveSec(int second)
        {
            RemoveMilliSeconds(second * 1000);
        }
        #endregion
        #endregion

        #region Active
        public bool IsActive()
        {
            try
            {
                IntPtr activeHandle = GetForegroundWindow();
                return (activeHandle == this.Handle);
            }
            catch
            {
                return false;
            }
        }

        private void checkBoxPrecisino_CheckedChanged(object sender, EventArgs e)
        {
            Precisino = checkBoxPrecisino.Checked;
        }

        private void checkBoxVR_CheckedChanged(object sender, EventArgs e)
        {
            VoiceRecognition = checkBoxVR.Checked;
        }

        private void checkBoxNextEpSave_CheckedChanged(object sender, EventArgs e)
        {
            saveEpEndNext = checkBoxNextEpSave.Checked;
        }

        [DllImport("user32.dll", CharSet = CharSet.Auto, ExactSpelling = true)]
        private static extern IntPtr GetForegroundWindow();

        #endregion
    }

}