﻿namespace SerieControl
{
    partial class Form1
    {
        /// <summary>
        /// Variabile di progettazione necessaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Pulire le risorse in uso.
        /// </summary>
        /// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Codice generato da Progettazione Windows Form

        /// <summary>
        /// Metodo necessario per il supporto della finestra di progettazione. Non modificare
        /// il contenuto del metodo con l'editor di codice.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.contextMenuStripSerie = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.addToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addEmptyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.removeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.vistoToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.vistoFalseToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.importaFileInfoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.getInfoFromInternetToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.getInfoFromNameToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sortToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sortWithSortDataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.removeDuplicateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenuStripSeason = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItemSeasonAdd = new System.Windows.Forms.ToolStripMenuItem();
            this.addEmptyToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemSeasonRemove = new System.Windows.Forms.ToolStripMenuItem();
            this.vistoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.vistoFalseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenuStripEp = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.addEmptyToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem5 = new System.Windows.Forms.ToolStripMenuItem();
            this.setStartForSerieToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.setEndForSerieToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.textBoxTitle = new System.Windows.Forms.TextBox();
            this.textBoxInfo = new System.Windows.Forms.TextBox();
            this.textBoxDescription = new System.Windows.Forms.TextBox();
            this.buttonEdit = new System.Windows.Forms.Button();
            this.listBoxSerie = new System.Windows.Forms.ListBox();
            this.listBoxSeason = new System.Windows.Forms.ListBox();
            this.listBoxEp = new System.Windows.Forms.ListBox();
            this.textBoxPath = new System.Windows.Forms.TextBox();
            this.buttonSelectpath = new System.Windows.Forms.Button();
            this.listBoxVisulizzazioni = new System.Windows.Forms.ListBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.esportaFilesSerieToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.updateListsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.settingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.infoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.timerSaveStuff = new System.Windows.Forms.Timer(this.components);
            this.buttonSave = new System.Windows.Forms.Button();
            this.textBoxSearch = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.contextMenuStripSerie.SuspendLayout();
            this.contextMenuStripSeason.SuspendLayout();
            this.contextMenuStripEp.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // contextMenuStripSerie
            // 
            this.contextMenuStripSerie.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addToolStripMenuItem,
            this.removeToolStripMenuItem,
            this.vistoToolStripMenuItem1,
            this.importaFileInfoToolStripMenuItem,
            this.getInfoFromInternetToolStripMenuItem,
            this.getInfoFromNameToolStripMenuItem,
            this.sortToolStripMenuItem,
            this.removeDuplicateToolStripMenuItem});
            this.contextMenuStripSerie.Name = "contextMenuStripSerie";
            this.contextMenuStripSerie.Size = new System.Drawing.Size(190, 180);
            // 
            // addToolStripMenuItem
            // 
            this.addToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addEmptyToolStripMenuItem});
            this.addToolStripMenuItem.Name = "addToolStripMenuItem";
            this.addToolStripMenuItem.Size = new System.Drawing.Size(189, 22);
            this.addToolStripMenuItem.Text = "Add";
            this.addToolStripMenuItem.Click += new System.EventHandler(this.addToolStripMenuItem_Click);
            // 
            // addEmptyToolStripMenuItem
            // 
            this.addEmptyToolStripMenuItem.Name = "addEmptyToolStripMenuItem";
            this.addEmptyToolStripMenuItem.Size = new System.Drawing.Size(133, 22);
            this.addEmptyToolStripMenuItem.Text = "Add Empty";
            this.addEmptyToolStripMenuItem.Click += new System.EventHandler(this.addEmptyToolStripMenuItem_Click);
            // 
            // removeToolStripMenuItem
            // 
            this.removeToolStripMenuItem.Name = "removeToolStripMenuItem";
            this.removeToolStripMenuItem.Size = new System.Drawing.Size(189, 22);
            this.removeToolStripMenuItem.Text = "Remove";
            this.removeToolStripMenuItem.Click += new System.EventHandler(this.removeToolStripMenuItem_Click);
            // 
            // vistoToolStripMenuItem1
            // 
            this.vistoToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.vistoFalseToolStripMenuItem1});
            this.vistoToolStripMenuItem1.Name = "vistoToolStripMenuItem1";
            this.vistoToolStripMenuItem1.Size = new System.Drawing.Size(189, 22);
            this.vistoToolStripMenuItem1.Text = "Visto";
            this.vistoToolStripMenuItem1.Click += new System.EventHandler(this.vistoToolStripMenuItem1_Click);
            // 
            // vistoFalseToolStripMenuItem1
            // 
            this.vistoFalseToolStripMenuItem1.Name = "vistoFalseToolStripMenuItem1";
            this.vistoFalseToolStripMenuItem1.Size = new System.Drawing.Size(130, 22);
            this.vistoFalseToolStripMenuItem1.Text = "Visto: false";
            this.vistoFalseToolStripMenuItem1.Click += new System.EventHandler(this.vistoFalseToolStripMenuItem1_Click);
            // 
            // importaFileInfoToolStripMenuItem
            // 
            this.importaFileInfoToolStripMenuItem.Name = "importaFileInfoToolStripMenuItem";
            this.importaFileInfoToolStripMenuItem.Size = new System.Drawing.Size(189, 22);
            this.importaFileInfoToolStripMenuItem.Text = "Importa file info";
            this.importaFileInfoToolStripMenuItem.Click += new System.EventHandler(this.importaFileInfoToolStripMenuItem_Click);
            // 
            // getInfoFromInternetToolStripMenuItem
            // 
            this.getInfoFromInternetToolStripMenuItem.Name = "getInfoFromInternetToolStripMenuItem";
            this.getInfoFromInternetToolStripMenuItem.Size = new System.Drawing.Size(189, 22);
            this.getInfoFromInternetToolStripMenuItem.Text = "Get Info from internet";
            this.getInfoFromInternetToolStripMenuItem.Click += new System.EventHandler(this.getInfoFromInternetToolStripMenuItem_Click);
            // 
            // getInfoFromNameToolStripMenuItem
            // 
            this.getInfoFromNameToolStripMenuItem.Name = "getInfoFromNameToolStripMenuItem";
            this.getInfoFromNameToolStripMenuItem.Size = new System.Drawing.Size(189, 22);
            this.getInfoFromNameToolStripMenuItem.Text = "Get Info from name";
            this.getInfoFromNameToolStripMenuItem.Click += new System.EventHandler(this.getInfoFromNameToolStripMenuItem_Click);
            // 
            // sortToolStripMenuItem
            // 
            this.sortToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.sortWithSortDataToolStripMenuItem});
            this.sortToolStripMenuItem.Name = "sortToolStripMenuItem";
            this.sortToolStripMenuItem.Size = new System.Drawing.Size(189, 22);
            this.sortToolStripMenuItem.Text = "Sort";
            this.sortToolStripMenuItem.Click += new System.EventHandler(this.sortToolStripMenuItem_Click);
            // 
            // sortWithSortDataToolStripMenuItem
            // 
            this.sortWithSortDataToolStripMenuItem.Name = "sortWithSortDataToolStripMenuItem";
            this.sortWithSortDataToolStripMenuItem.Size = new System.Drawing.Size(170, 22);
            this.sortWithSortDataToolStripMenuItem.Text = "Sort with sort data";
            this.sortWithSortDataToolStripMenuItem.Click += new System.EventHandler(this.sortWithSortDataToolStripMenuItem_Click);
            // 
            // removeDuplicateToolStripMenuItem
            // 
            this.removeDuplicateToolStripMenuItem.Name = "removeDuplicateToolStripMenuItem";
            this.removeDuplicateToolStripMenuItem.Size = new System.Drawing.Size(189, 22);
            this.removeDuplicateToolStripMenuItem.Text = "Remove Duplicate";
            this.removeDuplicateToolStripMenuItem.Click += new System.EventHandler(this.removeDuplicateToolStripMenuItem_Click);
            // 
            // contextMenuStripSeason
            // 
            this.contextMenuStripSeason.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItemSeasonAdd,
            this.toolStripMenuItemSeasonRemove,
            this.vistoToolStripMenuItem});
            this.contextMenuStripSeason.Name = "contextMenuStripSerie";
            this.contextMenuStripSeason.Size = new System.Drawing.Size(118, 70);
            // 
            // toolStripMenuItemSeasonAdd
            // 
            this.toolStripMenuItemSeasonAdd.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addEmptyToolStripMenuItem1});
            this.toolStripMenuItemSeasonAdd.Name = "toolStripMenuItemSeasonAdd";
            this.toolStripMenuItemSeasonAdd.Size = new System.Drawing.Size(117, 22);
            this.toolStripMenuItemSeasonAdd.Text = "Add";
            this.toolStripMenuItemSeasonAdd.Click += new System.EventHandler(this.toolStripMenuItemSeasonAdd_Click);
            // 
            // addEmptyToolStripMenuItem1
            // 
            this.addEmptyToolStripMenuItem1.Name = "addEmptyToolStripMenuItem1";
            this.addEmptyToolStripMenuItem1.Size = new System.Drawing.Size(133, 22);
            this.addEmptyToolStripMenuItem1.Text = "Add Empty";
            this.addEmptyToolStripMenuItem1.Click += new System.EventHandler(this.addEmptyToolStripMenuItem1_Click);
            // 
            // toolStripMenuItemSeasonRemove
            // 
            this.toolStripMenuItemSeasonRemove.Name = "toolStripMenuItemSeasonRemove";
            this.toolStripMenuItemSeasonRemove.Size = new System.Drawing.Size(117, 22);
            this.toolStripMenuItemSeasonRemove.Text = "Remove";
            this.toolStripMenuItemSeasonRemove.Click += new System.EventHandler(this.toolStripMenuItemSeasonRemove_Click);
            // 
            // vistoToolStripMenuItem
            // 
            this.vistoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.vistoFalseToolStripMenuItem});
            this.vistoToolStripMenuItem.Name = "vistoToolStripMenuItem";
            this.vistoToolStripMenuItem.Size = new System.Drawing.Size(117, 22);
            this.vistoToolStripMenuItem.Text = "Visto";
            this.vistoToolStripMenuItem.Click += new System.EventHandler(this.vistoToolStripMenuItem_Click);
            // 
            // vistoFalseToolStripMenuItem
            // 
            this.vistoFalseToolStripMenuItem.Name = "vistoFalseToolStripMenuItem";
            this.vistoFalseToolStripMenuItem.Size = new System.Drawing.Size(130, 22);
            this.vistoFalseToolStripMenuItem.Text = "Visto: false";
            this.vistoFalseToolStripMenuItem.Click += new System.EventHandler(this.vistoFalseToolStripMenuItem_Click);
            // 
            // contextMenuStripEp
            // 
            this.contextMenuStripEp.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1,
            this.toolStripMenuItem3,
            this.toolStripMenuItem4,
            this.setStartForSerieToolStripMenuItem,
            this.setEndForSerieToolStripMenuItem});
            this.contextMenuStripEp.Name = "contextMenuStripSerie";
            this.contextMenuStripEp.Size = new System.Drawing.Size(163, 114);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addEmptyToolStripMenuItem2});
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(162, 22);
            this.toolStripMenuItem1.Text = "Add";
            this.toolStripMenuItem1.Click += new System.EventHandler(this.toolStripMenuItem1_Click);
            // 
            // addEmptyToolStripMenuItem2
            // 
            this.addEmptyToolStripMenuItem2.Name = "addEmptyToolStripMenuItem2";
            this.addEmptyToolStripMenuItem2.Size = new System.Drawing.Size(133, 22);
            this.addEmptyToolStripMenuItem2.Text = "Add Empty";
            this.addEmptyToolStripMenuItem2.Click += new System.EventHandler(this.addEmptyToolStripMenuItem2_Click);
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(162, 22);
            this.toolStripMenuItem3.Text = "Remove";
            this.toolStripMenuItem3.Click += new System.EventHandler(this.toolStripMenuItem3_Click);
            // 
            // toolStripMenuItem4
            // 
            this.toolStripMenuItem4.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem5});
            this.toolStripMenuItem4.Name = "toolStripMenuItem4";
            this.toolStripMenuItem4.Size = new System.Drawing.Size(162, 22);
            this.toolStripMenuItem4.Text = "Visto";
            this.toolStripMenuItem4.Click += new System.EventHandler(this.toolStripMenuItem4_Click);
            // 
            // toolStripMenuItem5
            // 
            this.toolStripMenuItem5.Name = "toolStripMenuItem5";
            this.toolStripMenuItem5.Size = new System.Drawing.Size(130, 22);
            this.toolStripMenuItem5.Text = "Visto: false";
            this.toolStripMenuItem5.Click += new System.EventHandler(this.toolStripMenuItem5_Click);
            // 
            // setStartForSerieToolStripMenuItem
            // 
            this.setStartForSerieToolStripMenuItem.Name = "setStartForSerieToolStripMenuItem";
            this.setStartForSerieToolStripMenuItem.Size = new System.Drawing.Size(162, 22);
            this.setStartForSerieToolStripMenuItem.Text = "Set sigla for serie";
            this.setStartForSerieToolStripMenuItem.Click += new System.EventHandler(this.setStartForSerieToolStripMenuItem_Click);
            // 
            // setEndForSerieToolStripMenuItem
            // 
            this.setEndForSerieToolStripMenuItem.Name = "setEndForSerieToolStripMenuItem";
            this.setEndForSerieToolStripMenuItem.Size = new System.Drawing.Size(162, 22);
            this.setEndForSerieToolStripMenuItem.Text = "Set end for serie";
            this.setEndForSerieToolStripMenuItem.Click += new System.EventHandler(this.setEndForSerieToolStripMenuItem_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox1.Location = new System.Drawing.Point(669, 25);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(407, 160);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 2;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // textBoxTitle
            // 
            this.textBoxTitle.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxTitle.Location = new System.Drawing.Point(669, 191);
            this.textBoxTitle.Name = "textBoxTitle";
            this.textBoxTitle.ReadOnly = true;
            this.textBoxTitle.Size = new System.Drawing.Size(407, 20);
            this.textBoxTitle.TabIndex = 3;
            this.textBoxTitle.Leave += new System.EventHandler(this.textBoxTitle_Leave);
            // 
            // textBoxInfo
            // 
            this.textBoxInfo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxInfo.Location = new System.Drawing.Point(669, 243);
            this.textBoxInfo.Multiline = true;
            this.textBoxInfo.Name = "textBoxInfo";
            this.textBoxInfo.ReadOnly = true;
            this.textBoxInfo.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBoxInfo.Size = new System.Drawing.Size(250, 108);
            this.textBoxInfo.TabIndex = 4;
            // 
            // textBoxDescription
            // 
            this.textBoxDescription.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxDescription.Location = new System.Drawing.Point(669, 357);
            this.textBoxDescription.Multiline = true;
            this.textBoxDescription.Name = "textBoxDescription";
            this.textBoxDescription.ReadOnly = true;
            this.textBoxDescription.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBoxDescription.Size = new System.Drawing.Size(407, 88);
            this.textBoxDescription.TabIndex = 5;
            this.textBoxDescription.Leave += new System.EventHandler(this.textBoxDescription_Leave);
            // 
            // buttonEdit
            // 
            this.buttonEdit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonEdit.Location = new System.Drawing.Point(1001, 451);
            this.buttonEdit.Name = "buttonEdit";
            this.buttonEdit.Size = new System.Drawing.Size(75, 23);
            this.buttonEdit.TabIndex = 10;
            this.buttonEdit.Text = "Edit";
            this.buttonEdit.UseVisualStyleBackColor = true;
            this.buttonEdit.Click += new System.EventHandler(this.buttonEdit_Click);
            // 
            // listBoxSerie
            // 
            this.listBoxSerie.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.listBoxSerie.ContextMenuStrip = this.contextMenuStripSerie;
            this.listBoxSerie.FormattingEnabled = true;
            this.listBoxSerie.HorizontalScrollbar = true;
            this.listBoxSerie.Location = new System.Drawing.Point(9, 51);
            this.listBoxSerie.Name = "listBoxSerie";
            this.listBoxSerie.Size = new System.Drawing.Size(153, 394);
            this.listBoxSerie.TabIndex = 17;
            this.listBoxSerie.SelectedIndexChanged += new System.EventHandler(this.listBoxSerie_SelectedIndexChanged);
            this.listBoxSerie.DoubleClick += new System.EventHandler(this.listBoxSerie_DoubleClick);
            this.listBoxSerie.KeyDown += new System.Windows.Forms.KeyEventHandler(this.listBoxSerie_KeyDown);
            // 
            // listBoxSeason
            // 
            this.listBoxSeason.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.listBoxSeason.ContextMenuStrip = this.contextMenuStripSeason;
            this.listBoxSeason.FormattingEnabled = true;
            this.listBoxSeason.HorizontalScrollbar = true;
            this.listBoxSeason.Location = new System.Drawing.Point(168, 26);
            this.listBoxSeason.Name = "listBoxSeason";
            this.listBoxSeason.Size = new System.Drawing.Size(153, 420);
            this.listBoxSeason.TabIndex = 18;
            this.listBoxSeason.SelectedIndexChanged += new System.EventHandler(this.listBoxSeason_SelectedIndexChanged);
            this.listBoxSeason.KeyDown += new System.Windows.Forms.KeyEventHandler(this.listBoxSeason_KeyDown);
            this.listBoxSeason.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.listBoxSeason_MouseDoubleClick);
            // 
            // listBoxEp
            // 
            this.listBoxEp.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.listBoxEp.ContextMenuStrip = this.contextMenuStripEp;
            this.listBoxEp.ForeColor = System.Drawing.SystemColors.WindowText;
            this.listBoxEp.FormattingEnabled = true;
            this.listBoxEp.HorizontalScrollbar = true;
            this.listBoxEp.Location = new System.Drawing.Point(327, 25);
            this.listBoxEp.Name = "listBoxEp";
            this.listBoxEp.Size = new System.Drawing.Size(336, 420);
            this.listBoxEp.TabIndex = 19;
            this.listBoxEp.SelectedIndexChanged += new System.EventHandler(this.listBoxEp_SelectedIndexChanged);
            this.listBoxEp.DoubleClick += new System.EventHandler(this.listBoxEp_DoubleClick);
            this.listBoxEp.KeyDown += new System.Windows.Forms.KeyEventHandler(this.listBoxEp_KeyDown);
            // 
            // textBoxPath
            // 
            this.textBoxPath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxPath.Location = new System.Drawing.Point(669, 217);
            this.textBoxPath.Name = "textBoxPath";
            this.textBoxPath.ReadOnly = true;
            this.textBoxPath.Size = new System.Drawing.Size(365, 20);
            this.textBoxPath.TabIndex = 20;
            this.textBoxPath.Leave += new System.EventHandler(this.textBoxPath_Leave);
            // 
            // buttonSelectpath
            // 
            this.buttonSelectpath.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonSelectpath.Enabled = false;
            this.buttonSelectpath.Location = new System.Drawing.Point(1040, 215);
            this.buttonSelectpath.Name = "buttonSelectpath";
            this.buttonSelectpath.Size = new System.Drawing.Size(36, 23);
            this.buttonSelectpath.TabIndex = 21;
            this.buttonSelectpath.Text = "...";
            this.buttonSelectpath.UseVisualStyleBackColor = true;
            this.buttonSelectpath.Click += new System.EventHandler(this.buttonSelectpath_Click);
            // 
            // listBoxVisulizzazioni
            // 
            this.listBoxVisulizzazioni.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.listBoxVisulizzazioni.FormattingEnabled = true;
            this.listBoxVisulizzazioni.Location = new System.Drawing.Point(925, 243);
            this.listBoxVisulizzazioni.Name = "listBoxVisulizzazioni";
            this.listBoxVisulizzazioni.Size = new System.Drawing.Size(151, 108);
            this.listBoxVisulizzazioni.TabIndex = 22;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1088, 24);
            this.menuStrip1.TabIndex = 23;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.esportaFilesSerieToolStripMenuItem,
            this.updateListsToolStripMenuItem,
            this.settingsToolStripMenuItem,
            this.infoToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // esportaFilesSerieToolStripMenuItem
            // 
            this.esportaFilesSerieToolStripMenuItem.Name = "esportaFilesSerieToolStripMenuItem";
            this.esportaFilesSerieToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.esportaFilesSerieToolStripMenuItem.Text = "Esporta files serie";
            this.esportaFilesSerieToolStripMenuItem.Click += new System.EventHandler(this.esportaFilesSerieToolStripMenuItem_Click);
            // 
            // updateListsToolStripMenuItem
            // 
            this.updateListsToolStripMenuItem.Name = "updateListsToolStripMenuItem";
            this.updateListsToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.updateListsToolStripMenuItem.Text = "Update Lists";
            this.updateListsToolStripMenuItem.Click += new System.EventHandler(this.updateListsToolStripMenuItem_Click);
            // 
            // settingsToolStripMenuItem
            // 
            this.settingsToolStripMenuItem.Name = "settingsToolStripMenuItem";
            this.settingsToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.settingsToolStripMenuItem.Text = "Settings";
            this.settingsToolStripMenuItem.Click += new System.EventHandler(this.settingsToolStripMenuItem_Click);
            // 
            // infoToolStripMenuItem
            // 
            this.infoToolStripMenuItem.Name = "infoToolStripMenuItem";
            this.infoToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.infoToolStripMenuItem.Text = "Info";
            this.infoToolStripMenuItem.Click += new System.EventHandler(this.infoToolStripMenuItem_Click);
            // 
            // timerSaveStuff
            // 
            this.timerSaveStuff.Interval = 1800000;
            this.timerSaveStuff.Tick += new System.EventHandler(this.timerSaveStuff_Tick);
            // 
            // buttonSave
            // 
            this.buttonSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonSave.Location = new System.Drawing.Point(1001, 480);
            this.buttonSave.Name = "buttonSave";
            this.buttonSave.Size = new System.Drawing.Size(75, 23);
            this.buttonSave.TabIndex = 24;
            this.buttonSave.Text = "Save";
            this.buttonSave.UseVisualStyleBackColor = true;
            this.buttonSave.Click += new System.EventHandler(this.buttonSave_Click);
            // 
            // textBoxSearch
            // 
            this.textBoxSearch.Location = new System.Drawing.Point(9, 26);
            this.textBoxSearch.Name = "textBoxSearch";
            this.textBoxSearch.Size = new System.Drawing.Size(152, 20);
            this.textBoxSearch.TabIndex = 25;
            this.textBoxSearch.TextChanged += new System.EventHandler(this.textBoxSearch_TextChanged);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(365, 484);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 27;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(500, 483);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 28;
            this.button2.Text = "button2";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(601, 484);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 30;
            this.button3.Text = "button3";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1088, 515);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.textBoxSearch);
            this.Controls.Add(this.buttonSave);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.listBoxVisulizzazioni);
            this.Controls.Add(this.buttonSelectpath);
            this.Controls.Add(this.textBoxPath);
            this.Controls.Add(this.listBoxEp);
            this.Controls.Add(this.listBoxSeason);
            this.Controls.Add(this.listBoxSerie);
            this.Controls.Add(this.buttonEdit);
            this.Controls.Add(this.textBoxDescription);
            this.Controls.Add(this.textBoxInfo);
            this.Controls.Add(this.textBoxTitle);
            this.Controls.Add(this.pictureBox1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Form1";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.contextMenuStripSerie.ResumeLayout(false);
            this.contextMenuStripSeason.ResumeLayout(false);
            this.contextMenuStripEp.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ContextMenuStrip contextMenuStripSerie;
        private System.Windows.Forms.ToolStripMenuItem addToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem removeToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip contextMenuStripSeason;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemSeasonAdd;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemSeasonRemove;
        private System.Windows.Forms.ToolStripMenuItem addEmptyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addEmptyToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem vistoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem vistoFalseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem vistoToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem vistoFalseToolStripMenuItem1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStripEp;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem5;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TextBox textBoxTitle;
        private System.Windows.Forms.TextBox textBoxInfo;
        private System.Windows.Forms.TextBox textBoxDescription;
        private System.Windows.Forms.Button buttonEdit;
        private System.Windows.Forms.ListBox listBoxSerie;
        private System.Windows.Forms.ListBox listBoxSeason;
        private System.Windows.Forms.ListBox listBoxEp;
        private System.Windows.Forms.TextBox textBoxPath;
        private System.Windows.Forms.Button buttonSelectpath;
        private System.Windows.Forms.ListBox listBoxVisulizzazioni;
        private System.Windows.Forms.ToolStripMenuItem importaFileInfoToolStripMenuItem;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem esportaFilesSerieToolStripMenuItem;
        private System.Windows.Forms.Timer timerSaveStuff;
        private System.Windows.Forms.Button buttonSave;
        private System.Windows.Forms.ToolStripMenuItem infoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem getInfoFromInternetToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem setStartForSerieToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem setEndForSerieToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sortToolStripMenuItem;
        private System.Windows.Forms.TextBox textBoxSearch;
        private System.Windows.Forms.ToolStripMenuItem removeDuplicateToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sortWithSortDataToolStripMenuItem;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ToolStripMenuItem getInfoFromNameToolStripMenuItem;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.ToolStripMenuItem updateListsToolStripMenuItem;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.ToolStripMenuItem settingsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addEmptyToolStripMenuItem2;
    }
}

