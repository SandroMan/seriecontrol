﻿namespace TestGUI
{
    partial class PanelsControlForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelsControl1 = new TestGUI.PanelsControl();
            this.SuspendLayout();
            // 
            // panelsControl1
            // 
            this.panelsControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelsControl1.AutoScroll = true;
            this.panelsControl1.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.panelsControl1.boxes = null;
            this.panelsControl1.Location = new System.Drawing.Point(0, 0);
            this.panelsControl1.Name = "panelsControl1";
            this.panelsControl1.Size = new System.Drawing.Size(1033, 565);
            this.panelsControl1.TabIndex = 0;
            // 
            // PanelsControlForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1030, 559);
            this.Controls.Add(this.panelsControl1);
            this.Name = "PanelsControlForm";
            this.Text = "PanelsControlForm";
            this.ResumeLayout(false);

        }

        #endregion

        private PanelsControl panelsControl1;
    }
}