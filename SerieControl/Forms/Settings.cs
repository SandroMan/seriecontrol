﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Speech.Recognition;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SerieControl
{

    public partial class Settings : Form
    {
        public static CultureInfo cultureVR { get; set; }

        public static bool saveExit { get; set; } = false;
        public static bool autoSaveTimer { get; set; } = false;
        public static bool refreshFocus { get; set; } = false;
        public static bool noEpisodebutAdd { get; set; } = true;


        public static Timer saveStuffTimer { get; set; }
        public Settings()
        {
            InitializeComponent();
            checkBoxSaveExit.Checked = saveExit;
            checkBoxAutoSaveTimer.Checked = autoSaveTimer;
            checkBoxRefreshFocus.Checked = refreshFocus;
            checkBoxNoEpisode.Checked = noEpisodebutAdd;
        }

        #region CheckBox
        private void checkBoxAutoSaveTimer_CheckedChanged(object sender, EventArgs e)
        {
            autoSaveTimer = checkBoxAutoSaveTimer.Checked;
            if (autoSaveTimer)
                saveStuffTimer.Start();
            else
                saveStuffTimer.Stop();
        }
        private void checkBoxSaveExit_CheckedChanged(object sender, EventArgs e)
        {
            saveExit = checkBoxSaveExit.Checked;
        }
        private void checkBoxRefreshFocus_CheckedChanged(object sender, EventArgs e)
        {
            refreshFocus = checkBoxRefreshFocus.Checked;
        }
        private void checkBoxNoEpisode_CheckedChanged(object sender, EventArgs e)
        {
            noEpisodebutAdd = checkBoxNoEpisode.Checked;
        }
        #endregion

        #region Cultures
        List<CultureInfo> cultures = new List<CultureInfo>();
        private void buttonRefresh_Click(object sender, EventArgs e)
        {
            cultures.Clear();
            comboBoxCulture.Items.Clear();
            foreach (RecognizerInfo ri in SpeechRecognitionEngine.InstalledRecognizers())
            {
                cultures.Add(ri.Culture);
                comboBoxCulture.Items.Add(ri.Culture.DisplayName + $"[{ri.Culture.ToString()}]");

                if(cultureVR != null && ri.Culture.Name == cultureVR.Name)
                {
                    comboBoxCulture.SelectedIndex = comboBoxCulture.Items.Count - 1;
                }
            }

            if (cultures.Count == 0)
            {
                if(MessageBox.Show("Ho notato che non hai nessuna lingua per il riconoscimento vocale installata, vuoi sapere come farlo ?", "Tutorial ?", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    Process.Start("ms-settings:regionlanguage");
                    MessageBox.Show("Tutorial in aggiornamento\n" +
                        //"Entra nelle impostazioni di windows (non pannello di controllo)\n" +
                        //"Clicca su \"Data/ora e lingua\" > \"Area geografica e lingua\"\n" +
                        "Clicca il pulsante \"Aggiungi una lingua\" e ora clicca su una lingua che ha il riconoscimento vocale come icona a lato\n" +
                        "Clicca su Avanti e Installa\n" +
                        "Aspetta la fine dell'installazione e poi prova a refreshare questa lista con il pulsanto con su scritto \"Ref.\"\n\n" +
                        "!!ATTENZIONE!! Purtroppo non sempre tutte le lingue con il riconoscimento vocale come icona funzionano, se non funziona la tua lingua nativa installare l'inglese USA");
                }
            }
        }

        private void comboBoxCulture_SelectedIndexChanged(object sender, EventArgs e)
        {
            cultureVR = cultures[comboBoxCulture.SelectedIndex];
        }

        private void Settings_Load(object sender, EventArgs e)
        {
            buttonRefresh_Click(null, null);
        }
        #endregion

    }
}
