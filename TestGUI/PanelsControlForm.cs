﻿using SerieControl;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TestGUI
{
    public partial class PanelsControlForm : Form
    {
        public PanelsControlForm()
        {
            InitializeComponent();

            List<Box> ll = new List<Box>();

            for (int i = 0; i < 15; i++)
            {
                Box box = new Box() { serie = new Serie("Name Serie" + i), Cover = TestGUI.Properties.Resources.cover };
                box.ButtonInfoClicked += Box_ButtonInfoClicked;
                box.ButtonPlayClicked += Box_ButtonPlayClicked;
                ll.Add(box);
            }
            panelsControl1.boxes = ll;
        }

        private void Box_ButtonInfoClicked(object sender, EventArgs e)
        {
            //MessageBox.Show(((Box)sender).name + "  button info clicked");
        }

        private void Box_ButtonPlayClicked(object sender, EventArgs e)
        {
            MessageBox.Show(((Box)sender).Name + "  button play clicked");
        }
    }
}
