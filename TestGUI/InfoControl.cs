﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing.Drawing2D;
using SerieControl;

namespace TestGUI
{
    public partial class InfoControl : UserControl
    {
        public InfoControl()
        {
            InitializeComponent();
        }

        public Color BehindColor
        {
            set
            {
                foreach (var control in Controls)
                {
                    ((Control)control).BackColor = value;
                };
            }
        }


        public Serie serie { get; set; }
        public void UpdateAll()
        {
            BackgroundImage = DrawZigZag(Size);
            labelName.Text = serie.name;
            labelName.Location = new Point((Width - space - labelName.Width) / 2 + space, labelName.Height);
            
            pictureBox1.Location = new Point(space + 20, pictureBox1.Height);







            pictureBox1.Image = Image.FromFile(AppDomain.CurrentDomain.BaseDirectory + "Flags\\" + serie.info.language + ".png");
        }

        int space;
        private Image DrawZigZag(Size size)
        {
            Bitmap image = new Bitmap(size.Width, size.Height);

            using (var g = Graphics.FromImage(image))
            {
                Form form = this.FindForm();
                if (form == null)
                    return null;
                
                int graySpace = 5;

                g.SmoothingMode = SmoothingMode.AntiAlias;
                g.InterpolationMode = InterpolationMode.HighQualityBicubic;
                g.PixelOffsetMode = PixelOffsetMode.HighQuality;
                Brush brush = Brushes.White;
                #region Header
                space = (int)(image.Width * 0.111);
                g.FillRectangle(brush, space + graySpace, 0, image.Width, image.Height);



                int x = space + graySpace, y = 0;
                //spicchi delle punte (metà lunghezza e l'altezza)
                int trW = 48, trH = 38;

                while (y < image.Height)
                {
                    g.FillPolygon(Brushes.Gray, new Point[] { new Point(x, y - graySpace), new Point(x - trW - graySpace, y + trH), new Point(x, y + (trH * 2) + graySpace) });
                    y += trH * 2;
                }

                y = 0;
                while (y < image.Height)
                {
                    g.FillPolygon(brush, new Point[] { new Point(x, y), new Point(x - trW, y + trH), new Point(x, y + (trH * 2)) });
                    y += trH * 2;
                }
                #endregion

                //image.Save(@"C:\test\1.png");

            }

            return image;
        }

    }
}
