﻿using System.Collections.Generic;
using System.Drawing;
using System.IO;

namespace SerieControl
{
    public class Season
    {
        #region SetUp
        public Season(Season season)
        {
            CopyProperties.CopyPropertiesTo(season, this);
        }

        public Season(string name, List<Episode> episodes)
        {
            this.name = name;
            Episodes = episodes;
        }
        public Season(string name)
        {
            this.name = name;
            Episodes = new List<Episode>();
        }
        #endregion

        public List<Episode> Episodes { get; set; }

        #region Info
        public string name
        {
            get { return infos.Count == 0 ? null : info.name; }
            set { if (infos.Count != 0) info.name = value; }
        }
        public SeasonMyInfo info
        {
            get { return infos.Count == 0 ? null : infos[indexInfo]; }
            set { if (infos.Count != 0) infos[indexInfo] = value; }
        }

        int indexInfo = 0;
        public List<SeasonMyInfo> infos { get; set; } = new List<SeasonMyInfo>() { new SeasonMyInfo() };

        #region Info sull'ep che non cambiano in base alla lingua
        public Image preview { get; set; }

        
        public string originalNetwork { get; set; }
        //public string BestPictiureFormat { get; set; } //1080p/i |1080x720|16:9 HDTV
        //public string BestAudioFormat { get; set; }

        public string showUrl { get; set; }

        //public string dvdDiscid { get; set; }
        //public object dvdSeason { get; set; }
        //public object dvdEpisodeNumber { get; set; }
        //public object dvdChapter { get; set; }
        public string imdbId { get; set; }
        public Dictionary<Site, double> sitesRatings { get; set; }
        #endregion
        #endregion

        #region Proprieties
        public bool Completata
        {
            set
            {
                foreach (var ep in Episodes)
                {
                    ep.visto = value;
                }
            }

            get
            { //return Episodes[Episodes.Count].visto; 
                foreach (var ep in Episodes)
                {
                    if (!ep.visto)
                        return false;
                }

                return true;
            }
        }

        public int DaVedere
        {
            get
            { //return Episodes[Episodes.Count].visto; 
                for (int i = 0; i < Episodes.Count; i++)
                {
                    if (!Episodes[i].visto)
                        return i;
                }

                return -1;
            }
        }

        public int DaIniziare
        {
            get
            { //return Episodes[Episodes.Count].visto; 
                for (int i = 0; i < Episodes.Count; i++)
                {
                    if (Episodes[i].tempoVisto == 0)
                        return i;
                }

                return -1;
            }
        }

        public bool Iniziata
        {
            get
            { //return Episodes[Episodes.Count].visto; 
                foreach (var ep in Episodes)
                {
                    if (ep.tempoVisto != 0)
                        return true;
                }

                return false;
            }
        }

        public override string ToString()
        {
            return name;
        }
        #endregion
    }
}
